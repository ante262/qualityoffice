﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Office.Service
{
    public class ForeignStudentService : IForeignStudentService
    {
        #region Constructors

        public ForeignStudentService(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IForeignStudent entity)
        {
            return Repository.DeleteAsync<foreignstudent>(Mapper.Map<foreignstudent>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<foreignstudent>(ID);
        }

        public async Task<List<IForeignStudent>> GetAsync()
        {
            return Mapper.Map<List<IForeignStudent>>(
                await Repository.WhereAsync<foreignstudent>()
                        .ToListAsync<foreignstudent>()
                );
        }

        public async Task<IForeignStudent> GetAsync(string ID)
        {
            return Mapper.Map<IForeignStudent>(await Repository.SingleAsync<foreignstudent>(ID));
        }

        public Task<int> InsertAsync(IForeignStudent entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            return Repository.InsertAsync<foreignstudent>(Mapper.Map<foreignstudent>(entity));
        }

        public Task<int> UpdateAsync(IForeignStudent entity)
        {
            return Repository.UpdateAsync<foreignstudent>(Mapper.Map<foreignstudent>(entity));
        }

        #endregion Methods
    }
}