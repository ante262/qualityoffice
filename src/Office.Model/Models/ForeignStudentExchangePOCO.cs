﻿using Office.Model.Common;
using System;

namespace Office.Model
{
    public partial class ForeignStudentExchangePOCO : IForeignStudentExchange
    {
        #region Properties

        public Nullable<short> AcademicYear { get; set; }
        public string Comment { get; set; }
        public ICourse Course { get; set; }
        public string CourseId { get; set; }
        public IDepartment Department { get; set; }
        public string DepartmentId { get; set; }
        public IDuration Duration { get; set; }
        public string DurationId { get; set; }
        public IExchangeOrganization ExchangeOrganization { get; set; }
        public string ExchangeOrganizationId { get; set; }
        public string ExchangeResult { get; set; }
        public IForeignInstitution ForeignInstitution { get; set; }
        public string ForeignInstitutionId { get; set; }
        public IForeignStudent ForeignStudent { get; set; }
        public string ForeignStudentId { get; set; }
        public IFunding FundingOfStay { get; set; }
        public string FundingOfStayId { get; set; }
        public string ID { get; set; }
        public ISemester Semester { get; set; }
        public string SemesterId { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public IFunding TravelFunding { get; set; }
        public string TravelFundingId { get; set; }
        public IUserProfile UserProfile { get; set; }
        public string UserProfileId { get; set; }
        public IVisitPurpose VisitPurpose { get; set; }
        public string VisitPurposeId { get; set; }

        #endregion Properties
    }
}