﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IOurProfessorVisitService
    {
        #region Methods

        Task<int> DeleteAsync(IOurProfessorVisit entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IOurProfessorVisit>> GetAsync();

        Task<IOurProfessorVisit> GetAsync(string ID);

        Task<int> InsertAsync(IOurProfessorVisit entity);

        Task<int> UpdateAsync(IOurProfessorVisit entity);

        #endregion Methods
    }
}