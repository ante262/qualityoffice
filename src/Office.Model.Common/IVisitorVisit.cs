﻿using System;
using System.Collections.Generic;

namespace Office.Model.Common
{
    public interface IVisitorVisit
    {
        #region Properties

        Nullable<short> AcademicYear { get; set; }
        string Comment { get; set; }
        IDepartment Department { get; set; }
        string DepartmentId { get; set; }
        IDuration Duration { get; set; }
        string DurationId { get; set; }
        IFunding FundingOfStay { get; set; }
        string FundingOfStayId { get; set; }
        string ID { get; set; }
        Nullable<System.DateTime> StartDate { get; set; }
        IFunding TravelFunding { get; set; }
        string TravelFundingId { get; set; }
        IUserProfile UserProfile { get; set; }
        string UserProfileId { get; set; }
        IVisitorProfile VisitorProfile { get; set; }
        string VisitorProfileId { get; set; }
        IList<IVisitorVisitType> VisitorVisitTypes { get; set; }
        string VisitResult { get; set; }

        #endregion Properties
    }
}