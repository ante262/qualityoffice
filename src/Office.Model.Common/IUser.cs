﻿using System;
using System.Collections.Generic;

namespace Office.Model.Common
{
    public interface IUser
    {
        #region Properties

        int AccessFailedCount { get; set; }
        string Email { get; set; }
        bool EmailConfirmed { get; set; }
        string ID { get; set; }
        bool LockoutEnabled { get; set; }
        Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        string PasswordHash { get; set; }
        string PhoneNumber { get; set; }
        bool PhoneNumberConfirmed { get; set; }

        //ICollection<IUserClaim> UserClaims { get; set; }
        //ICollection<IUserLogin> UserLogins { get; set; }
        //IUserProfile UserProfile { get; set; }
        ICollection<IRole> Roles { get; set; }

        string SecurityStamp { get; set; }
        bool TwoFactorEnabled { get; set; }
        string UserName { get; set; }

        #endregion Properties
    }
}