﻿(function (angular) {
    'use strict';
    angular.module('officeApp').directive('topBar', ['$state', 'localStorageService', 'authorizationService', function ($state, localStorageService, authorizationService) {
        return {
            templateUrl: 'app/directive/top-bar/top-bar.dir.html',
            scope: {},
            link: function (scope) {
                scope.currentState = function () {
                    return !($state.includes('login') || $state.includes('register'));
                };

                scope.$on("$stateChangeSuccess", function () {
                    scope.authorizationData = localStorageService.get('authorizationData');
                    scope.logged = scope.authorizationData ? true : false;
                });

                scope.logOut = function () {
                    authorizationService.logOut();
                    scope.authorizationData = {};
                    scope.logged = false;
                };
            } // link
        }; // return
    }]);
})(angular);