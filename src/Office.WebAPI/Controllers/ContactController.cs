﻿using AutoMapper;
using Office.Model.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/Contact")]
    public class ContactController : ApiController
    {
        #region Constructors

        public ContactController(IContactService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private IContactService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/Contact/
        [HttpDelete]
        [Route("{ID}")]
        public async Task<IHttpActionResult> Delete(string ID)
        {
            try
            {
                var result = await Service.DeleteAsync(ID);
                if (result == 1) return Ok("Deleted");
                else return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        // GET: api/Contact
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<ContactModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/Contact/5
        [HttpGet]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Get(string ID)
        {
            var result = await Service.GetAsync(ID);

            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<ContactModel>(result));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        // POST: api/Contact
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post(ContactModel entity)
        {
            try
            {
                var result = await Service.InsertAsync(Mapper.Map<IContact>(entity));

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Add operation error.");

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/Contact/5
        [HttpPut]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Put(string ID, ContactModel entity)
        {
            try
            {
                if (ID != entity.ID)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "IDs do not match.");
                }

                var result = await Service.UpdateAsync(Mapper.Map<IContact>(entity));
                if (result == 1)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "Failed.");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public partial class ContactModel
        {
            #region Properties

            public string Abbreviation { get; set; }
            public string Email { get; set; }
            public string ID { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public string TelephoneNumber { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}