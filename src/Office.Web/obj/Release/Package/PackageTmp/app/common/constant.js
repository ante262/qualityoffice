﻿(function (angular) {
    'use strict';
    angular.module('officeApp')
        .constant('notifications', {
            entity: {
                registerSuccess: "Registracija je uspjela. Kroz nekoliko sekundi dobit ćete e-mail za verifikaciju.",
                registerError: "Greška! Registracija nije uspjela!",
                registerInputError: "Greška! Ponovljena lozinka ili e-mail nisu ispravni!",
                saveSuccess: "Novi unos uspješno spremljen!",
                saveError: "Greška pri spremanju unosa!",
                editSuccess: "Izmjena je spremljena!",
                editError: "Greška! Izmjena nije uspjela!",
                deleteSuccess: "Unos je obrisan!",
                deleteError: "Greška pri brisanju!",
                loadError: "Greška pri učitavanju podataka, pokušajte ponovo!",
                serverError: "Greška pri dohvaćanju podataka sa servera, provjerite konekciju!",
                loginError: "Greška prilikom unosa korisničkog imena ili lozinke!"
            }
        })
})(angular);