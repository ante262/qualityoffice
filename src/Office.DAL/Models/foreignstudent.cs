using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class foreignstudent
    {
        public foreignstudent()
        {
            this.foreignstudentexchanges = new List<foreignstudentexchange>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string TelephoneNumber { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<foreignstudentexchange> foreignstudentexchanges { get; set; }
    }
}
