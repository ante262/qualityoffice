﻿using System;

namespace Office.Model.Common
{
    public interface IUserAcademicRank
    {
        #region Properties

        string Abbreviation { get; set; }
        string EnglishName { get; set; }
        string ID { get; set; }
        string Name { get; set; }

        #endregion Properties
    }
}