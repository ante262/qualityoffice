(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ForeignInstitutionController', ['$scope', 'foreignInstitutionService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, foreignInstitutionService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.foreignInstitutions = [];
            vm.foreignInstitution = {};
            vm.selectedForeignInstitution = null;
            vm.loading = true;

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editForeignInstitution = function (data) {
                ngDialog.open({
                    template: 'app/foreign-institution/edit-foreign-institution.html',
                    controller: 'EditForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.showForeignInstitution = function (data) {
                ngDialog.open({
                    template: 'app/foreign-institution/show-foreign-institution.html',
                    controller: 'ShowForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            };

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return foreignInstitutionService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('foreignInstitution', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get foreignInstitution:' + data.message);
                    $state.go('foreignInstitution', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.foreignInstitutions = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedForeignInstitution = item;
                foreignInstitutionService.get(vm.selectedForeignInstitution)
                    .success(function (data) {
                        vm.foreignInstitution = data;
                    }).error(function (data) {
                        console.log("Unable to get this foreignInstitution:" + data.message);
                    });
            };

            vm.delete = function (data) {
                vm.foreignInstitution = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Institucija '" + vm.foreignInstitution.Name + "' će biti obrisana!")) {
                    foreignInstitutionService.delete(vm.foreignInstitution.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);