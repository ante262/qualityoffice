﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface INonPublicationLectureService
    {
        #region Methods

        Task<int> DeleteAsync(INonPublicationLecture entity);

        Task<int> DeleteAsync(string ID);

        Task<List<INonPublicationLecture>> GetAsync();

        Task<INonPublicationLecture> GetAsync(string ID);

        Task<int> InsertAsync(INonPublicationLecture entity);

        Task<int> UpdateAsync(INonPublicationLecture entity);

        #endregion Methods
    }
}