(function (angular) {
    'use strict';
    angular.module('officeApp').service('foreignStudentService', ['$http', 'baseUrl','$httpParamSerializerJQLike',
        function ($http, baseUrl, $httpParamSerializerJQLike) {
            var tUrl = "ForeignStudent";

            return {
                //get all foreignStudents
                fetch: function () {
                    return $http.get(baseUrl + tUrl);
                },
                //get foreignStudent by id
                get: function (id) {
                    return $http.get(baseUrl + tUrl + "/" + id);
                },
                //post foreignStudent
                post: function (data) {
                    var req = {
                        method: 'POST',
                        url: baseUrl + tUrl,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //put foreignStudent
                put: function (data) {
                    var req = {
                        method: 'PUT',
                        url: baseUrl + tUrl + "/" + data.ID,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //delete foreignStudent
                delete: function (item) {
                    var req = {
                        method: 'DELETE',
                        url: baseUrl + tUrl + "/" + item,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    };
                    return $http(req);
                }
            };
        }]);
})(angular);