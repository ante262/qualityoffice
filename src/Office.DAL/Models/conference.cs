using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class conference
    {
        public conference()
        {
            this.conferenceinstitutions = new List<conferenceinstitution>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string ConferenceTypeId { get; set; }
        public string Location { get; set; }
        public string URL { get; set; }
        public string Locality { get; set; }
        public string Profession { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public virtual conferencetype conferencetype { get; set; }
        public virtual ICollection<conferenceinstitution> conferenceinstitutions { get; set; }
    }
}
