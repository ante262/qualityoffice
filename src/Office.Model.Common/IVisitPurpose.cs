﻿using System;

namespace Office.Model.Common
{
    public interface IVisitPurpose
    {
        #region Properties

        string Abbreviation { get; set; }
        string Comment { get; set; }
        string ID { get; set; }
        string Type { get; set; }

        #endregion Properties
    }
}