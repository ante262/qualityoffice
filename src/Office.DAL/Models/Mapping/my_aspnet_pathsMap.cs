using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class my_aspnet_pathsMap : EntityTypeConfiguration<my_aspnet_paths>
    {
        public my_aspnet_pathsMap()
        {
            // Primary Key
            this.HasKey(t => t.pathId);

            // Properties
            this.Property(t => t.pathId)
                .IsRequired()
                .HasMaxLength(36);

            this.Property(t => t.path)
                .IsRequired()
                .HasMaxLength(256);

            this.Property(t => t.loweredPath)
                .IsRequired()
                .HasMaxLength(256);

            // Table & Column Mappings
            this.ToTable("my_aspnet_paths", "officeapp");
            this.Property(t => t.applicationId).HasColumnName("applicationId");
            this.Property(t => t.pathId).HasColumnName("pathId");
            this.Property(t => t.path).HasColumnName("path");
            this.Property(t => t.loweredPath).HasColumnName("loweredPath");
        }
    }
}
