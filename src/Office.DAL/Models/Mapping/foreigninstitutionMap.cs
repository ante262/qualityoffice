using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class foreigninstitutionMap : EntityTypeConfiguration<foreigninstitution>
    {
        public foreigninstitutionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.InstitutionName)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.City)
                .HasMaxLength(128);

            this.Property(t => t.State)
                .HasMaxLength(128);

            this.Property(t => t.URL)
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .HasMaxLength(1073741823);

            this.Property(t => t.ContactId)
                .HasMaxLength(128);

            this.Property(t => t.UserProfileId)
                .HasMaxLength(128);

            this.Property(t => t.ContractId)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("foreigninstitution", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.InstitutionName).HasColumnName("InstitutionName");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Foreign).HasColumnName("Foreign");
            this.Property(t => t.ContactId).HasColumnName("ContactId");
            this.Property(t => t.UserProfileId).HasColumnName("UserProfileId");
            this.Property(t => t.IsContract).HasColumnName("IsContract");
            this.Property(t => t.ContractId).HasColumnName("ContractId");

            // Relationships
            this.HasOptional(t => t.contact)
                .WithMany(t => t.foreigninstitutions)
                .HasForeignKey(d => d.ContactId);
            this.HasOptional(t => t.contract)
                .WithMany(t => t.foreigninstitutions)
                .HasForeignKey(d => d.ContractId);
            this.HasOptional(t => t.userprofile)
                .WithMany(t => t.foreigninstitutions)
                .HasForeignKey(d => d.UserProfileId);

        }
    }
}
