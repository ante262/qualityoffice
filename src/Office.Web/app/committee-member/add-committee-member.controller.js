(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddCommitteeMemberController', ['$scope', 'committeeMemberService', 'userProfileService', 'lookupTablesService', 'notifications', 'ngDialog',
        function ($scope, committeeMemberService, userProfileService, lookupTablesService, notifications, ngDialog) {
            var vm = $scope.vm = {};
            vm.committeeMember = {};
            vm.lookups = [];
            vm.userProfiles = [];
            vm.check = {
                user: false,
                role: false,
                institution: false,
                committee: false
            };

            vm.addUserProfile = function () {
                ngDialog.open({
                    template: 'app/user-profile/add-user-profile.html',
                    controller: 'AddUserProfileController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchUsers = function () {
                return userProfileService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.userProfiles = data;
                    });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('Committees, CommitteeMemberRoles, ElectingInstitutions')
                    .then(function (data) {
                        vm.lookups = data;
                    },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchUsers();
                vm.fetchLookups();
            }
            vm.fetch();

            vm.post = function (item) {
                if (item.CommitteeMemberRole != null || item.ElectingInstitution != null ||
                    item.Committee != null) {
                    lookupTablesService.clearCache();
                };

                committeeMemberService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log('error', data);
                    });
            };
        }]);
})(angular);