﻿(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ChairController', ['$scope', 'chairService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, chairService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.chairs = [];
            vm.chair = {};
            vm.selectedChair = null;
            vm.loading = true;

            vm.addChair = function () {
                ngDialog.open({
                    template: 'app/chair/add-chair.html',
                    controller: 'AddChairController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editChair = function (data) {
                ngDialog.open({
                    template: 'app/chair/edit-chair.html',
                    controller: 'EditChairController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            }

            vm.showChair = function (data) {
                ngDialog.open({
                    template: 'app/chair/show-chair.html',
                    controller: 'ShowChairController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            }

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return chairService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('chair', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get chair:' + data.message);
                    $state.go('chair', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.chairs = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedChair = item;
                chairService.get(vm.selectedChair).success(function (data) {
                    vm.chair = data;
                }).error(function (data) {
                    console.log("Unable to get this chair:" + data.message);
                });
            };

            vm.delete = function (data) {
                vm.chair = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Predsjedništvo '" + vm.chair.Name + "' će biti obrisano!")) {
                    chairService.delete(vm.chair.ID)
                        .success(function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        })
                        .error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        })
                        .finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);