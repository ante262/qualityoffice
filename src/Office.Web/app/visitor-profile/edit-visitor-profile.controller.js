(function (angular) {
    'use strict';
    angular.module('officeApp').controller('EditVisitorProfileController', ['$scope', 'visitorProfileService', 'foreignInstitutionService', 'lookupTablesService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, visitorProfileService, foreignInstitutionService, lookupTablesService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.visitorProfile = $scope.ngDialogData;
            vm.lookups = [];
            vm.foreignInstitutions = [];
            vm.check = {
                title: false,
                institution: false
            };

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchForeignInstitutions = function () {
                return foreignInstitutionService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.foreignInstitutions = data;
                    });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('UserTitles').then(function (data) {
                    vm.lookups = data;
                },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchForeignInstitutions();
                vm.fetchLookups();
            }
            vm.fetch();

            vm.put = function (item) {
                if (item.UserTitle) {
                    lookupTablesService.clearCache();
                };
                vm.visitorProfile.ForeignInstitution = null;

                visitorProfileService.put(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                        $state.go('visitorProfile', { 'notification': notifications.entity.editError });
                    });
            };
        }]);
})(angular);