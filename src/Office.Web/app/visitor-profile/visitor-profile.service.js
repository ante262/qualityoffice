(function (angular) {
    'use strict';
    angular.module('officeApp').service('visitorProfileService', ['$http', 'baseUrl','$httpParamSerializerJQLike',
        function ($http, baseUrl, $httpParamSerializerJQLike) {
            var tUrl = "VisitorProfile";

            return {
                //get all visitorProfiles
                fetch: function () {
                    return $http.get(baseUrl + tUrl);
                },
                //get visitorProfile by id
                get: function (id) {
                    return $http.get(baseUrl + tUrl + "/" + id);
                },
                //post visitorProfile
                post: function (data) {
                    var req = {
                        method: 'POST',
                        url: baseUrl + tUrl,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //put visitorProfile
                put: function (data) {
                    var req = {
                        method: 'PUT',
                        url: baseUrl + tUrl + "/" + data.ID,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //delete visitorProfile
                delete: function (item) {
                    var req = {
                        method: 'DELETE',
                        url: baseUrl + tUrl + "/" + item,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    };
                    return $http(req);
                }
            };
        }]);
})(angular);