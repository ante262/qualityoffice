﻿(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddConferenceTypeController', ['$scope', 'conferenceTypeService', 'notifications', '$window', '$state', 'ngDialog',
        function ($scope, conferenceTypeService, notifications, $window, $state, ngDialog) {
            var vm = $scope.vm = {};
            vm.conferenceType = {};

            vm.post = function (item) {
                conferenceTypeService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(data);
                    });
            };
        }]);
})(angular);