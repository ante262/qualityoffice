CREATE DATABASE  IF NOT EXISTS `officeapp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `officeapp`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: officeapp
-- ------------------------------------------------------
-- Server version	5.6.30-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chair`
--

DROP TABLE IF EXISTS `chair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chair` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) NOT NULL,
  `EnglishName` varchar(128) DEFAULT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `committee`
--

DROP TABLE IF EXISTS `committee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `committee` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) NOT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `committeemember`
--

DROP TABLE IF EXISTS `committeemember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `committeemember` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `UserProfileId` varchar(128) DEFAULT NULL,
  `Title` varchar(128) DEFAULT NULL,
  `CommitteeId` varchar(128) DEFAULT NULL,
  `CommitteeMemberRoleId` varchar(128) DEFAULT NULL,
  `PeriodOfElection` varchar(128) DEFAULT NULL,
  `DateOfElection` date DEFAULT NULL,
  `ElectingInstitutionId` varchar(128) DEFAULT NULL,
  `Comment` longtext,
  PRIMARY KEY (`ID`),
  KEY `committeemember_userprofile` (`UserProfileId`),
  KEY `committeemember_committee` (`CommitteeId`),
  KEY `committeemember_committeememberrole` (`CommitteeMemberRoleId`),
  KEY `committeemember_electinginstitution` (`ElectingInstitutionId`),
  CONSTRAINT `committeemember_committee` FOREIGN KEY (`CommitteeId`) REFERENCES `committee` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `committeemember_committeememberrole` FOREIGN KEY (`CommitteeMemberRoleId`) REFERENCES `committeememberrole` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `committeemember_electinginstitution` FOREIGN KEY (`ElectingInstitutionId`) REFERENCES `electinginstitution` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `committeemember_userprofile` FOREIGN KEY (`UserProfileId`) REFERENCES `userprofile` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `committeememberrole`
--

DROP TABLE IF EXISTS `committeememberrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `committeememberrole` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) NOT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conference`
--

DROP TABLE IF EXISTS `conference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) DEFAULT NULL,
  `EnglishName` varchar(128) DEFAULT NULL,
  `ConferenceTypeId` varchar(128) DEFAULT NULL,
  `Location` varchar(128) DEFAULT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `Locality` varchar(128) DEFAULT NULL,
  `Profession` varchar(128) DEFAULT NULL,
  `Comment` longtext,
  `Date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `conference_conferencetype` (`ConferenceTypeId`),
  CONSTRAINT `conference_conferencetype` FOREIGN KEY (`ConferenceTypeId`) REFERENCES `conferencetype` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conferenceinstitution`
--

DROP TABLE IF EXISTS `conferenceinstitution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conferenceinstitution` (
  `ID` varchar(128) NOT NULL,
  `ConferenceId` varchar(128) DEFAULT NULL,
  `ForeignInstitutionId` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `conferenceinstitution_conference` (`ConferenceId`),
  KEY `conferenceinstitution_foreigninstitution` (`ForeignInstitutionId`),
  CONSTRAINT `conferenceinstitution_conference` FOREIGN KEY (`ConferenceId`) REFERENCES `conference` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `conferenceinstitution_foreigninstitution` FOREIGN KEY (`ForeignInstitutionId`) REFERENCES `foreigninstitution` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conferencetype`
--

DROP TABLE IF EXISTS `conferencetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conferencetype` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Type` varchar(128) NOT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Type_UNIQUE` (`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) DEFAULT NULL,
  `Surname` varchar(128) DEFAULT NULL,
  `TelephoneNumber` varchar(128) DEFAULT NULL,
  `Email` varchar(128) DEFAULT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contract`
--

DROP TABLE IF EXISTS `contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) DEFAULT NULL,
  `ContractTypeId` varchar(128) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `DurationId` varchar(128) DEFAULT NULL,
  `FundingTypeId` varchar(128) DEFAULT NULL,
  `Description` longtext,
  `URL` varchar(250) DEFAULT NULL,
  `Comment` longtext,
  PRIMARY KEY (`ID`),
  KEY `contract_contracttype` (`ContractTypeId`),
  KEY `contract_duration` (`DurationId`),
  KEY `contract_fundingtype` (`FundingTypeId`),
  CONSTRAINT `contract_contracttype` FOREIGN KEY (`ContractTypeId`) REFERENCES `contracttype` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `contract_duration` FOREIGN KEY (`DurationId`) REFERENCES `duration` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `contract_fundingtype` FOREIGN KEY (`FundingTypeId`) REFERENCES `funding` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contracttype`
--

DROP TABLE IF EXISTS `contracttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contracttype` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Type` varchar(128) DEFAULT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) NOT NULL,
  `Comment` longtext,
  `URL` varchar(250) DEFAULT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) NOT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `Comment` longtext,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duration`
--

DROP TABLE IF EXISTS `duration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duration` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Length` varchar(30) NOT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electinginstitution`
--

DROP TABLE IF EXISTS `electinginstitution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electinginstitution` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) NOT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exchangeorganization`
--

DROP TABLE IF EXISTS `exchangeorganization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exchangeorganization` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) NOT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `Comment` longtext,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreigninstitution`
--

DROP TABLE IF EXISTS `foreigninstitution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreigninstitution` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `InstitutionName` varchar(128) NOT NULL,
  `City` varchar(128) DEFAULT NULL,
  `State` varchar(128) DEFAULT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `Description` longtext,
  `Foreign` bit(1) DEFAULT NULL,
  `ContactId` varchar(128) DEFAULT NULL,
  `UserProfileId` varchar(128) DEFAULT NULL,
  `IsContract` bit(1) DEFAULT NULL,
  `ContractId` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `foreigninstitution_contact_idx` (`ContactId`),
  KEY `foreigninstitution_userprofile_idx` (`UserProfileId`),
  KEY `foreigninstitution_contract_idx` (`ContractId`),
  CONSTRAINT `foreigninstitution_contact` FOREIGN KEY (`ContactId`) REFERENCES `contact` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreigninstitution_contract` FOREIGN KEY (`ContractId`) REFERENCES `contract` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreigninstitution_userprofile` FOREIGN KEY (`UserProfileId`) REFERENCES `userprofile` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreignstudent`
--

DROP TABLE IF EXISTS `foreignstudent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreignstudent` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) NOT NULL,
  `Surname` varchar(128) NOT NULL,
  `City` varchar(128) DEFAULT NULL,
  `State` varchar(128) DEFAULT NULL,
  `TelephoneNumber` varchar(128) DEFAULT NULL,
  `Email` varchar(128) DEFAULT NULL,
  `Comment` longtext,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreignstudentexchange`
--

DROP TABLE IF EXISTS `foreignstudentexchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreignstudentexchange` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `DepartmentId` varchar(128) DEFAULT NULL,
  `UserProfileId` varchar(128) DEFAULT NULL,
  `CourseId` varchar(128) DEFAULT NULL,
  `VisitPurposeId` varchar(128) DEFAULT NULL,
  `ForeignStudentId` varchar(128) DEFAULT NULL,
  `SemesterId` varchar(128) DEFAULT NULL,
  `ForeignInstitutionId` varchar(128) DEFAULT NULL,
  `AcademicYear` smallint(6) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `DurationId` varchar(128) DEFAULT NULL,
  `ExchangeOrganizationId` varchar(128) DEFAULT NULL,
  `TravelFundingId` varchar(128) DEFAULT NULL,
  `FundingOfStayId` varchar(128) DEFAULT NULL,
  `ExchangeResult` longtext,
  `Comment` longtext,
  PRIMARY KEY (`ID`),
  KEY `foreignstudentexchange_department` (`DepartmentId`),
  KEY `foreignstudentexchange_userprofile` (`UserProfileId`),
  KEY `foreignstudentexchange_course` (`CourseId`),
  KEY `foreignstudentexchange_visitpurpose` (`VisitPurposeId`),
  KEY `foreignstudentexchange_foreignstudent` (`ForeignStudentId`),
  KEY `foreignstudentexchange_semester` (`SemesterId`),
  KEY `foreignstudentexchange_foreigninstitution` (`ForeignInstitutionId`),
  KEY `foreignstudentexchange_duration` (`DurationId`),
  KEY `foreignstudentexchange_exchangeorganization` (`ExchangeOrganizationId`),
  KEY `foreignstudentexchange_travelfunding` (`TravelFundingId`),
  KEY `foreignstudentexchange_fundingofstay` (`FundingOfStayId`),
  CONSTRAINT `foreignstudentexchange_course` FOREIGN KEY (`CourseId`) REFERENCES `course` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreignstudentexchange_department` FOREIGN KEY (`DepartmentId`) REFERENCES `department` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreignstudentexchange_duration` FOREIGN KEY (`DurationId`) REFERENCES `duration` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreignstudentexchange_exchangeorganization` FOREIGN KEY (`ExchangeOrganizationId`) REFERENCES `exchangeorganization` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreignstudentexchange_foreigninstitution` FOREIGN KEY (`ForeignInstitutionId`) REFERENCES `foreigninstitution` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreignstudentexchange_foreignstudent` FOREIGN KEY (`ForeignStudentId`) REFERENCES `foreignstudent` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreignstudentexchange_fundingofstay` FOREIGN KEY (`FundingOfStayId`) REFERENCES `funding` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreignstudentexchange_semester` FOREIGN KEY (`SemesterId`) REFERENCES `semester` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreignstudentexchange_travelfunding` FOREIGN KEY (`TravelFundingId`) REFERENCES `funding` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreignstudentexchange_userprofile` FOREIGN KEY (`UserProfileId`) REFERENCES `userprofile` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `foreignstudentexchange_visitpurpose` FOREIGN KEY (`VisitPurposeId`) REFERENCES `visitpurpose` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `funding`
--

DROP TABLE IF EXISTS `funding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funding` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Type` varchar(30) NOT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_applications`
--

DROP TABLE IF EXISTS `my_aspnet_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_membership`
--

DROP TABLE IF EXISTS `my_aspnet_membership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_membership` (
  `userId` int(11) NOT NULL DEFAULT '0',
  `Email` varchar(128) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Password` varchar(128) NOT NULL,
  `PasswordKey` char(32) DEFAULT NULL,
  `PasswordFormat` tinyint(4) DEFAULT NULL,
  `PasswordQuestion` varchar(255) DEFAULT NULL,
  `PasswordAnswer` varchar(255) DEFAULT NULL,
  `IsApproved` tinyint(1) DEFAULT NULL,
  `LastActivityDate` datetime DEFAULT NULL,
  `LastLoginDate` datetime DEFAULT NULL,
  `LastPasswordChangedDate` datetime DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `IsLockedOut` tinyint(1) DEFAULT NULL,
  `LastLockedOutDate` datetime DEFAULT NULL,
  `FailedPasswordAttemptCount` int(10) unsigned DEFAULT NULL,
  `FailedPasswordAttemptWindowStart` datetime DEFAULT NULL,
  `FailedPasswordAnswerAttemptCount` int(10) unsigned DEFAULT NULL,
  `FailedPasswordAnswerAttemptWindowStart` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='2';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_paths`
--

DROP TABLE IF EXISTS `my_aspnet_paths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_paths` (
  `applicationId` int(11) NOT NULL,
  `pathId` varchar(36) NOT NULL,
  `path` varchar(256) NOT NULL,
  `loweredPath` varchar(256) NOT NULL,
  PRIMARY KEY (`pathId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_personalizationallusers`
--

DROP TABLE IF EXISTS `my_aspnet_personalizationallusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_personalizationallusers` (
  `pathId` varchar(36) NOT NULL,
  `pageSettings` blob NOT NULL,
  `lastUpdatedDate` datetime NOT NULL,
  PRIMARY KEY (`pathId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_personalizationperuser`
--

DROP TABLE IF EXISTS `my_aspnet_personalizationperuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_personalizationperuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicationId` int(11) NOT NULL,
  `pathId` varchar(36) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `pageSettings` blob NOT NULL,
  `lastUpdatedDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_profiles`
--

DROP TABLE IF EXISTS `my_aspnet_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_profiles` (
  `userId` int(11) NOT NULL,
  `valueindex` longtext,
  `stringdata` longtext,
  `binarydata` longblob,
  `lastUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_roles`
--

DROP TABLE IF EXISTS `my_aspnet_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicationId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_schemaversion`
--

DROP TABLE IF EXISTS `my_aspnet_schemaversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_schemaversion` (
  `version` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_sessioncleanup`
--

DROP TABLE IF EXISTS `my_aspnet_sessioncleanup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_sessioncleanup` (
  `LastRun` datetime NOT NULL,
  `IntervalMinutes` int(11) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  PRIMARY KEY (`ApplicationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_sessions`
--

DROP TABLE IF EXISTS `my_aspnet_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_sessions` (
  `SessionId` varchar(191) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Expires` datetime NOT NULL,
  `LockDate` datetime NOT NULL,
  `LockId` int(11) NOT NULL,
  `Timeout` int(11) NOT NULL,
  `Locked` tinyint(1) NOT NULL,
  `SessionItems` longblob,
  `Flags` int(11) NOT NULL,
  PRIMARY KEY (`SessionId`,`ApplicationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_sitemap`
--

DROP TABLE IF EXISTS `my_aspnet_sitemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_sitemap` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) DEFAULT NULL,
  `Description` varchar(512) DEFAULT NULL,
  `Url` varchar(512) DEFAULT NULL,
  `Roles` varchar(1000) DEFAULT NULL,
  `ParentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_users`
--

DROP TABLE IF EXISTS `my_aspnet_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicationId` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `isAnonymous` tinyint(1) NOT NULL DEFAULT '1',
  `lastActivityDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_aspnet_usersinroles`
--

DROP TABLE IF EXISTS `my_aspnet_usersinroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_usersinroles` (
  `userId` int(11) NOT NULL DEFAULT '0',
  `roleId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userId`,`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nonpublicationlecture`
--

DROP TABLE IF EXISTS `nonpublicationlecture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nonpublicationlecture` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) DEFAULT NULL,
  `Surname` varchar(128) DEFAULT NULL,
  `Location` varchar(128) DEFAULT NULL,
  `City` varchar(128) DEFAULT NULL,
  `State` varchar(128) DEFAULT NULL,
  `ConferenceTypeId` varchar(128) DEFAULT NULL,
  `Period` varchar(128) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `Title` varchar(128) DEFAULT NULL,
  `EnglishTitle` varchar(128) DEFAULT NULL,
  `Profession` varchar(128) DEFAULT NULL,
  `Summary` longtext,
  `Description` longtext,
  `URL` varchar(250) DEFAULT NULL,
  `Comment` longtext,
  PRIMARY KEY (`ID`),
  KEY `nonpublicationlecture_conferencetype` (`ConferenceTypeId`),
  CONSTRAINT `nonpublicationlecture_conferencetype` FOREIGN KEY (`ConferenceTypeId`) REFERENCES `conferencetype` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ourprofessorvisit`
--

DROP TABLE IF EXISTS `ourprofessorvisit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ourprofessorvisit` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `UserProfileId` varchar(128) DEFAULT NULL,
  `DepartmentId` varchar(128) DEFAULT NULL,
  `ForeignInstitutionId` varchar(128) DEFAULT NULL,
  `ConferenceTitle` longtext,
  `AcademicYear` smallint(6) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `DurationId` varchar(128) DEFAULT NULL,
  `TravelFundingId` varchar(128) DEFAULT NULL,
  `FundingOfStayId` varchar(128) DEFAULT NULL,
  `VisitResult` longtext,
  `URL` varchar(250) DEFAULT NULL,
  `Comment` longtext,
  PRIMARY KEY (`ID`),
  KEY `ourprofessorvisit_foreigninstitution` (`ForeignInstitutionId`),
  KEY `ourprofessorvisit_department` (`DepartmentId`),
  KEY `ourprofessorvisit_duration` (`DurationId`),
  KEY `ourprofessorvisit_userprofile` (`UserProfileId`),
  KEY `ourprofessorvisit_travelfunding` (`TravelFundingId`),
  KEY `ourprofessorvisit_fundingofstay` (`FundingOfStayId`),
  CONSTRAINT `ourprofessorvisit_department` FOREIGN KEY (`DepartmentId`) REFERENCES `department` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourprofessorvisit_duration` FOREIGN KEY (`DurationId`) REFERENCES `duration` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourprofessorvisit_foreigninstitution` FOREIGN KEY (`ForeignInstitutionId`) REFERENCES `foreigninstitution` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourprofessorvisit_fundingofstay` FOREIGN KEY (`FundingOfStayId`) REFERENCES `funding` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourprofessorvisit_travelfunding` FOREIGN KEY (`TravelFundingId`) REFERENCES `funding` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourprofessorvisit_userprofile` FOREIGN KEY (`UserProfileId`) REFERENCES `userprofile` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ourstudentexchange`
--

DROP TABLE IF EXISTS `ourstudentexchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ourstudentexchange` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `ForeignInstitutionId` varchar(128) DEFAULT NULL,
  `DepartmentId` varchar(128) DEFAULT NULL,
  `VisitPurposeId` varchar(128) DEFAULT NULL,
  `Name` varchar(128) DEFAULT NULL,
  `Surname` varchar(128) DEFAULT NULL,
  `SemesterId` varchar(128) DEFAULT NULL,
  `AcademicYear` smallint(6) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `DurationId` varchar(128) DEFAULT NULL,
  `ExchangeOrganizationId` varchar(128) DEFAULT NULL,
  `TravelFundingId` varchar(128) DEFAULT NULL,
  `FundingOfStayId` varchar(128) DEFAULT NULL,
  `ExchangeResult` longtext,
  `Comment` longtext,
  PRIMARY KEY (`ID`),
  KEY `ourstudentexchange_foreigninstitution` (`ForeignInstitutionId`),
  KEY `ourstudentexchange_department` (`DepartmentId`),
  KEY `ourstudentexchange_visitpurpose` (`VisitPurposeId`),
  KEY `ourstudentexchange_semester` (`SemesterId`),
  KEY `ourstudentexchange_duration` (`DurationId`),
  KEY `ourstudentexchange_exchangeorganization` (`ExchangeOrganizationId`),
  KEY `ourstudentexchange_travelfunding` (`TravelFundingId`),
  KEY `ourstudentexchange_fundingofstay` (`FundingOfStayId`),
  CONSTRAINT `ourstudentexchange_department` FOREIGN KEY (`DepartmentId`) REFERENCES `department` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourstudentexchange_duration` FOREIGN KEY (`DurationId`) REFERENCES `duration` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourstudentexchange_exchangeorganization` FOREIGN KEY (`ExchangeOrganizationId`) REFERENCES `exchangeorganization` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourstudentexchange_foreigninstitution` FOREIGN KEY (`ForeignInstitutionId`) REFERENCES `foreigninstitution` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourstudentexchange_fundingofstay` FOREIGN KEY (`FundingOfStayId`) REFERENCES `funding` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourstudentexchange_semester` FOREIGN KEY (`SemesterId`) REFERENCES `semester` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourstudentexchange_travelfunding` FOREIGN KEY (`TravelFundingId`) REFERENCES `funding` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `ourstudentexchange_visitpurpose` FOREIGN KEY (`VisitPurposeId`) REFERENCES `visitpurpose` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `professorvisittype`
--

DROP TABLE IF EXISTS `professorvisittype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professorvisittype` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `OurProfessorVisitId` varchar(128) DEFAULT NULL,
  `VisitTypeId` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `professorvisittype_ourprofessorvisit` (`OurProfessorVisitId`),
  KEY `professorvisittype_visittype` (`VisitTypeId`),
  CONSTRAINT `professorvisittype_ourprofessorvisit` FOREIGN KEY (`OurProfessorVisitId`) REFERENCES `ourprofessorvisit` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `professorvisittype_visittype` FOREIGN KEY (`VisitTypeId`) REFERENCES `visittype` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `researchgroup`
--

DROP TABLE IF EXISTS `researchgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `researchgroup` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) NOT NULL,
  `EnglishName` varchar(128) DEFAULT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `ID` varchar(128) NOT NULL,
  `Name` varchar(256) NOT NULL,
  `Description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `semester`
--

DROP TABLE IF EXISTS `semester`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `semester` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Type` varchar(30) NOT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID` varchar(128) NOT NULL,
  `Email` varchar(256) DEFAULT NULL,
  `EmailConfirmed` tinyint(1) NOT NULL,
  `PasswordHash` longtext,
  `SecurityStamp` longtext,
  `PhoneNumber` longtext,
  `PhoneNumberConfirmed` tinyint(1) NOT NULL,
  `TwoFactorEnabled` tinyint(1) NOT NULL,
  `LockoutEndDateUtc` datetime DEFAULT NULL,
  `LockoutEnabled` tinyint(1) NOT NULL,
  `AccessFailedCount` int(11) NOT NULL,
  `UserName` varchar(256) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `useracademicrank`
--

DROP TABLE IF EXISTS `useracademicrank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useracademicrank` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) NOT NULL,
  `EnglishName` varchar(128) DEFAULT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userclaim`
--

DROP TABLE IF EXISTS `userclaim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userclaim` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` varchar(128) NOT NULL,
  `ClaimType` longtext,
  `ClaimValue` longtext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `UserId` (`UserId`),
  CONSTRAINT `ApplicationUser_Claims` FOREIGN KEY (`UserId`) REFERENCES `user` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userlogin`
--

DROP TABLE IF EXISTS `userlogin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userlogin` (
  `LoginProvider` varchar(128) NOT NULL,
  `ProviderKey` varchar(128) NOT NULL,
  `UserId` varchar(128) NOT NULL,
  PRIMARY KEY (`LoginProvider`,`ProviderKey`,`UserId`),
  KEY `ApplicationUser_Logins` (`UserId`),
  CONSTRAINT `ApplicationUser_Logins` FOREIGN KEY (`UserId`) REFERENCES `user` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userprofile`
--

DROP TABLE IF EXISTS `userprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userprofile` (
  `ID` varchar(128) NOT NULL,
  `Name` varchar(128) NOT NULL,
  `Surname` varchar(128) NOT NULL,
  `TitleId` varchar(128) DEFAULT NULL,
  `AcademicRankId` varchar(128) DEFAULT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `PicturePath` varchar(500) DEFAULT NULL,
  `ResearchGroupId` varchar(128) DEFAULT NULL,
  `ChairId` varchar(128) DEFAULT NULL,
  `Room` varchar(10) DEFAULT NULL,
  `Floor` tinyint(2) DEFAULT NULL,
  `TelephoneNumber` varchar(128) DEFAULT NULL,
  `Comment` longtext,
  `Email` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userprofile_usertitle` (`TitleId`),
  KEY `userprofile_useracademicrank` (`AcademicRankId`),
  KEY `userprofile_researchgroup` (`ResearchGroupId`),
  KEY `userprofile_chair` (`ChairId`),
  CONSTRAINT `UserProfile_Chair` FOREIGN KEY (`ChairId`) REFERENCES `chair` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `UserProfile_ResearchGroup` FOREIGN KEY (`ResearchGroupId`) REFERENCES `researchgroup` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `UserProfile_UserAcademicRank` FOREIGN KEY (`AcademicRankId`) REFERENCES `useracademicrank` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `UserProfile_UserTitle` FOREIGN KEY (`TitleId`) REFERENCES `usertitle` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userrole`
--

DROP TABLE IF EXISTS `userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrole` (
  `UserId` varchar(128) NOT NULL,
  `RoleId` varchar(128) NOT NULL,
  PRIMARY KEY (`UserId`,`RoleId`),
  KEY `IdentityRole_Users` (`RoleId`),
  CONSTRAINT `ApplicationUser_Roles` FOREIGN KEY (`UserId`) REFERENCES `user` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `IdentityRole_Users` FOREIGN KEY (`RoleId`) REFERENCES `role` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usertitle`
--

DROP TABLE IF EXISTS `usertitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usertitle` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Name` varchar(128) NOT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visitorprofile`
--

DROP TABLE IF EXISTS `visitorprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitorprofile` (
  `ID` varchar(128) NOT NULL,
  `Name` varchar(128) NOT NULL,
  `Surname` varchar(128) NOT NULL,
  `TitleId` varchar(128) DEFAULT NULL,
  `Email` varchar(128) DEFAULT NULL,
  `ForeignInstitutionId` varchar(128) DEFAULT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `PicturePath` varchar(500) DEFAULT NULL,
  `TelephoneNumber` varchar(128) DEFAULT NULL,
  `Comment` longtext,
  PRIMARY KEY (`ID`),
  KEY `visitorprofile_usertitle` (`TitleId`),
  KEY `visitorprofile_foreigninstitution` (`ForeignInstitutionId`),
  CONSTRAINT `visitorprofile_foreigninstitution` FOREIGN KEY (`ForeignInstitutionId`) REFERENCES `foreigninstitution` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `visitorprofile_usertitle` FOREIGN KEY (`TitleId`) REFERENCES `usertitle` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visitorvisit`
--

DROP TABLE IF EXISTS `visitorvisit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitorvisit` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `VisitorProfileId` varchar(128) DEFAULT NULL,
  `DepartmentId` varchar(128) DEFAULT NULL,
  `AcademicYear` smallint(6) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `DurationId` varchar(128) DEFAULT NULL,
  `UserProfileId` varchar(128) DEFAULT NULL,
  `TravelFundingId` varchar(128) DEFAULT NULL,
  `FundingOfStayId` varchar(128) DEFAULT NULL,
  `VisitResult` longtext,
  `Comment` longtext,
  PRIMARY KEY (`ID`),
  KEY `visitorvisit_visitorprofile` (`VisitorProfileId`),
  KEY `visitorvisit_department` (`DepartmentId`),
  KEY `visitorvisit_duration` (`DurationId`),
  KEY `visitorvisit_userprofile` (`UserProfileId`),
  KEY `visitorvisit_travelfunding` (`TravelFundingId`),
  KEY `visitorvisit_fundingofstay` (`FundingOfStayId`),
  CONSTRAINT `visitorvisit_department` FOREIGN KEY (`DepartmentId`) REFERENCES `department` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `visitorvisit_duration` FOREIGN KEY (`DurationId`) REFERENCES `duration` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `visitorvisit_fundingofstay` FOREIGN KEY (`FundingOfStayId`) REFERENCES `funding` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `visitorvisit_travelfunding` FOREIGN KEY (`TravelFundingId`) REFERENCES `funding` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `visitorvisit_userprofile` FOREIGN KEY (`UserProfileId`) REFERENCES `userprofile` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `visitorvisit_visitorprofile` FOREIGN KEY (`VisitorProfileId`) REFERENCES `visitorprofile` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visitorvisittype`
--

DROP TABLE IF EXISTS `visitorvisittype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitorvisittype` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `VisitorVisitId` varchar(128) DEFAULT NULL,
  `VisitTypeId` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `visitorvisittype_visitorvisit` (`VisitorVisitId`),
  KEY `visitorvisittype_visittype` (`VisitTypeId`),
  CONSTRAINT `visitorvisittype_visitorvisit` FOREIGN KEY (`VisitorVisitId`) REFERENCES `visitorvisit` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `visitorvisittype_visittype` FOREIGN KEY (`VisitTypeId`) REFERENCES `visittype` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visitpurpose`
--

DROP TABLE IF EXISTS `visitpurpose`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitpurpose` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Type` varchar(128) NOT NULL,
  `Comment` longtext,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visittype`
--

DROP TABLE IF EXISTS `visittype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visittype` (
  `ID` varchar(128) NOT NULL DEFAULT '',
  `Type` varchar(128) NOT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-08 22:09:30
