﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Office.WebAPI.Models;

namespace Office.WebAPI
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is
    // used by the application.

    public class MyUserManager : UserManager<ApplicationUser>
    {
        #region Constructors

        public MyUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        #endregion Constructors

        #region Methods

        public static MyUserManager Create(IdentityFactoryOptions<MyUserManager> options, IOwinContext context)
        {
            // Allows cors for the /token endpoint this is different from webapi endpoints.
            if (context.Environment.Values.Contains("/Token"))
            {
                context.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            }

            var manager = new MyUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = false,
                RequireUppercase = false,
            };
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }

        #endregion Methods
    }
}