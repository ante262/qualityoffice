﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IUserProfileService
    {
        #region Methods

        Task<int> DeleteAsync(IUserProfile entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IUserProfile>> GetAsync();

        Task<IUserProfile> GetAsync(string ID);

        Task<int> InsertAsync(IUserProfile entity);

        Task<int> UpdateAsync(IUserProfile entity);

        #endregion Methods
    }
}