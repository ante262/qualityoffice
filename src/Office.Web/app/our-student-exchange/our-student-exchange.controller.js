(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('OurStudentExchangeController', ['$scope', 'ourStudentExchangeService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, ourStudentExchangeService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.ourStudentExchanges = [];
            vm.ourStudentExchange = {};
            vm.selectedOurStudentExchange = null;
            vm.loading = true;

            vm.addOurStudentExchange = function () {
                ngDialog.open({
                    template: 'app/our-student-exchange/add-our-student-exchange.html',
                    controller: 'AddOurStudentExchangeController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editOurStudentExchange = function (data) {
                ngDialog.open({
                    template: 'app/our-student-exchange/edit-our-student-exchange.html',
                    controller: 'EditOurStudentExchangeController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.showOurStudentExchange = function (data) {
                ngDialog.open({
                    template: 'app/our-student-exchange/show-our-student-exchange.html',
                    controller: 'ShowOurStudentExchangeController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
            };

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return ourStudentExchangeService.fetch()
                .error(function (data) {
                    if (data === null) {
                        $state.go('ourStudentExchange', { 'notification': notifications.entity.serverError });
                    };
                    console.log('Unable to get ourStudentExchange:' + data.message);
                    $state.go('ourStudentExchange', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.ourStudentExchanges = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedOurStudentExchange = item;
                ourStudentExchangeService.get(vm.selectedOurStudentExchange)
                .success(function (data) {
                    vm.ourStudentExchange = data;
                })
                .error(function (data) {
                    console.log("Unable to get this ourStudentExchange:" + data.message);
                });
            };

            vm.delete = function (data) {
                vm.ourStudentExchange = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Boravak studenta '" + vm.ourStudentExchange.Name + "' će biti obrisan!")) {
                    ourStudentExchangeService.delete(vm.ourStudentExchange.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);