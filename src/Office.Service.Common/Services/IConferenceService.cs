﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IConferenceService
    {
        #region Methods

        Task<int> DeleteAsync(IConference entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IConference>> GetAsync();

        Task<IConference> GetAsync(string ID);

        Task<int> InsertAsync(IConference entity);

        Task<int> UpdateAsync(IConference entity);

        #endregion Methods
    }
}