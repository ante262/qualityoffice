﻿using AutoMapper;
using Office.Model;
using Office.Model.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/CommitteeMember")]
    public class CommitteeMemberController : ApiController
    {
        #region Constructors

        public CommitteeMemberController(ICommitteeMemberService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private ICommitteeMemberService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/CommitteeMember/
        [HttpDelete]
        [Route("{ID}")]
        public async Task<IHttpActionResult> Delete(string ID)
        {
            try
            {
                var result = await Service.DeleteAsync(ID);
                if (result == 1) return Ok("Deleted");
                else return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        // GET: api/CommitteeMember
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<CommitteeMemberModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/CommitteeMember/5
        [HttpGet]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Get(string ID)
        {
            var result = await Service.GetAsync(ID);

            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<CommitteeMemberModel>(result));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        // POST: api/CommitteeMember
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post(CommitteeMemberModel entity)
        {
            try
            {
                var result = await Service.InsertAsync(Mapper.Map<ICommitteeMember>(entity));

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Add operation error.");

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/CommitteeMember/5
        [HttpPut]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Put(string ID, CommitteeMemberModel entity)
        {
            try
            {
                if (ID != entity.ID)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "IDs do not match.");
                }

                var result = await Service.UpdateAsync(Mapper.Map<ICommitteeMember>(entity));
                if (result > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "Failed.");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public partial class CommitteeMemberModel
        {
            #region Properties

            public string Comment { get; set; }
            public CommitteeModel Committee { get; set; }
            public string CommitteeId { get; set; }
            public CommitteeMemberRolePOCO CommitteeMemberRole { get; set; }
            public string CommitteeMemberRoleId { get; set; }
            public Nullable<System.DateTime> DateOfElection { get; set; }
            public ElectingInstitutionModel ElectingInstitution { get; set; }
            public string ElectingInstitutionId { get; set; }
            public string ID { get; set; }
            public string PeriodOfElection { get; set; }
            public string Title { get; set; }
            public UserProfileModel UserProfile { get; set; }
            public string UserProfileId { get; set; }

            #endregion Properties
        }

        public partial class CommitteeModel
        {
            #region Properties

            public string Name { get; set; }

            #endregion Properties
        }

        public partial class ElectingInstitutionModel
        {
            #region Properties

            public string Name { get; set; }

            #endregion Properties
        }

        public partial class UserProfileModel
        {
            #region Properties

            public string Name { get; set; }
            public string Surname { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}