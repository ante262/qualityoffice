(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowVisitorVisitController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.visitorVisit = $scope.ngDialogData;
            
        }]);
})(angular);