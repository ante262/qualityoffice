﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Office.Service
{
    public class OurStudentExchangeService : IOurStudentExchangeService
    {
        #region Constructors

        public OurStudentExchangeService(IRepository repository, ILookupTablesService lookupTablesService)
        {
            Repository = repository;
            LookupTablesService = lookupTablesService;
        }

        #endregion Constructors

        #region Properties

        protected ILookupTablesService LookupTablesService { get; private set; }

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IOurStudentExchange entity)
        {
            return Repository.DeleteAsync<ourstudentexchange>(Mapper.Map<ourstudentexchange>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<ourstudentexchange>(ID);
        }

        public async Task<List<IOurStudentExchange>> GetAsync()
        {
            // include referencing tables into OurStudentExchange table
            var result = await Repository.WhereAsync<ourstudentexchange>()
                          .Include(c => c.department)
                          .Include(c => c.duration)
                          .Include(c => c.exchangeorganization)
                          .Include(c => c.foreigninstitution)
                          .Include(c => c.funding)
                          .Include(c => c.funding1)
                          .Include(c => c.semester)
                          .Include(c => c.visitpurpose)
                          .ToListAsync<ourstudentexchange>();

            return Mapper.Map<List<IOurStudentExchange>>(result);
        }

        public async Task<IOurStudentExchange> GetAsync(string ID)
        {
            return Mapper.Map<IOurStudentExchange>(await Repository.SingleAsync<ourstudentexchange>(ID));
        }

        public Task<int> InsertAsync(IOurStudentExchange entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            var counter = 0;
            if (entity.Department != null && entity.Department.ID == null)
            {
                entity.Department.ID = Guid.NewGuid().ToString("N");
                entity.DepartmentId = entity.Department.ID;
                counter++;
            }
            if (entity.VisitPurpose != null && entity.VisitPurpose.ID == null)
            {
                entity.VisitPurpose.ID = Guid.NewGuid().ToString("N");
                entity.VisitPurposeId = entity.VisitPurpose.ID;
                counter++;
            }
            if (entity.Semester != null && entity.Semester.ID == null)
            {
                entity.Semester.ID = Guid.NewGuid().ToString("N");
                entity.SemesterId = entity.Semester.ID;
                counter++;
            }
            if (entity.Duration != null && entity.Duration.ID == null)
            {
                entity.Duration.ID = Guid.NewGuid().ToString("N");
                entity.DurationId = entity.Duration.ID;
                counter++;
            }
            if (entity.FundingOfStay != null && entity.FundingOfStay.ID == null)
            {
                entity.FundingOfStay.ID = Guid.NewGuid().ToString("N");
                entity.FundingOfStayId = entity.FundingOfStay.ID;
                counter++;
            }
            if (entity.TravelFunding != null && entity.TravelFunding.ID == null)
            {
                entity.TravelFunding.ID = Guid.NewGuid().ToString("N");
                entity.TravelFundingId = entity.TravelFunding.ID;
                counter++;
            }
            if (counter > 0)
            {
                LookupTablesService.ClearCache();
            }

            return Repository.InsertAsync<ourstudentexchange>(Mapper.Map<ourstudentexchange>(entity));
        }

        public async Task<int> UpdateAsync(IOurStudentExchange entity)
        {
            var lookupItems = await LookupTablesService.GetAsync();

            var counter = 0;
            var unitOfWork = Repository.CreateUnitOfWork();
            if (entity.Department != null
                && !lookupItems.Departments.Select(c => c.Name).Contains(entity.Department.Name))
            {
                entity.Department.ID = Guid.NewGuid().ToString("N");
                entity.DepartmentId = entity.Department.ID;
                var map = Mapper.Map<department>(entity.Department);
                await unitOfWork.AddAsync<department>(map);
                counter++;
            }
            if (entity.Department != null
                && entity.Department.ID == entity.DepartmentId
                && lookupItems.Departments
                .Select(c => c.Name.ToLower())
                .Contains(entity.Department.Name.ToLower()))
            {
                entity.DepartmentId = lookupItems.Departments
                    .Where(c => c.Name.ToLower() == entity.Department.Name.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.Duration != null
                && !lookupItems.Durations
                .Select(c => c.Length.ToLower())
                .Contains(entity.Duration.Length.ToLower()))
            {
                entity.Duration.ID = Guid.NewGuid().ToString("N");
                entity.DurationId = entity.Duration.ID;
                var map = Mapper.Map<duration>(entity.Duration);
                await unitOfWork.AddAsync<duration>(map);
                counter++;
            }
            if (entity.Duration != null
                && entity.Duration.ID == entity.DurationId
                && lookupItems.Durations
                .Select(c => c.Length.ToLower())
                .Contains(entity.Duration.Length.ToLower()))
            {
                entity.DurationId = lookupItems.Durations
                    .Where(c => c.Length.ToLower() == entity.Duration.Length.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.TravelFunding != null
                && !lookupItems.Fundings.Select(c => c.Type).Contains(entity.TravelFunding.Type))
            {
                entity.TravelFunding.ID = Guid.NewGuid().ToString("N");
                entity.TravelFundingId = entity.TravelFunding.ID;
                var map = Mapper.Map<funding>(entity.TravelFunding);
                await unitOfWork.AddAsync<funding>(map);
                counter++;
            }
            if (entity.TravelFunding != null
                && entity.TravelFunding.ID == entity.TravelFundingId
                && lookupItems.Fundings
                .Select(c => c.Type.ToLower())
                .Contains(entity.TravelFunding.Type.ToLower()))
            {
                entity.TravelFundingId = lookupItems.Fundings
                    .Where(c => c.Type.ToLower() == entity.TravelFunding.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.FundingOfStay != null
                && !lookupItems.Fundings.Select(c => c.Type).Contains(entity.FundingOfStay.Type))
            {
                entity.FundingOfStay.ID = Guid.NewGuid().ToString("N");
                entity.FundingOfStayId = entity.FundingOfStay.ID;
                var map = Mapper.Map<funding>(entity.FundingOfStay);
                await unitOfWork.AddAsync<funding>(map);
                counter++;
            }
            if (entity.FundingOfStay != null
                && entity.FundingOfStay.ID == entity.FundingOfStayId
                && lookupItems.Fundings
                .Select(c => c.Type.ToLower())
                .Contains(entity.FundingOfStay.Type.ToLower()))
            {
                entity.FundingOfStayId = lookupItems.Fundings
                    .Where(c => c.Type.ToLower() == entity.FundingOfStay.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.VisitPurpose != null
                && !lookupItems.VisitPurposes
                .Select(c => c.Type.ToLower())
                .Contains(entity.VisitPurpose.Type.ToLower()))
            {
                entity.VisitPurpose.ID = Guid.NewGuid().ToString("N");
                entity.VisitPurposeId = entity.VisitPurpose.ID;
                var map = Mapper.Map<visitpurpose>(entity.VisitPurpose);
                await unitOfWork.AddAsync<visitpurpose>(map);
                counter++;
            }
            if (entity.VisitPurpose != null
                && entity.VisitPurpose.ID == entity.VisitPurposeId
                && lookupItems.VisitPurposes
                .Select(c => c.Type.ToLower())
                .Contains(entity.VisitPurpose.Type.ToLower()))
            {
                entity.VisitPurposeId = lookupItems.VisitPurposes
                    .Where(c => c.Type.ToLower() == entity.VisitPurpose.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.Semester != null
                && !lookupItems.Semesters
                .Select(c => c.Type.ToLower())
                .Contains(entity.Semester.Type.ToLower()))
            {
                entity.Semester.ID = Guid.NewGuid().ToString("N");
                entity.SemesterId = entity.Semester.ID;
                var map = Mapper.Map<semester>(entity.Semester);
                await unitOfWork.AddAsync<semester>(map);
                counter++;
            }
            if (entity.Semester != null
                && entity.Semester.ID == entity.SemesterId
                && lookupItems.Semesters
                .Select(c => c.Type.ToLower())
                .Contains(entity.Semester.Type.ToLower()))
            {
                entity.SemesterId = lookupItems.Semesters
                    .Where(c => c.Type.ToLower() == entity.Semester.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }

            entity.Department = null;
            entity.Duration = null;
            entity.TravelFunding = null;
            entity.FundingOfStay = null;
            entity.VisitPurpose = null;
            entity.Semester = null;
            await unitOfWork.UpdateAsync<ourstudentexchange>(Mapper.Map<ourstudentexchange>(entity));
            if (counter > 0)
            {
                await LookupTablesService.ClearCache();
            }
            return await unitOfWork.CommitAsync();
        }

        #endregion Methods
    }
}