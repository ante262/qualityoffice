(function (angular) {
    'use strict';
    angular.module('officeApp').controller('EditContractController', ['$scope', 'contractService', 'lookupTablesService', 'notifications', '$window', '$state', '$stateParams',
        function ($scope, contractService, lookupTablesService, notifications, $window, $state, $stateParams) {
            var vm = $scope.vm = {};
            vm.contract = $scope.ngDialogData;
            if (vm.contract && vm.contract.StartDate) {
                vm.contract.StartDate = new Date(vm.contract.StartDate);
            }
            vm.lookups = [];
            vm.check = {
                type: false,
                duration: false,
                funding: false
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('ContractTypes, Durations, Fundings')
                    .then(function (data) {
                        vm.lookups = data;
                    },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchLookups();
            }
            vm.fetch();

            vm.put = function (item) {
                if (item.ContractType != null || item.Duration != null ||
                    item.Funding != null) {
                    lookupTablesService.clearCache();
                };

                contractService.put(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                        $state.go('contract', { 'notification': notifications.entity.editError });
                    });
            };
        }]);
})(angular);