﻿using Office.Model.Common;
using System;

namespace Office.Model
{
    public partial class ContactPOCO : IContact
    {
        #region Properties

        public string Abbreviation { get; set; }
        public string Email { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string TelephoneNumber { get; set; }

        #endregion Properties
    }
}