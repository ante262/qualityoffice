using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class funding
    {
        public funding()
        {
            this.contracts = new List<contract>();
            this.foreignstudentexchanges = new List<foreignstudentexchange>();
            this.foreignstudentexchanges1 = new List<foreignstudentexchange>();
            this.ourprofessorvisits = new List<ourprofessorvisit>();
            this.ourprofessorvisits1 = new List<ourprofessorvisit>();
            this.ourstudentexchanges = new List<ourstudentexchange>();
            this.ourstudentexchanges1 = new List<ourstudentexchange>();
            this.visitorvisits = new List<visitorvisit>();
            this.visitorvisits1 = new List<visitorvisit>();
        }

        public string ID { get; set; }
        public string Type { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<contract> contracts { get; set; }
        public virtual ICollection<foreignstudentexchange> foreignstudentexchanges { get; set; }
        public virtual ICollection<foreignstudentexchange> foreignstudentexchanges1 { get; set; }
        public virtual ICollection<ourprofessorvisit> ourprofessorvisits { get; set; }
        public virtual ICollection<ourprofessorvisit> ourprofessorvisits1 { get; set; }
        public virtual ICollection<ourstudentexchange> ourstudentexchanges { get; set; }
        public virtual ICollection<ourstudentexchange> ourstudentexchanges1 { get; set; }
        public virtual ICollection<visitorvisit> visitorvisits { get; set; }
        public virtual ICollection<visitorvisit> visitorvisits1 { get; set; }
    }
}
