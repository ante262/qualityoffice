(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddExchangeOrganizationController', ['$scope', 'exchangeOrganizationService', 'notifications', '$window', '$state',
        function ($scope, exchangeOrganizationService, notifications, $window, $state) {
            var vm = $scope.vm = {};
            vm.exchangeOrganization = {};

            vm.post = function (item) {
                exchangeOrganizationService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log('error', data);
                    });
            };
        }]);
})(angular);