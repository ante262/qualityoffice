(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ForeignStudentController', ['$scope', 'foreignStudentService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, foreignStudentService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.foreignStudents = [];
            vm.foreignStudent = {};
            vm.selectedForeignStudent = null;
            vm.loading = true;

            vm.addForeignStudent = function () {
                ngDialog.open({
                    template: 'app/foreign-student/add-foreign-student.html',
                    controller: 'AddForeignStudentController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editForeignStudent = function (data) {
                ngDialog.open({
                    template: 'app/foreign-student/edit-foreign-student.html',
                    controller: 'EditForeignStudentController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.showForeignStudent = function (data) {
                ngDialog.open({
                    template: 'app/foreign-student/show-foreign-student.html',
                    controller: 'ShowForeignStudentController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            };

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return foreignStudentService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('foreignStudent', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get foreignStudent:' + data.message);
                    $state.go('foreignStudent', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.foreignStudents = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedForeignStudent = item;
                foreignStudentService.get(vm.selectedForeignStudent).success(function (data) {
                    vm.foreignStudent = data;
                }).error(function (data) {
                    console.log("Unable to get this foreignStudent:" + data.message);
                });
            };

            vm.delete = function (data) {
                vm.foreignStudent = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Student '" + vm.foreignStudent.Name + " " + vm.foreignStudent.Surname + "' će biti obrisan!")) {
                    foreignStudentService.delete(vm.foreignStudent.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);