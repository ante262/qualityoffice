﻿(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('EditConferenceTypeController', ['$scope', 'conferenceTypeService', 'notifications',
        function ($scope, conferenceTypeService, notifications) {
            var vm = $scope.vm = {};
            vm.conferenceType = $scope.ngDialogData;

            vm.put = function (item) {
                vm.conferenceType.ID = item.ID;
                vm.conferenceType.Name = item.Name;
                vm.conferenceType.EnglishName = item.EnglishName;
                vm.conferenceType.URL = item.URL;
                vm.conferenceType.Abbreviation = item.Abbreviation;

                conferenceTypeService.put(vm.conferenceType)
                    .success(function (data) {
                        $scope.closeThisDialog();
                    })
                    .error(function (data) {
                        console.log(data);
                    });
            };
        }]);
})(angular);