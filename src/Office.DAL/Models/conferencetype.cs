using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class conferencetype
    {
        public conferencetype()
        {
            this.conferences = new List<conference>();
            this.nonpublicationlectures = new List<nonpublicationlecture>();
        }

        public string ID { get; set; }
        public string Type { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<conference> conferences { get; set; }
        public virtual ICollection<nonpublicationlecture> nonpublicationlectures { get; set; }
    }
}
