﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IContactService
    {
        #region Methods

        Task<int> DeleteAsync(IContact entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IContact>> GetAsync();

        Task<IContact> GetAsync(string ID);

        Task<int> InsertAsync(IContact entity);

        Task<int> UpdateAsync(IContact entity);

        #endregion Methods
    }
}