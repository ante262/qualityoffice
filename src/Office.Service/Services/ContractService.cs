﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Office.Service
{
    public class ContractService : IContractService
    {
        #region Constructors

        public ContractService(IRepository repository, ILookupTablesService lookupTablesService)
        {
            Repository = repository;
            LookupTablesService = lookupTablesService;
        }

        #endregion Constructors

        #region Properties

        protected ILookupTablesService LookupTablesService { get; private set; }

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IContract entity)
        {
            return Repository.DeleteAsync<contract>(Mapper.Map<contract>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<contract>(ID);
        }

        public async Task<List<IContract>> GetAsync()
        {
            // include ContractType, Duration and Funding table into Contract table
            var result = await Repository.WhereAsync<contract>()
                          .Include(c => c.contracttype)
                          .Include(d => d.duration)
                          .Include(f => f.funding)
                          .ToListAsync<contract>();

            return Mapper.Map<List<IContract>>(result);
        }

        public async Task<IContract> GetAsync(string ID)
        {
            return Mapper.Map<IContract>(await Repository.SingleAsync<contract>(ID));
        }

        public Task<int> InsertAsync(IContract entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            var counter = 0;
            if (entity.ContractType != null && entity.ContractType.ID == null)
            {
                entity.ContractType.ID = Guid.NewGuid().ToString("N");
                entity.ContractTypeId = entity.ContractType.ID;
                counter++;
            }
            if (entity.Duration != null && entity.Duration.ID == null)
            {
                entity.Duration.ID = Guid.NewGuid().ToString("N");
                entity.DurationId = entity.Duration.ID;
                counter++;
            }
            if (entity.Funding != null && entity.Funding.ID == null)
            {
                entity.Funding.ID = Guid.NewGuid().ToString("N");
                entity.FundingTypeId = entity.Funding.ID;
                counter++;
            }
            if (counter > 0)
            {
                LookupTablesService.ClearCache();
            }

            return Repository.InsertAsync<contract>(Mapper.Map<contract>(entity));
        }

        public async Task<int> UpdateAsync(IContract entity)
        {
            var lookupItems = await LookupTablesService.GetAsync();

            var counter = 0;
            var unitOfWork = Repository.CreateUnitOfWork();
            if (entity.ContractType != null
                && !lookupItems.ContractTypes.Select(c => c.Type).Contains(entity.ContractType.Type))
            {
                entity.ContractType.ID = Guid.NewGuid().ToString("N");
                entity.ContractTypeId = entity.ContractType.ID;
                var map = Mapper.Map<contracttype>(entity.ContractType);
                await unitOfWork.AddAsync<contracttype>(map);
                counter++;
            }
            if (entity.ContractType != null
                && entity.ContractType.ID == entity.ContractTypeId
                && lookupItems.ContractTypes
                .Select(c => c.Type.ToLower())
                .Contains(entity.ContractType.Type.ToLower()))
            {
                entity.ContractTypeId = lookupItems.ContractTypes
                    .Where(c => c.Type.ToLower() == entity.ContractType.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.Duration != null
                && !lookupItems.Durations
                .Select(c => c.Length.ToLower())
                .Contains(entity.Duration.Length.ToLower()))
            {
                entity.Duration.ID = Guid.NewGuid().ToString("N");
                entity.DurationId = entity.Duration.ID;
                var map = Mapper.Map<duration>(entity.Duration);
                await unitOfWork.AddAsync<duration>(map);
                counter++;
            }
            if (entity.Duration != null
                && entity.Duration.ID == entity.DurationId
                && lookupItems.Durations
                .Select(c => c.Length.ToLower())
                .Contains(entity.Duration.Length.ToLower()))
            {
                entity.DurationId = lookupItems.Durations
                    .Where(c => c.Length.ToLower() == entity.Duration.Length.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.Funding != null
                && !lookupItems.Fundings.Select(c => c.Type).Contains(entity.Funding.Type))
            {
                entity.Funding.ID = Guid.NewGuid().ToString("N");
                entity.FundingTypeId = entity.Funding.ID;
                var map = Mapper.Map<funding>(entity.Funding);
                await unitOfWork.AddAsync<funding>(map);
                counter++;
            }
            if (entity.Funding != null
                && entity.Funding.ID == entity.FundingTypeId
                && lookupItems.Fundings
                .Select(c => c.Type.ToLower())
                .Contains(entity.Funding.Type.ToLower()))
            {
                entity.FundingTypeId = lookupItems.Fundings
                    .Where(c => c.Type.ToLower() == entity.Funding.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }

            entity.ContractType = null;
            entity.Duration = null;
            entity.Funding = null;
            await unitOfWork.UpdateAsync<contract>(Mapper.Map<contract>(entity));
            if (counter > 0)
            {
                await LookupTablesService.ClearCache();
            }
            return await unitOfWork.CommitAsync();
        }

        #endregion Methods
    }
}