﻿using System;

namespace Office.Model.Common
{
    public interface IRole
    {
        #region Properties

        string Description { get; set; }
        string ID { get; set; }
        string Name { get; set; }

        #endregion Properties
    }
}