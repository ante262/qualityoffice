using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class my_aspnet_sitemapMap : EntityTypeConfiguration<my_aspnet_sitemap>
    {
        public my_aspnet_sitemapMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Title)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(512);

            this.Property(t => t.Url)
                .HasMaxLength(512);

            this.Property(t => t.Roles)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("my_aspnet_sitemap", "officeapp");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Url).HasColumnName("Url");
            this.Property(t => t.Roles).HasColumnName("Roles");
            this.Property(t => t.ParentId).HasColumnName("ParentId");
        }
    }
}
