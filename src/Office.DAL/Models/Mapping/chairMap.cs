using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class chairMap : EntityTypeConfiguration<chair>
    {
        public chairMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.EnglishName)
                .HasMaxLength(128);

            this.Property(t => t.URL)
                .HasMaxLength(250);

            this.Property(t => t.Abbreviation)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("chair", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.EnglishName).HasColumnName("EnglishName");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.Abbreviation).HasColumnName("Abbreviation");
        }
    }
}
