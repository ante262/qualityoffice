﻿using Office.Model.Common;
using System;
using System.Collections.Generic;

namespace Office.Model
{
    public partial class VisitorVisitPOCO : IVisitorVisit
    {
        #region Properties

        public Nullable<short> AcademicYear { get; set; }
        public string Comment { get; set; }
        public IDepartment Department { get; set; }
        public string DepartmentId { get; set; }
        public IDuration Duration { get; set; }
        public string DurationId { get; set; }
        public IFunding FundingOfStay { get; set; }
        public string FundingOfStayId { get; set; }
        public string ID { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public IFunding TravelFunding { get; set; }
        public string TravelFundingId { get; set; }
        public IUserProfile UserProfile { get; set; }
        public string UserProfileId { get; set; }
        public IVisitorProfile VisitorProfile { get; set; }
        public string VisitorProfileId { get; set; }
        public IList<IVisitorVisitType> VisitorVisitTypes { get; set; }
        public string VisitResult { get; set; }

        #endregion Properties
    }
}