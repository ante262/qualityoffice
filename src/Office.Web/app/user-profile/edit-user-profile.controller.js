(function (angular) {
    'use strict';
    angular.module('officeApp').controller('EditUserProfileController', ['$scope', 'userProfileService', 'lookupTablesService', 'notifications', '$window', '$state', '$stateParams',
        function ($scope, userProfileService, lookupTablesService, notifications, $window, $state, $stateParams) {
            var vm = $scope.vm = {};
            vm.userProfile = $scope.ngDialogData;
            vm.lookups = [];
            vm.check = {
                title: false,
                chair: false,
                group: false,
                rank: false
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('UserTitles, Chairs, UserAcademicRanks, ResearchGroups')
                    .then(function (data) {
                        vm.lookups = data;
                    },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchLookups();
            }
            vm.fetch();

            vm.put = function (item) {
                if (item.UserTitle || item.UserAcademicRank || item.ResearchGroup || item.Chair) {
                    lookupTablesService.clearCache();
                };

                userProfileService.put(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                        $state.go('userProfile', { 'notification': notifications.entity.editError });
                    });
            };
        }]);
})(angular);