using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class contract
    {
        public contract()
        {
            this.foreigninstitutions = new List<foreigninstitution>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string ContractTypeId { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public string DurationId { get; set; }
        public string FundingTypeId { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public string Comment { get; set; }
        public virtual contracttype contracttype { get; set; }
        public virtual duration duration { get; set; }
        public virtual funding funding { get; set; }
        public virtual ICollection<foreigninstitution> foreigninstitutions { get; set; }
    }
}
