﻿(function (angular) {
    'use strict';
    angular.module('officeApp').controller('LoginUserController', ['$scope', 'authorizationService', 'notifications', '$state', '$stateParams',
        function ($scope, authorizationService, notifications, $state, $stateParams) {
            var vm = $scope.vm = {};
            vm.loginUser = {};

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            }

            vm.login = function (item) {
                vm.fadeClass = 'is-hidden';
                vm.loading = true;
                authorizationService.login(item).then(
                    function (data) {
                        $state.go('home');
                    },
                    function (error) {
                        vm.loading = false;
                        vm.fadeClass = "";
                        $state.go('login', { 'notification': notifications.entity.loginError });
                    });
            };
        }]);
})(angular);