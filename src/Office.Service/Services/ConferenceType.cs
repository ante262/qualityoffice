﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Office.Service
{
    public class ConferenceTypeService : IConferenceTypeService
    {
        #region Constructors

        public ConferenceTypeService(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IConferenceType entity)
        {
            return Repository.DeleteAsync<conferencetype>(Mapper.Map<conferencetype>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<conferencetype>(ID);
        }

        public async Task<List<IConferenceType>> GetAsync()
        {
            return Mapper.Map<List<IConferenceType>>(
                await Repository.WhereAsync<conferencetype>()
                        .ToListAsync<conferencetype>()
                );
        }

        public async Task<IConferenceType> GetAsync(string ID)
        {
            return Mapper.Map<IConferenceType>(await Repository.SingleAsync<conferencetype>(ID));
        }

        public Task<int> InsertAsync(IConferenceType entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            return Repository.InsertAsync<conferencetype>(Mapper.Map<conferencetype>(entity));
        }

        public Task<int> UpdateAsync(IConferenceType entity)
        {
            return Repository.UpdateAsync<conferencetype>(Mapper.Map<conferencetype>(entity));
        }

        #endregion Methods
    }
}