(function (angular) {
    'use strict';
    angular.module('officeApp').controller('EditExchangeOrganizationController', ['$scope', 'exchangeOrganizationService', 'notifications', '$window', '$state', '$stateParams',
        function ($scope, exchangeOrganizationService, notifications, $window, $state, $stateParams) {
            var vm = $scope.vm = {};
            vm.exchangeOrganization = $scope.ngDialogData;

            vm.put = function (item) {
                exchangeOrganizationService.put(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                        $state.go('exchangeOrganization', { 'notification': notifications.entity.editError });
                    });
            };
        }]);
})(angular);