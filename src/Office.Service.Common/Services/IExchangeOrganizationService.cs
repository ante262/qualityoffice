﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IExchangeOrganizationService
    {
        #region Methods

        Task<int> DeleteAsync(IExchangeOrganization entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IExchangeOrganization>> GetAsync();

        Task<IExchangeOrganization> GetAsync(string ID);

        Task<int> InsertAsync(IExchangeOrganization entity);

        Task<int> UpdateAsync(IExchangeOrganization entity);

        #endregion Methods
    }
}