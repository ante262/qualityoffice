(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ContactController', ['$scope', 'contactService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, contactService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.contacts = [];
            vm.contact = {};
            vm.selectedContact = null;
            vm.loading = true;

            vm.addContact = function () {
                ngDialog.open({
                    template: 'app/contact/add-contact.html',
                    controller: 'AddContactController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editContact = function (data) {
                ngDialog.open({
                    template: 'app/contact/edit-contact.html',
                    controller: 'EditContactController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            }

            vm.showContact = function (data) {
                ngDialog.open({
                    template: 'app/contact/show-contact.html',
                    controller: 'ShowContactController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            }

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return contactService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('contact', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get contact:' + data.message);
                    $state.go('contact', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.contacts = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedContact = item;
                contactService.get(vm.selectedContact)
                    .success(function (data) {
                        vm.contact = data;
                    }).error(function (data) {
                        console.log("Unable to get this contact:" + data.message);
                    });
            };

            vm.delete = function (data) {
                vm.contact = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Kontakt '" + vm.contact.Name + "' će biti obrisan!")) {
                    contactService.delete(vm.contact.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);