﻿(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ShowRoleController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.role = $scope.ngDialogData;
        }]);
})(angular);