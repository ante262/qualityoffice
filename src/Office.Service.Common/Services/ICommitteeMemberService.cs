﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface ICommitteeMemberService
    {
        #region Methods

        Task<int> DeleteAsync(ICommitteeMember entity);

        Task<int> DeleteAsync(string ID);

        Task<List<ICommitteeMember>> GetAsync();

        Task<ICommitteeMember> GetAsync(string ID);

        Task<int> InsertAsync(ICommitteeMember entity);

        Task<int> UpdateAsync(ICommitteeMember entity);

        #endregion Methods
    }
}