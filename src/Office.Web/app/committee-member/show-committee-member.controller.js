(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowCommitteeMemberController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.committeeMember = $scope.ngDialogData;
        }]);
})(angular);