(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowOurStudentExchangeController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.ourStudentExchange = $scope.ngDialogData;
        }]);
})(angular);