﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IRoleService
    {
        #region Methods

        Task<int> DeleteAsync(IRole entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IRole>> GetAsync();

        Task<IRole> GetAsync(string ID);

        Task<int> InsertAsync(IRole entity);

        Task<int> UpdateAsync(IRole entity);

        #endregion Methods
    }
}