﻿using System;

namespace Office.Model.Common
{
    public interface IContact
    {
        #region Properties

        string Abbreviation { get; set; }
        string Email { get; set; }
        string ID { get; set; }
        string Name { get; set; }
        string Surname { get; set; }
        string TelephoneNumber { get; set; }

        #endregion Properties
    }
}