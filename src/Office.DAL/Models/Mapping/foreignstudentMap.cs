using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class foreignstudentMap : EntityTypeConfiguration<foreignstudent>
    {
        public foreignstudentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Surname)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.City)
                .HasMaxLength(128);

            this.Property(t => t.State)
                .HasMaxLength(128);

            this.Property(t => t.TelephoneNumber)
                .HasMaxLength(128);

            this.Property(t => t.Email)
                .HasMaxLength(128);

            this.Property(t => t.Comment)
                .HasMaxLength(1073741823);

            this.Property(t => t.Abbreviation)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("foreignstudent", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Surname).HasColumnName("Surname");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.TelephoneNumber).HasColumnName("TelephoneNumber");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.Abbreviation).HasColumnName("Abbreviation");
        }
    }
}
