﻿(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddRoleController', ['$scope', 'roleService', 'notifications', '$window', '$state',
        function ($scope, roleService, notifications, $window, $state) {
            var vm = $scope.vm = {};
            vm.role = {};

            vm.post = function (item) {
                roleService.post(item)
                    .success(function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(data);
                    });
            };
        }]);
})(angular);