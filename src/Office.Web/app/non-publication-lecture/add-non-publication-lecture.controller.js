(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddNonPublicationLectureController', ['$scope', 'nonPublicationLectureService', 'lookupTablesService', 'foreignInstitutionService', 'notifications', '$window', '$state',
        function ($scope, nonPublicationLectureService, lookupTablesService, foreignInstitutionService, notifications, $window, $state) {
            var vm = $scope.vm = {};
            vm.nonPublicationLecture = {};
            vm.lookups = [];
            vm.foreignInstitutions = [];
            vm.profession = [{ id: 1, Type: "Znanstveno" }, { id: 2, Type: "Stručno" }];
            vm.check = {
                type: false,
                institution: false
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('ConferenceTypes').then(function (data) {
                    vm.lookups = data;
                },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };
            vm.fetchLookups();

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchForeignInstitutions = function () {
                return foreignInstitutionService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.foreignInstitutions = data;
                    });
            };

            vm.fetch = function () {
                vm.fetchLookups();
                vm.fetchForeignInstitutions();
            }
            vm.fetch();

            vm.post = function (item) {
                if (item.ConferenceType != null) {
                    lookupTablesService.clearCache();
                };

                nonPublicationLectureService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log('error', data);
                    });
            };
        }]);
})(angular);