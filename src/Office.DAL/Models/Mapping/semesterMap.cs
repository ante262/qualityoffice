using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class semesterMap : EntityTypeConfiguration<semester>
    {
        public semesterMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Type)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.Abbreviation)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("semester", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Abbreviation).HasColumnName("Abbreviation");
        }
    }
}
