﻿namespace Office.Repository.Common
{
    public interface IRepositoryFactory
    {
        #region Methods

        IRoleRepository CreateRoleRepository();

        IUserRepository CreateUserRepository();

        #endregion Methods
    }
}