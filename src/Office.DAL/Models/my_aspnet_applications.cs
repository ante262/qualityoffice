using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class my_aspnet_applications
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}
