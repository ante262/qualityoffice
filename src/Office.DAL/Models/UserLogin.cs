using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class userlogin
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public string UserId { get; set; }
        public virtual user user { get; set; }
    }
}
