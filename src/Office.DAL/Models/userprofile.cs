using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class userprofile
    {
        public userprofile()
        {
            this.committeemembers = new List<committeemember>();
            this.foreigninstitutions = new List<foreigninstitution>();
            this.foreignstudentexchanges = new List<foreignstudentexchange>();
            this.ourprofessorvisits = new List<ourprofessorvisit>();
            this.visitorvisits = new List<visitorvisit>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string TitleId { get; set; }
        public string AcademicRankId { get; set; }
        public string URL { get; set; }
        public string PicturePath { get; set; }
        public string ResearchGroupId { get; set; }
        public string ChairId { get; set; }
        public string Room { get; set; }
        public Nullable<sbyte> Floor { get; set; }
        public string TelephoneNumber { get; set; }
        public string Comment { get; set; }
        public string Email { get; set; }
        public virtual chair chair { get; set; }
        public virtual ICollection<committeemember> committeemembers { get; set; }
        public virtual ICollection<foreigninstitution> foreigninstitutions { get; set; }
        public virtual ICollection<foreignstudentexchange> foreignstudentexchanges { get; set; }
        public virtual ICollection<ourprofessorvisit> ourprofessorvisits { get; set; }
        public virtual researchgroup researchgroup { get; set; }
        public virtual useracademicrank useracademicrank { get; set; }
        public virtual usertitle usertitle { get; set; }
        public virtual ICollection<visitorvisit> visitorvisits { get; set; }
    }
}
