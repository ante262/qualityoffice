﻿using System;

namespace Office.Model.Common
{
    public interface IProfessorVisitType
    {
        #region Properties

        string ID { get; set; }
        string OurProfessorVisitId { get; set; }
        IVisitType VisitType { get; set; }
        string VisitTypeId { get; set; }

        #endregion Properties
    }
}