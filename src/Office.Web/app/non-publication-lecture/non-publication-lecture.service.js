(function (angular) {
    'use strict';
    angular.module('officeApp').service('nonPublicationLectureService', ['$http', 'baseUrl','$httpParamSerializerJQLike',
        function ($http, baseUrl, $httpParamSerializerJQLike) {
            var tUrl = "NonPublicationLecture";

            return {
                //get all nonPublicationLectures
                fetch: function () {
                    return $http.get(baseUrl + tUrl);
                },
                //get nonPublicationLecture by id
                get: function (id) {
                    return $http.get(baseUrl + tUrl + "/" + id);
                },
                //post nonPublicationLecture
                post: function (data) {
                    var req = {
                        method: 'POST',
                        url: baseUrl + tUrl,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //put nonPublicationLecture
                put: function (data) {
                    var req = {
                        method: 'PUT',
                        url: baseUrl + tUrl + "/" + data.ID,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //delete nonPublicationLecture
                delete: function (item) {
                    var req = {
                        method: 'DELETE',
                        url: baseUrl + tUrl + "/" + item,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    };
                    return $http(req);
                }
            };
        }]);
})(angular);