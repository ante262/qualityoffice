using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class my_aspnet_personalizationallusersMap : EntityTypeConfiguration<my_aspnet_personalizationallusers>
    {
        public my_aspnet_personalizationallusersMap()
        {
            // Primary Key
            this.HasKey(t => t.pathId);

            // Properties
            this.Property(t => t.pathId)
                .IsRequired()
                .HasMaxLength(36);

            this.Property(t => t.pageSettings)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("my_aspnet_personalizationallusers", "officeapp");
            this.Property(t => t.pathId).HasColumnName("pathId");
            this.Property(t => t.pageSettings).HasColumnName("pageSettings");
            this.Property(t => t.lastUpdatedDate).HasColumnName("lastUpdatedDate");
        }
    }
}
