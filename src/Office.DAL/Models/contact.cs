using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class contact
    {
        public contact()
        {
            this.foreigninstitutions = new List<foreigninstitution>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string TelephoneNumber { get; set; }
        public string Email { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<foreigninstitution> foreigninstitutions { get; set; }
    }
}
