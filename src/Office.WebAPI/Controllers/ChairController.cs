﻿using AutoMapper;
using Office.Model.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/Chair")]
    public class ChairController : ApiController
    {
        #region Constructors

        public ChairController(IChairService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private IChairService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/Chair/
        [HttpDelete]
        [Route("{ID}")]
        public async Task<IHttpActionResult> Delete(string ID)
        {
            try
            {
                var result = await Service.DeleteAsync(ID);
                if (result == 1) return Ok("Deleted");
                else return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        // GET: api/Chair
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<ChairModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/Chair/5
        [HttpGet]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Get(string ID)
        {
            var result = await Service.GetAsync(ID);

            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<ChairModel>(result));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        // POST: api/Chair
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post(ChairModel entity)
        {
            try
            {
                var result = await Service.InsertAsync(Mapper.Map<IChair>(entity));

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Add operation error.");

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/Chair/5
        [HttpPut]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Put(string ID, ChairModel entity)
        {
            try
            {
                if (ID != entity.ID)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "IDs do not match.");
                }

                var result = await Service.UpdateAsync(Mapper.Map<IChair>(entity));
                if (result == 1)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "Failed.");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public partial class ChairModel
        {
            #region Properties

            public string Abbreviation { get; set; }
            public string EnglishName { get; set; }
            public string ID { get; set; }
            public string Name { get; set; }
            public string URL { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}