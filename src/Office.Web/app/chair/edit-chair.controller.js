﻿(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('EditChairController', ['$scope', 'chairService', 'notifications',
        function ($scope, chairService, notifications) {
            var vm = $scope.vm = {};
            vm.chair = $scope.ngDialogData;

            vm.put = function (item) {
                vm.chair.ID = item.ID;
                vm.chair.Name = item.Name;
                vm.chair.EnglishName = item.EnglishName;
                vm.chair.URL = item.URL;
                vm.chair.Abbreviation = item.Abbreviation;

                chairService.put(vm.chair)
                    .success(function (data) {
                        $scope.closeThisDialog();
                    })
                    .error(function (data) {
                        console.log(data);
                    });
            };
        }]);
})(angular);