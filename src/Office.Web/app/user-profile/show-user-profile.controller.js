(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowUserProfileController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.userProfile = $scope.ngDialogData;
        }]);
})(angular);