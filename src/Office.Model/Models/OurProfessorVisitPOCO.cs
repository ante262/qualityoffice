﻿using Office.Model.Common;
using System;
using System.Collections.Generic;

namespace Office.Model
{
    public partial class OurProfessorVisitPOCO : IOurProfessorVisit
    {
        #region Properties

        public Nullable<short> AcademicYear { get; set; }
        public string Comment { get; set; }
        public string ConferenceTitle { get; set; }
        public IDepartment Department { get; set; }
        public string DepartmentId { get; set; }
        public IDuration Duration { get; set; }
        public string DurationId { get; set; }
        public IForeignInstitution ForeignInstitution { get; set; }
        public string ForeignInstitutionId { get; set; }
        public IFunding FundingOfStay { get; set; }
        public string FundingOfStayId { get; set; }
        public string ID { get; set; }
        public IList<IProfessorVisitType> ProfessorVisitTypes { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public IFunding TravelFunding { get; set; }
        public string TravelFundingId { get; set; }
        public string URL { get; set; }
        public IUserProfile UserProfile { get; set; }
        public string UserProfileId { get; set; }
        public string VisitResult { get; set; }

        #endregion Properties
    }
}