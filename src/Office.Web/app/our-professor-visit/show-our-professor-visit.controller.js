(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowOurProfessorVisitController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.ourProfessorVisit = $scope.ngDialogData;
            
        }]);
})(angular);