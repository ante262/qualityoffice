﻿(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ShowChairController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.chair = $scope.ngDialogData;
        }]);
})(angular);