using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class professorvisittype
    {
        public string ID { get; set; }
        public string OurProfessorVisitId { get; set; }
        public string VisitTypeId { get; set; }
        public virtual ourprofessorvisit ourprofessorvisit { get; set; }
        public virtual visittype visittype { get; set; }
    }
}
