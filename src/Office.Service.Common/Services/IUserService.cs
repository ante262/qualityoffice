﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IUserService
    {
        #region Methods

        Task<int> DeleteAsync(IUser entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IUser>> GetAsync();

        Task<IUser> GetAsync(string ID);

        #endregion Methods
    }
}