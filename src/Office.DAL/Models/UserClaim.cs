using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class userclaim
    {
        public int ID { get; set; }
        public string UserId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public virtual user user { get; set; }
    }
}
