﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Office.Service
{
    public class VisitorVisitService : IVisitorVisitService
    {
        #region Constructors

        public VisitorVisitService(IRepository repository, ILookupTablesService lookupTablesService)
        {
            Repository = repository;
            LookupTablesService = lookupTablesService;
        }

        #endregion Constructors

        #region Properties

        protected ILookupTablesService LookupTablesService { get; private set; }

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public async Task<int> AddChoiceRangeForDeleteAsync(IUnitOfWork unitOfWork, string[] ids)
        {
            var result = 0;
            foreach (var id in ids)
            {
                result += await unitOfWork.DeleteAsync<visitorvisittype>(id);
            }
            return result;
        }

        public async Task<int> AddChoiceRangeForInsertAsync(IUnitOfWork unitOfWork, string visitorVisitId, IVisitorVisitType[] entities)
        {
            var result = 0;
            var newCollection = entities.Where(c => c.ID != null);
            var oldCollection = entities.Where(c => c.VisitTypeId != null);
            foreach (var element in newCollection)
            {
                var visitTypeItem = new visittype();
                visitTypeItem.ID = Guid.NewGuid().ToString("N");
                visitTypeItem.Type = element.ID;
                result += await unitOfWork.AddAsync<visittype>(Mapper.Map<visittype>(visitTypeItem));
                element.ID = Guid.NewGuid().ToString("N");
                element.VisitorVisitId = visitorVisitId;
                element.VisitTypeId = visitTypeItem.ID;
            }
            foreach (var element in oldCollection)
            {
                element.ID = Guid.NewGuid().ToString("N");
                element.VisitorVisitId = visitorVisitId;
                element.VisitType = null;
                var map = Mapper.Map<visitorvisittype>(element);
                result += await unitOfWork.AddAsync<visitorvisittype>(map);
            }
            return result;
        }

        public async Task<int> AddChoiceRangeForUpdateAsync(IUnitOfWork unitOfWork, IVisitorVisitType[] entities)
        {
            var result = 0;
            foreach (var entity in entities)
            {
                result += await unitOfWork.UpdateAsync<visitorvisittype>(Mapper.Map<visitorvisittype>(entity));
            }
            return result;
        }

        public Task<int> AddForUpdateAsync(IUnitOfWork unitOfWork, IVisitorVisit entity)
        {
            var map = Mapper.Map<visitorvisit>(entity);
            map.visitorvisittypes = null;
            return unitOfWork.UpdateAsync<visitorvisit>(map);
        }

        public Task<int> DeleteAsync(IVisitorVisit entity)
        {
            return Repository.DeleteAsync<visitorvisit>(Mapper.Map<visitorvisit>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<visitorvisit>(ID);
        }

        public async Task<List<IVisitorVisit>> GetAsync()
        {
            // include referencing tables into VisitorVisit table
            var result = await Repository.WhereAsync<visitorvisit>()
                          .Include(c => c.department)
                          .Include(c => c.duration)
                          .Include(c => c.funding)
                          .Include(c => c.funding1)
                          .Include(c => c.userprofile)
                          .Include(c => c.visitorprofile)
                          .Include(ci => ci.visitorvisittypes.Select(fi => fi.visittype))
                          .ToListAsync<visitorvisit>();

            return Mapper.Map<List<IVisitorVisit>>(result);
        }

        public async Task<IVisitorVisit> GetAsync(string ID)
        {
            return Mapper.Map<IVisitorVisit>(await Repository.SingleAsync<visitorvisit>(ID));
        }

        public async Task<int> InsertAsync(IVisitorVisit entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            var counter = 0;
            if (entity.Department != null && entity.Department.ID == null)
            {
                entity.Department.ID = Guid.NewGuid().ToString("N");
                entity.DepartmentId = entity.Department.ID;
                counter++;
            }
            if (entity.Duration != null && entity.Duration.ID == null)
            {
                entity.Duration.ID = Guid.NewGuid().ToString("N");
                entity.DurationId = entity.Duration.ID;
                counter++;
            }
            if (entity.FundingOfStay != null && entity.FundingOfStay.ID == null)
            {
                entity.FundingOfStay.ID = Guid.NewGuid().ToString("N");
                entity.FundingOfStayId = entity.FundingOfStay.ID;
                counter++;
            }
            if (entity.TravelFunding != null && entity.TravelFunding.ID == null)
            {
                entity.TravelFunding.ID = Guid.NewGuid().ToString("N");
                entity.TravelFundingId = entity.TravelFunding.ID;
                counter++;
            }
            var collectionForInsert = entity.VisitorVisitTypes.ToArray();
            if (collectionForInsert.Where(c => c.ID != null).Count() > 0) { counter++; };
            var unitOfWork = Repository.CreateUnitOfWork();
            await this.AddChoiceRangeForInsertAsync(unitOfWork, entity.ID, collectionForInsert);
            var map = Mapper.Map<visitorvisit>(entity);
            map.visitorvisittypes = null;
            await unitOfWork.AddAsync<visitorvisit>(map);
            if (counter > 0) { LookupTablesService.ClearCache(); };
            return await unitOfWork.CommitAsync();
        }

        public async Task<int> UpdateAsync(IVisitorVisit entity)
        {
            var lookupItems = await LookupTablesService.GetAsync();
            var existing = await Repository.SingleAsync<visitorvisit>(entity.ID);

            var counter = 0;
            var unitOfWork = Repository.CreateUnitOfWork();
            if (entity.Department != null
                && !lookupItems.Departments.Select(c => c.Name).Contains(entity.Department.Name))
            {
                entity.Department.ID = Guid.NewGuid().ToString("N");
                entity.DepartmentId = entity.Department.ID;
                var map = Mapper.Map<department>(entity.Department);
                await unitOfWork.AddAsync<department>(map);
                counter++;
            }
            if (entity.Department != null
                && entity.Department.ID == entity.DepartmentId
                && lookupItems.Departments
                .Select(c => c.Name.ToLower())
                .Contains(entity.Department.Name.ToLower()))
            {
                entity.DepartmentId = lookupItems.Departments
                    .Where(c => c.Name.ToLower() == entity.Department.Name.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.Duration != null
                && !lookupItems.Durations
                .Select(c => c.Length.ToLower())
                .Contains(entity.Duration.Length.ToLower()))
            {
                entity.Duration.ID = Guid.NewGuid().ToString("N");
                entity.DurationId = entity.Duration.ID;
                var map = Mapper.Map<duration>(entity.Duration);
                await unitOfWork.AddAsync<duration>(map);
                counter++;
            }
            if (entity.Duration != null
                && entity.Duration.ID == entity.DurationId
                && lookupItems.Durations
                .Select(c => c.Length.ToLower())
                .Contains(entity.Duration.Length.ToLower()))
            {
                entity.DurationId = lookupItems.Durations
                    .Where(c => c.Length.ToLower() == entity.Duration.Length.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.TravelFunding != null
                && !lookupItems.Fundings.Select(c => c.Type).Contains(entity.TravelFunding.Type))
            {
                entity.TravelFunding.ID = Guid.NewGuid().ToString("N");
                entity.TravelFundingId = entity.TravelFunding.ID;
                var map = Mapper.Map<funding>(entity.TravelFunding);
                await unitOfWork.AddAsync<funding>(map);
                counter++;
            }
            if (entity.TravelFunding != null
                && entity.TravelFunding.ID == entity.TravelFundingId
                && lookupItems.Fundings
                .Select(c => c.Type.ToLower())
                .Contains(entity.TravelFunding.Type.ToLower()))
            {
                entity.TravelFundingId = lookupItems.Fundings
                    .Where(c => c.Type.ToLower() == entity.TravelFunding.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.FundingOfStay != null
                && !lookupItems.Fundings.Select(c => c.Type).Contains(entity.FundingOfStay.Type))
            {
                entity.FundingOfStay.ID = Guid.NewGuid().ToString("N");
                entity.FundingOfStayId = entity.FundingOfStay.ID;
                var map = Mapper.Map<funding>(entity.FundingOfStay);
                await unitOfWork.AddAsync<funding>(map);
                counter++;
            }
            if (entity.FundingOfStay != null
                && entity.FundingOfStay.ID == entity.FundingOfStayId
                && lookupItems.Fundings
                .Select(c => c.Type.ToLower())
                .Contains(entity.FundingOfStay.Type.ToLower()))
            {
                entity.FundingOfStayId = lookupItems.Fundings
                    .Where(c => c.Type.ToLower() == entity.FundingOfStay.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }

            var collectionForInsert = entity.VisitorVisitTypes
                .Where(choice => existing.visitorvisittypes
                    .SingleOrDefault(c => c.ID == choice.ID) == null)
                    .ToArray();
            var collectionForUpdate = entity.VisitorVisitTypes
                .Where(choice => existing.visitorvisittypes
                    .SingleOrDefault(c => c.ID == choice.ID) != null)
                    .ToArray();
            var collectionForDelete = existing.visitorvisittypes
                .Where(choice => entity.VisitorVisitTypes
                    .SingleOrDefault(c => c.ID == choice.ID) == null)
                    .Select(c => c.ID)
                    .ToArray();

            entity.Department = null;
            entity.Duration = null;
            entity.TravelFunding = null;
            entity.FundingOfStay = null;
            await this.AddChoiceRangeForDeleteAsync(unitOfWork, collectionForDelete);
            await this.AddChoiceRangeForUpdateAsync(unitOfWork, collectionForUpdate);
            await this.AddChoiceRangeForInsertAsync(unitOfWork, entity.ID, collectionForInsert);
            await this.AddForUpdateAsync(unitOfWork, entity);
            if (counter > 0)
            {
                await LookupTablesService.ClearCache();
            }
            return await unitOfWork.CommitAsync();
        }

        #endregion Methods
    }
}