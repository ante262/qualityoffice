using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class visitorvisittypeMap : EntityTypeConfiguration<visitorvisittype>
    {
        public visitorvisittypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.VisitorVisitId)
                .HasMaxLength(128);

            this.Property(t => t.VisitTypeId)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("visitorvisittype", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.VisitorVisitId).HasColumnName("VisitorVisitId");
            this.Property(t => t.VisitTypeId).HasColumnName("VisitTypeId");

            // Relationships
            this.HasOptional(t => t.visitorvisit)
                .WithMany(t => t.visitorvisittypes)
                .HasForeignKey(d => d.VisitorVisitId);
            this.HasOptional(t => t.visittype)
                .WithMany(t => t.visitorvisittypes)
                .HasForeignKey(d => d.VisitTypeId);

        }
    }
}
