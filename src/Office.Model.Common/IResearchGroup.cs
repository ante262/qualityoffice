﻿using System;

namespace Office.Model.Common
{
    public interface IResearchGroup
    {
        #region Properties

        string Abbreviation { get; set; }
        string EnglishName { get; set; }
        string ID { get; set; }
        string Name { get; set; }
        string URL { get; set; }

        #endregion Properties
    }
}