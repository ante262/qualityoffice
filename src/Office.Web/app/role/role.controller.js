﻿(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('RoleController', ['$scope', 'roleService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, roleService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.roles = [];
            vm.role = {};
            vm.selectedRole = null;
            vm.loading = true;

            vm.addRole = function () {
                ngDialog.open({
                    template: 'app/role/add-role.html',
                    controller: 'AddRoleController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editRole = function (data) {
                ngDialog.open({
                    template: 'app/role/edit-role.html',
                    controller: 'EditRoleController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.showRole = function (data) {
                ngDialog.open({
                    template: 'app/role/show-role.html',
                    controller: 'ShowRoleController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            };

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return roleService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('role', { 'notification': notifications.entity.serverError });
                    };
                    console.log('Unable to get roles:' + data.message);
                    $state.go('role', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.roles = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedRole = item;
                roleService.get(vm.selectedRole)
                    .success(function (data) {
                        vm.role = data;
                    }).error(function (data) {
                        console.log("Unable to get this role:" + data.message);
                    });
            };

            vm.delete = function (data) {
                vm.role = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Uloga '" + vm.role.Name + "' će biti obrisana!")) {
                    roleService.delete(vm.role.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);