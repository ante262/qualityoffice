(function (angular) {
    'use strict';
    angular.module('officeApp').service('userProfileService', ['$http', 'baseUrl','$httpParamSerializerJQLike',
        function ($http, baseUrl, $httpParamSerializerJQLike) {
            var tUrl = "UserProfile";

            return {
                //get all userProfiles
                fetch: function () {
                    return $http.get(baseUrl + tUrl);
                },
                //get userProfile by id
                get: function (id) {
                    return $http.get(baseUrl + tUrl + "/" + id);
                },
                //post userProfile
                post: function (data) {
                    var req = {
                        method: 'POST',
                        url: baseUrl + tUrl,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //put userProfile
                put: function (data) {
                    var req = {
                        method: 'PUT',
                        url: baseUrl + tUrl + "/" + data.ID,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //delete userProfile
                delete: function (item) {
                    var req = {
                        method: 'DELETE',
                        url: baseUrl + tUrl + "/" + item,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    };
                    return $http(req);
                }
            };
        }]);
})(angular);