(function (angular) {
    'use strict';
    angular.module('officeApp').service('committeeMemberService', ['$http', 'baseUrl','$httpParamSerializerJQLike',
        function ($http, baseUrl, $httpParamSerializerJQLike) {
            var tUrl = "CommitteeMember";

            return {
                //get all committeeMembers
                fetch: function () {
                    return $http.get(baseUrl + tUrl);
                },
                //get committeeMember by id
                get: function (id) {
                    return $http.get(baseUrl + tUrl + "/" + id);
                },
                //post committeeMember
                post: function (data) {
                    var req = {
                        method: 'POST',
                        url: baseUrl + tUrl,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //put committeeMember
                put: function (data) {
                    var req = {
                        method: 'PUT',
                        url: baseUrl + tUrl + "/" + data.ID,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //delete committeeMember
                delete: function (item) {
                    var req = {
                        method: 'DELETE',
                        url: baseUrl + tUrl + "/" + item,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    };
                    return $http(req);
                }
            };
        }]);
})(angular);