﻿using System;

namespace Office.Model.Common
{
    public interface ICommitteeMember
    {
        #region Properties

        string Comment { get; set; }
        ICommittee Committee { get; set; }
        string CommitteeId { get; set; }
        ICommitteeMemberRole CommitteeMemberRole { get; set; }
        string CommitteeMemberRoleId { get; set; }
        Nullable<System.DateTime> DateOfElection { get; set; }
        IElectingInstitution ElectingInstitution { get; set; }
        string ElectingInstitutionId { get; set; }
        string ID { get; set; }
        string PeriodOfElection { get; set; }
        string Title { get; set; }
        IUserProfile UserProfile { get; set; }
        string UserProfileId { get; set; }

        #endregion Properties
    }
}