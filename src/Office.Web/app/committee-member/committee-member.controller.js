(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('CommitteeMemberController', ['$scope', 'committeeMemberService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, committeeMemberService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.committeeMembers = [];
            vm.committeeMember = {};
            vm.selectedCommitteeMember = null;
            vm.loading = true;

            vm.addCommitteeMember = function () {
                ngDialog.open({
                    template: 'app/committee-member/add-committee-member.html',
                    controller: 'AddCommitteeMemberController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editCommitteeMember = function (data) {
                ngDialog.open({
                    template: 'app/committee-member/edit-committee-member.html',
                    controller: 'EditCommitteeMemberController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            }

            vm.showCommitteeMember = function (data) {
                ngDialog.open({
                    template: 'app/committee-member/show-committee-member.html',
                    controller: 'ShowCommitteeMemberController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            }

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }
            vm.show = function (comment) {
                if (comment) {
                    vm.opened = true;
                    vm.comment = comment;
                }
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return committeeMemberService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('committeeMember', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get committeeMember:' + data.message);
                    $state.go('committeeMember', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.committeeMembers = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedCommitteeMember = item;
                committeeMemberService.get(vm.selectedCommitteeMember).success(function (data) {
                    vm.committeeMember = data;
                }).error(function (data) {
                    console.log("Unable to get this committeeMember:" + data.message);
                });
            };

            vm.delete = function (data) {
                vm.committeeMember = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Član povjerenstva '" + vm.committeeMember.Name + "' će biti obrisano!")) {
                    committeeMemberService.delete(vm.committeeMember.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);