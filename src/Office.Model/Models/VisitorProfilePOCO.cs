﻿using Office.Model.Common;

using System;

namespace Office.Model
{
    public partial class VisitorProfilePOCO : IVisitorProfile
    {
        #region Properties

        public string Comment { get; set; }
        public string Email { get; set; }
        public IForeignInstitution ForeignInstitution { get; set; }
        public string ForeignInstitutionId { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public string PicturePath { get; set; }
        public string Surname { get; set; }
        public string TelephoneNumber { get; set; }
        public string TitleId { get; set; }
        public string URL { get; set; }
        public IUserTitle UserTitle { get; set; }

        #endregion Properties
    }
}