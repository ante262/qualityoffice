(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddOurStudentExchangeController', ['$scope', 'ourStudentExchangeService', 'lookupTablesService', 'foreignInstitutionService', 'exchangeOrganizationService', 'notifications', '$window', '$state', 'ngDialog',
        function ($scope, ourStudentExchangeService, lookupTablesService, foreignInstitutionService, exchangeOrganizationService, notifications, $window, $state, ngDialog) {
            var vm = $scope.vm = {};
            vm.ourStudentExchange = {};
            vm.lookups = [];
            vm.foreignInstitutions = [];
            vm.exchangeOrganizations = [];
            vm.check = {
                institution: false,
                department: false,
                purpose: false,
                organization: false,
                semester: false,
                duration: false,
                tfunding: false,
                sfunding: false
            };

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addExchangeOrganization = function () {
                ngDialog.open({
                    template: 'app/exchange-organization/add-exchange-organization.html',
                    controller: 'AddExchangeOrganizationController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchForeignInstitutions = function () {
                return foreignInstitutionService.fetch()
                .error(function (data) {
                })
                .success(function (data) {
                    vm.foreignInstitutions = data;
                });
            };

            vm.fetchExchangeOrganizations = function () {
                return exchangeOrganizationService.fetch()
                .error(function (data) {
                })
                .success(function (data) {
                    vm.exchangeOrganizations = data;
                });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('Departments, VisitPurposes, Durations, Fundings, Semesters')
                .then(function (data) {
                    vm.lookups = data;
                },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchForeignInstitutions();
                vm.fetchExchangeOrganizations();
                vm.fetchLookups();
            }
            vm.fetch();

            vm.post = function (item) {
                if (item.Department || item.Duration || item.TravelFunding || item.FundingOfStay ||
                    item.VisitPurpose || item.Semester) {
                    lookupTablesService.clearCache();
                };

                ourStudentExchangeService.post(item)
                    .success(function (data) {
                        $scope.closeThisDialog();
                    })
                    .error(function (data) {
                        console.log(item);
                    });
            };
        }]);
})(angular);