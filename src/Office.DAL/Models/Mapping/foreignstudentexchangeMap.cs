using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class foreignstudentexchangeMap : EntityTypeConfiguration<foreignstudentexchange>
    {
        public foreignstudentexchangeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.DepartmentId)
                .HasMaxLength(128);

            this.Property(t => t.UserProfileId)
                .HasMaxLength(128);

            this.Property(t => t.CourseId)
                .HasMaxLength(128);

            this.Property(t => t.VisitPurposeId)
                .HasMaxLength(128);

            this.Property(t => t.ForeignStudentId)
                .HasMaxLength(128);

            this.Property(t => t.SemesterId)
                .HasMaxLength(128);

            this.Property(t => t.ForeignInstitutionId)
                .HasMaxLength(128);

            this.Property(t => t.DurationId)
                .HasMaxLength(128);

            this.Property(t => t.ExchangeOrganizationId)
                .HasMaxLength(128);

            this.Property(t => t.TravelFundingId)
                .HasMaxLength(128);

            this.Property(t => t.FundingOfStayId)
                .HasMaxLength(128);

            this.Property(t => t.ExchangeResult)
                .HasMaxLength(1073741823);

            this.Property(t => t.Comment)
                .HasMaxLength(1073741823);

            // Table & Column Mappings
            this.ToTable("foreignstudentexchange", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DepartmentId).HasColumnName("DepartmentId");
            this.Property(t => t.UserProfileId).HasColumnName("UserProfileId");
            this.Property(t => t.CourseId).HasColumnName("CourseId");
            this.Property(t => t.VisitPurposeId).HasColumnName("VisitPurposeId");
            this.Property(t => t.ForeignStudentId).HasColumnName("ForeignStudentId");
            this.Property(t => t.SemesterId).HasColumnName("SemesterId");
            this.Property(t => t.ForeignInstitutionId).HasColumnName("ForeignInstitutionId");
            this.Property(t => t.AcademicYear).HasColumnName("AcademicYear");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.DurationId).HasColumnName("DurationId");
            this.Property(t => t.ExchangeOrganizationId).HasColumnName("ExchangeOrganizationId");
            this.Property(t => t.TravelFundingId).HasColumnName("TravelFundingId");
            this.Property(t => t.FundingOfStayId).HasColumnName("FundingOfStayId");
            this.Property(t => t.ExchangeResult).HasColumnName("ExchangeResult");
            this.Property(t => t.Comment).HasColumnName("Comment");

            // Relationships
            this.HasOptional(t => t.course)
                .WithMany(t => t.foreignstudentexchanges)
                .HasForeignKey(d => d.CourseId);
            this.HasOptional(t => t.department)
                .WithMany(t => t.foreignstudentexchanges)
                .HasForeignKey(d => d.DepartmentId);
            this.HasOptional(t => t.duration)
                .WithMany(t => t.foreignstudentexchanges)
                .HasForeignKey(d => d.DurationId);
            this.HasOptional(t => t.exchangeorganization)
                .WithMany(t => t.foreignstudentexchanges)
                .HasForeignKey(d => d.ExchangeOrganizationId);
            this.HasOptional(t => t.foreigninstitution)
                .WithMany(t => t.foreignstudentexchanges)
                .HasForeignKey(d => d.ForeignInstitutionId);
            this.HasOptional(t => t.foreignstudent)
                .WithMany(t => t.foreignstudentexchanges)
                .HasForeignKey(d => d.ForeignStudentId);
            this.HasOptional(t => t.funding)
                .WithMany(t => t.foreignstudentexchanges)
                .HasForeignKey(d => d.FundingOfStayId);
            this.HasOptional(t => t.semester)
                .WithMany(t => t.foreignstudentexchanges)
                .HasForeignKey(d => d.SemesterId);
            this.HasOptional(t => t.funding1)
                .WithMany(t => t.foreignstudentexchanges1)
                .HasForeignKey(d => d.TravelFundingId);
            this.HasOptional(t => t.userprofile)
                .WithMany(t => t.foreignstudentexchanges)
                .HasForeignKey(d => d.UserProfileId);
            this.HasOptional(t => t.visitpurpose)
                .WithMany(t => t.foreignstudentexchanges)
                .HasForeignKey(d => d.VisitPurposeId);

        }
    }
}
