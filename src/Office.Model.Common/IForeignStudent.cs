﻿using System;

namespace Office.Model.Common
{
    public interface IForeignStudent
    {
        #region Properties

        string Abbreviation { get; set; }
        string City { get; set; }
        string Comment { get; set; }
        string Email { get; set; }
        string ID { get; set; }
        string Name { get; set; }
        string State { get; set; }
        string Surname { get; set; }
        string TelephoneNumber { get; set; }

        #endregion Properties
    }
}