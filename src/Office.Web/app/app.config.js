﻿(function (angular) {
    'use strict';
    angular.module('officeApp')
        .constant("baseUrl", 'http://mathos-office.azurewebsites.net/api/')
        //.constant("baseUrl", 'http://office.api/api/')
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $urlRouterProvider.otherwise('/');

        //Authorization interceptor
        $httpProvider.interceptors.push('authorizationInterceptorService');

        //******* States *******\\
        $stateProvider.state('home', {
            url: '/',
            templateUrl: "app/home.html"
        })

        //Login state
        .state('login', {
            url: '/login',
            params: {
                notification: null,
            },
            templateUrl: 'app/login/login-user.html',
            controller: 'LoginUserController'
        })
        //Register state
        .state('register', {
            url: '/register',
            params: {
                notification: null,
            },
            templateUrl: 'app/register/register-user.html',
            controller: 'RegisterUserController'
        })
        //States for Role table
        .state('role', {
            url: '/role',
            params: {
                notification: null,
            },
            templateUrl: 'app/role/role.html',
            controller: 'RoleController'
        }).state('addRole', {
            url: '/role/add',
            templateUrl: 'app/role/add-role.html',
            controller: 'AddRoleController'
        }).state('editRole', {
            url: '/role/edit',
            params: {
                infoRole: null,
            },
            templateUrl: 'app/role/edit-role.html',
            controller: 'EditRoleController'
        })

        //States for Chair table
        .state('chair', {
            url: '/chair',
            params: {
                notification: null,
            },
            templateUrl: 'app/chair/chair.html',
            controller: 'ChairController'
        }).state('addChair', {
            url: '/chair/add',
            templateUrl: 'app/chair/add-chair.html',
            controller: 'AddChairController'
        }).state('editChair', {
            url: '/chair/edit',
            params: {
                infoChair: null,
            },
            templateUrl: 'app/chair/edit-chair.html',
            controller: 'EditChairController'
        })

        //States for CommitteeMember table
        .state('committeeMember', {
            url: '/committee-member',
            params: {
                notification: null,
            },
            templateUrl: 'app/committee-member/committee-member.html',
            controller: 'CommitteeMemberController'
        })
        .state('addCommitteeMember', {
            url: '/committee-member/add',
            templateUrl: 'app/committee-member/add-committee-member.html',
            controller: 'AddCommitteeMemberController'
        })
        .state('editCommitteeMember', {
            url: '/committee-member/edit',
            params: {
                infoCommitteeMember: null,
            },
            templateUrl: 'app/committee-member/edit-committee-member.html',
            controller: 'EditCommitteeMemberController'
        })

        //States for ForeignStudentExchange table
        .state('foreignStudentExchange', {
            url: '/foreign-student-exchange',
            params: {
                notification: null,
            },
            templateUrl: 'app/foreign-student-exchange/foreign-student-exchange.html',
            controller: 'ForeignStudentExchangeController'
        })
        .state('addForeignStudentExchange', {
            url: '/foreign-student-exchange/add',
            templateUrl: 'app/foreign-student-exchange/add-foreign-student-exchange.html',
            controller: 'AddForeignStudentExchangeController'
        })
        .state('editForeignStudentExchange', {
            url: '/foreign-student-exchange/edit',
            params: {
                infoForeignStudentExchange: null,
            },
            templateUrl: 'app/foreign-student-exchange/edit-foreign-student-exchange.html',
            controller: 'EditForeignStudentExchangeController'
        })

        //States for OurStudentExchange table
        .state('ourStudentExchange', {
            url: '/our-student-exchange',
            params: {
                notification: null,
            },
            templateUrl: 'app/our-student-exchange/our-student-exchange.html',
            controller: 'OurStudentExchangeController'
        })
        .state('addOurStudentExchange', {
            url: '/our-student-exchange/add',
            templateUrl: 'app/our-student-exchange/add-our-student-exchange.html',
            controller: 'AddOurStudentExchangeController'
        })
        .state('editOurStudentExchange', {
            url: '/our-student-exchange/edit',
            params: {
                infoOurStudentExchange: null,
            },
            templateUrl: 'app/our-student-exchange/edit-our-student-exchange.html',
            controller: 'EditOurStudentExchangeController'
        })

        //States for OurProfessorVisit table
        .state('ourProfessorVisit', {
            url: '/our-professor-visit',
            params: {
                notification: null,
            },
            templateUrl: 'app/our-professor-visit/our-professor-visit.html',
            controller: 'OurProfessorVisitController'
        })
        .state('addOurProfessorVisit', {
            url: '/our-professor-visit/add',
            templateUrl: 'app/our-professor-visit/add-our-professor-visit.html',
            controller: 'AddOurProfessorVisitController'
        })
        .state('editOurProfessorVisit', {
            url: '/our-professor-visit/edit',
            params: {
                infoOurProfessorVisit: null,
            },
            templateUrl: 'app/our-professor-visit/edit-our-professor-visit.html',
            controller: 'EditOurProfessorVisitController'
        })

        //States for VisitorVisit table
        .state('visitorVisit', {
            url: '/visitor-visit',
            params: {
                notification: null,
            },
            templateUrl: 'app/visitor-visit/visitor-visit.html',
            controller: 'VisitorVisitController'
        })
        .state('addVisitorVisit', {
            url: '/visitor-visit/add',
            templateUrl: 'app/visitor-visit/add-visitor-visit.html',
            controller: 'AddVisitorVisitController'
        })
        .state('editVisitorVisit', {
            url: '/visitor-visit/edit',
            params: {
                infoVisitorVisit: null,
            },
            templateUrl: 'app/visitor-visit/edit-visitor-visit.html',
            controller: 'EditVisitorVisitController'
        })

        //States for NonPublicationLecture table
        .state('nonPublicationLecture', {
            url: '/non-publication-lecture',
            params: {
                notification: null,
            },
            templateUrl: 'app/non-publication-lecture/non-publication-lecture.html',
            controller: 'NonPublicationLectureController'
        })
        .state('addNonPublicationLecture', {
            url: '/non-publication-lecture/add',
            templateUrl: 'app/non-publication-lecture/add-non-publication-lecture.html',
            controller: 'AddNonPublicationLectureController'
        })
        .state('editNonPublicationLecture', {
            url: '/non-publication-lecture/edit',
            params: {
                infoNonPublicationLecture: null,
            },
            templateUrl: 'app/non-publication-lecture/edit-non-publication-lecture.html',
            controller: 'EditNonPublicationLectureController'
        })

        //States for VisitorProfile table
        .state('visitorProfile', {
            url: '/visitor-profile',
            params: {
                notification: null,
            },
            templateUrl: 'app/visitor-profile/visitor-profile.html',
            controller: 'VisitorProfileController'
        })
        .state('addVisitorProfile', {
            url: '/visitor-profile/add',
            templateUrl: 'app/visitor-profile/add-visitor-profile.html',
            controller: 'AddVisitorProfileController'
        })
        .state('editVisitorProfile', {
            url: '/visitor-profile/edit',
            params: {
                infoVisitorProfile: null,
            },
            templateUrl: 'app/visitor-profile/edit-visitor-profile.html',
            controller: 'EditVisitorProfileController'
        })

        //States for ForeignStudent table
        .state('foreignStudent', {
            url: '/foreign-student',
            params: {
                notification: null,
            },
            templateUrl: 'app/foreign-student/foreign-student.html',
            controller: 'ForeignStudentController'
        })
        .state('addForeignStudent', {
            url: '/foreign-student/add',
            templateUrl: 'app/foreign-student/add-foreign-student.html',
            controller: 'AddForeignStudentController'
        })
        .state('editForeignStudent', {
            url: '/foreign-student/edit',
            params: {
                infoForeignStudent: null,
            },
            templateUrl: 'app/foreign-student/edit-foreign-student.html',
            controller: 'EditForeignStudentController'
        })

        //States for ExchangeOrganization table
        .state('exchangeOrganization', {
            url: '/exchange-organization',
            params: {
                notification: null,
            },
            templateUrl: 'app/exchange-organization/exchange-organization.html',
            controller: 'ExchangeOrganizationController'
        })
        .state('addExchangeOrganization', {
            url: '/exchange-organization/add',
            templateUrl: 'app/exchange-organization/add-exchange-organization.html',
            controller: 'AddExchangeOrganizationController'
        })
        .state('editExchangeOrganization', {
            url: '/exchange-organization/edit',
            params: {
                infoExchangeOrganization: null,
            },
            templateUrl: 'app/exchange-organization/edit-exchange-organization.html',
            controller: 'EditExchangeOrganizationController'
        })

        //States for Conference table
        .state('conference', {
            url: '/conference',
            params: {
                notification: null,
            },
            templateUrl: 'app/conference/conference.html',
            controller: 'ConferenceController'
        })
        .state('addConference', {
            url: '/conference/add',
            templateUrl: 'app/conference/add-conference.html',
            controller: 'AddConferenceController'
        })
        .state('editConference', {
            url: '/conference/edit',
            params: {
                infoConference: null,
            },
            templateUrl: 'app/conference/edit-conference.html',
            controller: 'EditConferenceController'
        })

        //States for Contract table
        .state('contract', {
            url: '/contract',
            params: {
                notification: null,
            },
            templateUrl: 'app/contract/contract.html',
            controller: 'ContractController'
        })
        .state('addContract', {
            url: '/contract/add',
            templateUrl: 'app/contract/add-contract.html',
            controller: 'AddContractController'
        }).state('editContract', {
            url: '/contract/edit',
            params: {
                infoContract: null,
            },
            templateUrl: 'app/contract/edit-contract.html',
            controller: 'EditContractController'
        })

        //States for Contact table
        .state('contact', {
            url: '/contact',
            params: {
                notification: null,
            },
            templateUrl: 'app/contact/contact.html',
            controller: 'ContactController'
        })
        .state('addContact', {
            url: '/contact/add',
            templateUrl: 'app/contact/add-contact.html',
            controller: 'AddContactController'
        }).state('editContact', {
            url: '/contact/edit',
            params: {
                infoContact: null,
            },
            templateUrl: 'app/contact/edit-contact.html',
            controller: 'EditContactController'
        })

        //States for UserProfile table
        .state('userProfile', {
            url: '/user-profile',
            params: {
                notification: null,
            },
            templateUrl: 'app/user-profile/user-profile.html',
            controller: 'UserProfileController'
        })
        .state('addUserProfile', {
            url: '/user-profile/add',
            templateUrl: 'app/user-profile/add-user-profile.html',
            controller: 'AddUserProfileController'
        }).state('editUserProfile', {
            url: '/user-profile/edit',
            params: {
                infoUserProfile: null,
            },
            templateUrl: 'app/user-profile/edit-user-profile.html',
            controller: 'EditUserProfileController'
        })

        //States for ForeignInstitution table
        .state('foreignInstitution', {
            url: '/foreign-institution',
            params: {
                notification: null,
            },
            templateUrl: 'app/foreign-institution/foreign-institution.html',
            controller: 'ForeignInstitutionController'
        })
        .state('addForeignInstitution', {
            url: '/foreign-institution/add',
            templateUrl: 'app/foreign-institution/add-foreign-institution.html',
            controller: 'AddForeignInstitutionController'
        })
        .state('editForeignInstitution', {
            url: '/foreign-institution/edit',
            params: {
                infoForeignInstitution: null,
            },
            templateUrl: 'app/foreign-institution/edit-foreign-institution.html',
            controller: 'EditForeignInstitutionController'
        })

        //States for ConferenceType table
        .state('conferenceType', {
            url: '/conference-type',
            params: {
                notification: null,
            },
            templateUrl: 'app/conference-type/conference-type.html',
            controller: 'ConferenceTypeController'
        })
        .state('addConferenceType', {
            url: '/conference-type/add',
            templateUrl: 'app/conference-type/add-conference-type.html',
            controller: 'AddConferenceTypeController'
        })
        .state('editConferenceType', {
            url: '/conference-type/edit',
            params: {
                infoConferenceType: null,
            },
            templateUrl: 'app/conference-type/edit-conference-type.html',
            controller: 'EditConferenceTypeController'
        })

        ;
    }]);
    // Watch for unauthorized users and redirect them to the login page
    angular.module('officeApp').run(['localStorageService', '$rootScope', '$state',
        function (localStorageService, $rootScope, $state) {
            $rootScope.$on('$stateChangeStart', function (event, toState, fromState) {
                if (!localStorageService.get('authorizationData') && toState.name != 'login' && toState.name != 'home' && toState.name != 'register') {
                    $state.go('login');
                    event.preventDefault();
                    return;
                }
            });
        }]);
})(angular);