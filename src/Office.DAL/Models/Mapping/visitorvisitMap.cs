using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class visitorvisitMap : EntityTypeConfiguration<visitorvisit>
    {
        public visitorvisitMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.VisitorProfileId)
                .HasMaxLength(128);

            this.Property(t => t.DepartmentId)
                .HasMaxLength(128);

            this.Property(t => t.DurationId)
                .HasMaxLength(128);

            this.Property(t => t.UserProfileId)
                .HasMaxLength(128);

            this.Property(t => t.TravelFundingId)
                .HasMaxLength(128);

            this.Property(t => t.FundingOfStayId)
                .HasMaxLength(128);

            this.Property(t => t.VisitResult)
                .HasMaxLength(1073741823);

            this.Property(t => t.Comment)
                .HasMaxLength(1073741823);

            // Table & Column Mappings
            this.ToTable("visitorvisit", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.VisitorProfileId).HasColumnName("VisitorProfileId");
            this.Property(t => t.DepartmentId).HasColumnName("DepartmentId");
            this.Property(t => t.AcademicYear).HasColumnName("AcademicYear");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.DurationId).HasColumnName("DurationId");
            this.Property(t => t.UserProfileId).HasColumnName("UserProfileId");
            this.Property(t => t.TravelFundingId).HasColumnName("TravelFundingId");
            this.Property(t => t.FundingOfStayId).HasColumnName("FundingOfStayId");
            this.Property(t => t.VisitResult).HasColumnName("VisitResult");
            this.Property(t => t.Comment).HasColumnName("Comment");

            // Relationships
            this.HasOptional(t => t.department)
                .WithMany(t => t.visitorvisits)
                .HasForeignKey(d => d.DepartmentId);
            this.HasOptional(t => t.duration)
                .WithMany(t => t.visitorvisits)
                .HasForeignKey(d => d.DurationId);
            this.HasOptional(t => t.funding)
                .WithMany(t => t.visitorvisits)
                .HasForeignKey(d => d.FundingOfStayId);
            this.HasOptional(t => t.funding1)
                .WithMany(t => t.visitorvisits1)
                .HasForeignKey(d => d.TravelFundingId);
            this.HasOptional(t => t.userprofile)
                .WithMany(t => t.visitorvisits)
                .HasForeignKey(d => d.UserProfileId);
            this.HasOptional(t => t.visitorprofile)
                .WithMany(t => t.visitorvisits)
                .HasForeignKey(d => d.VisitorProfileId);

        }
    }
}
