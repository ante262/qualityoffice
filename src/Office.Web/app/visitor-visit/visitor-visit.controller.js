(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('VisitorVisitController', ['$scope', 'visitorVisitService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, visitorVisitService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.visitorVisits = [];
            vm.visitorVisit = {};
            vm.selectedVisitorVisit = null;
            vm.loading = true;

            vm.addVisitorVisit = function () {
                ngDialog.open({
                    template: 'app/visitor-visit/add-visitor-visit.html',
                    controller: 'AddVisitorVisitController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editVisitorVisit = function (data) {
                ngDialog.open({
                    template: 'app/visitor-visit/edit-visitor-visit.html',
                    controller: 'EditVisitorVisitController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.showVisitorVisit = function (data) {
                ngDialog.open({
                    template: 'app/visitor-visit/show-visitor-visit.html',
                    controller: 'ShowVisitorVisitController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            };

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return visitorVisitService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('visitorVisit', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get visitorVisit:' + data.message);
                    $state.go('visitorVisit', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.visitorVisits = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedVisitorVisit = item;
                visitorVisitService.get(vm.selectedVisitorVisit)
                    .success(function (data) {
                        vm.visitorVisit = data;
                    }).error(function (data) {
                        console.log("Unable to get this visitorVisit:" + data.message);
                    });
            };

            vm.delete = function (data) {
                vm.visitorVisit = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Boravak djelatnika '" + vm.visitorVisit.VisitorProfile.Name + " " + vm.visitorVisit.VisitorProfile.Surname + "' će biti obrisan!")) {
                    visitorVisitService.delete(vm.visitorVisit.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);