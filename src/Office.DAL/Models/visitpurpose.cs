using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class visitpurpose
    {
        public visitpurpose()
        {
            this.foreignstudentexchanges = new List<foreignstudentexchange>();
            this.ourstudentexchanges = new List<ourstudentexchange>();
        }

        public string ID { get; set; }
        public string Type { get; set; }
        public string Comment { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<foreignstudentexchange> foreignstudentexchanges { get; set; }
        public virtual ICollection<ourstudentexchange> ourstudentexchanges { get; set; }
    }
}
