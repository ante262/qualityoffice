using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class visitorprofile
    {
        public visitorprofile()
        {
            this.visitorvisits = new List<visitorvisit>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string TitleId { get; set; }
        public string Email { get; set; }
        public string ForeignInstitutionId { get; set; }
        public string URL { get; set; }
        public string PicturePath { get; set; }
        public string TelephoneNumber { get; set; }
        public string Comment { get; set; }
        public virtual foreigninstitution foreigninstitution { get; set; }
        public virtual usertitle usertitle { get; set; }
        public virtual ICollection<visitorvisit> visitorvisits { get; set; }
    }
}
