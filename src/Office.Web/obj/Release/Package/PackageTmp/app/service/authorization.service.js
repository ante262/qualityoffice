﻿(function (angular) {
    'use strict';
    angular.module('officeApp').service('authorizationService', ['$http', '$q', 'localStorageService', 'baseUrl',
        function ($http, $q, localStorageService, baseUrl) {
            var apiBase = baseUrl.substring(0, baseUrl.length - 4);
            var authorizationService = {
                // Register method - used for registering a new user
                register: function (registrationData) {
                    this.logOut();
                    //TODO Look into this way of sending requests. Do we need transform request? If we need, can we simplify
                    //     this with a helper function that would build the request object?
                    var req = {
                        method: 'POST',
                        url: baseUrl + "Account/Register",
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        transformRequest: function (obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: registrationData
                    };
                    return $http(req);
                },

                // Login method - used to login a user by requesting a token from the api and setting it in the users header
                login: function (loginData) {
                    var deferred = $q.defer();
                    var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

                    $http.post(apiBase + "Token", data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
                        localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName });

                        deferred.resolve(response);
                    }).error(function (error) {
                        logOut();
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }
            };

            // Logout method - used to terminate a users session by deleting the token
            function logOut() {
                localStorageService.remove('authorizationData');
            };
            authorizationService.logOut = logOut;
            return authorizationService;
        }]);
})(angular);