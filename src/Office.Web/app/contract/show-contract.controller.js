(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowContractController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.contract = $scope.ngDialogData;
        }]);
})(angular);