using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class userprofileMap : EntityTypeConfiguration<userprofile>
    {
        public userprofileMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Surname)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.TitleId)
                .HasMaxLength(128);

            this.Property(t => t.AcademicRankId)
                .HasMaxLength(128);

            this.Property(t => t.URL)
                .HasMaxLength(250);

            this.Property(t => t.PicturePath)
                .HasMaxLength(500);

            this.Property(t => t.ResearchGroupId)
                .HasMaxLength(128);

            this.Property(t => t.ChairId)
                .HasMaxLength(128);

            this.Property(t => t.Room)
                .HasMaxLength(10);

            this.Property(t => t.TelephoneNumber)
                .HasMaxLength(128);

            this.Property(t => t.Comment)
                .HasMaxLength(1073741823);

            this.Property(t => t.Email)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("userprofile", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Surname).HasColumnName("Surname");
            this.Property(t => t.TitleId).HasColumnName("TitleId");
            this.Property(t => t.AcademicRankId).HasColumnName("AcademicRankId");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.PicturePath).HasColumnName("PicturePath");
            this.Property(t => t.ResearchGroupId).HasColumnName("ResearchGroupId");
            this.Property(t => t.ChairId).HasColumnName("ChairId");
            this.Property(t => t.Room).HasColumnName("Room");
            this.Property(t => t.Floor).HasColumnName("Floor");
            this.Property(t => t.TelephoneNumber).HasColumnName("TelephoneNumber");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.Email).HasColumnName("Email");

            // Relationships
            this.HasOptional(t => t.chair)
                .WithMany(t => t.userprofiles)
                .HasForeignKey(d => d.ChairId);
            this.HasOptional(t => t.researchgroup)
                .WithMany(t => t.userprofiles)
                .HasForeignKey(d => d.ResearchGroupId);
            this.HasOptional(t => t.useracademicrank)
                .WithMany(t => t.userprofiles)
                .HasForeignKey(d => d.AcademicRankId);
            this.HasOptional(t => t.usertitle)
                .WithMany(t => t.userprofiles)
                .HasForeignKey(d => d.TitleId);

        }
    }
}
