using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class conferenceinstitution
    {
        public string ID { get; set; }
        public string ConferenceId { get; set; }
        public string ForeignInstitutionId { get; set; }
        public virtual conference conference { get; set; }
        public virtual foreigninstitution foreigninstitution { get; set; }
    }
}
