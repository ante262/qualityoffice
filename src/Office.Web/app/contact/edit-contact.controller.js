(function (angular) {
    'use strict';
    angular.module('officeApp').controller('EditContactController', ['$scope', 'contactService', 'notifications', '$state',
        function ($scope, contactService, notifications, $state) {
            var vm = $scope.vm = {};
            vm.contact = $scope.ngDialogData;

            vm.put = function (item) {
                contactService.put(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                        $state.go('contact', { 'notification': notifications.entity.editError });
                    });
            };
        }]);
})(angular);