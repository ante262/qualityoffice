using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class department
    {
        public department()
        {
            this.foreignstudentexchanges = new List<foreignstudentexchange>();
            this.ourprofessorvisits = new List<ourprofessorvisit>();
            this.ourstudentexchanges = new List<ourstudentexchange>();
            this.visitorvisits = new List<visitorvisit>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public string Comment { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<foreignstudentexchange> foreignstudentexchanges { get; set; }
        public virtual ICollection<ourprofessorvisit> ourprofessorvisits { get; set; }
        public virtual ICollection<ourstudentexchange> ourstudentexchanges { get; set; }
        public virtual ICollection<visitorvisit> visitorvisits { get; set; }
    }
}
