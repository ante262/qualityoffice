using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class conferenceMap : EntityTypeConfiguration<conference>
    {
        public conferenceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Name)
                .HasMaxLength(128);

            this.Property(t => t.EnglishName)
                .HasMaxLength(128);

            this.Property(t => t.ConferenceTypeId)
                .HasMaxLength(128);

            this.Property(t => t.Location)
                .HasMaxLength(128);

            this.Property(t => t.URL)
                .HasMaxLength(250);

            this.Property(t => t.Locality)
                .HasMaxLength(128);

            this.Property(t => t.Profession)
                .HasMaxLength(128);

            this.Property(t => t.Comment)
                .HasMaxLength(1073741823);

            // Table & Column Mappings
            this.ToTable("conference", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.EnglishName).HasColumnName("EnglishName");
            this.Property(t => t.ConferenceTypeId).HasColumnName("ConferenceTypeId");
            this.Property(t => t.Location).HasColumnName("Location");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.Locality).HasColumnName("Locality");
            this.Property(t => t.Profession).HasColumnName("Profession");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.Date).HasColumnName("Date");

            // Relationships
            this.HasOptional(t => t.conferencetype)
                .WithMany(t => t.conferences)
                .HasForeignKey(d => d.ConferenceTypeId);

        }
    }
}
