﻿using Office.Model.Common;
using System;

namespace Office.Model
{
    public partial class NonPublicationLecturePOCO : INonPublicationLecture
    {
        #region Properties

        public string City { get; set; }
        public string Comment { get; set; }
        public IConferenceType ConferenceType { get; set; }
        public string ConferenceTypeId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Description { get; set; }
        public string EnglishTitle { get; set; }
        public IForeignInstitution ForeignInstitution { get; set; }
        public string ForeignInstitutionId { get; set; }
        public string ID { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public string Period { get; set; }
        public string Profession { get; set; }
        public string State { get; set; }
        public string Summary { get; set; }
        public string Surname { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }

        #endregion Properties
    }
}