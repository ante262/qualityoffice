﻿using Office.Model.Common;

using System;

namespace Office.Model
{
    public partial class VisitorVisitTypePOCO : IVisitorVisitType
    {
        #region Properties

        public string ID { get; set; }
        public string VisitorVisitId { get; set; }
        public IVisitType VisitType { get; set; }
        public string VisitTypeId { get; set; }

        #endregion Properties
    }
}