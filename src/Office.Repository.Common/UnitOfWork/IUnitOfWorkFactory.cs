﻿namespace Office.Repository.Common
{
    public interface IUnitOfWorkFactory
    {
        #region Methods

        /// <summary>
        /// Creates the unit of work.
        /// </summary>
        /// <returns></returns>
        IUnitOfWork CreateUnitOfWork();

        #endregion Methods
    }
}