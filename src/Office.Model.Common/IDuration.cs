﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office.Model.Common
{
    public interface IDuration
    {
        string Abbreviation { get; set; }
        string ID { get; set; }
        string Length { get; set; }
    }
}
