﻿using Office.Model.Common;
using System;

namespace Office.Model
{
    public partial class CommitteeMemberPOCO : ICommitteeMember
    {
        #region Properties

        public string Comment { get; set; }
        public ICommittee Committee { get; set; }
        public string CommitteeId { get; set; }
        public ICommitteeMemberRole CommitteeMemberRole { get; set; }
        public string CommitteeMemberRoleId { get; set; }
        public Nullable<System.DateTime> DateOfElection { get; set; }
        public IElectingInstitution ElectingInstitution { get; set; }
        public string ElectingInstitutionId { get; set; }
        public string ID { get; set; }
        public string PeriodOfElection { get; set; }
        public string Title { get; set; }
        public IUserProfile UserProfile { get; set; }
        public string UserProfileId { get; set; }

        #endregion Properties
    }
}