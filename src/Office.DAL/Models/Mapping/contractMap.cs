using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class contractMap : EntityTypeConfiguration<contract>
    {
        public contractMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Name)
                .HasMaxLength(128);

            this.Property(t => t.ContractTypeId)
                .HasMaxLength(128);

            this.Property(t => t.DurationId)
                .HasMaxLength(128);

            this.Property(t => t.FundingTypeId)
                .HasMaxLength(128);

            this.Property(t => t.Description)
                .HasMaxLength(1073741823);

            this.Property(t => t.URL)
                .HasMaxLength(250);

            this.Property(t => t.Comment)
                .HasMaxLength(1073741823);

            // Table & Column Mappings
            this.ToTable("contract", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.ContractTypeId).HasColumnName("ContractTypeId");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.DurationId).HasColumnName("DurationId");
            this.Property(t => t.FundingTypeId).HasColumnName("FundingTypeId");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.Comment).HasColumnName("Comment");

            // Relationships
            this.HasOptional(t => t.contracttype)
                .WithMany(t => t.contracts)
                .HasForeignKey(d => d.ContractTypeId);
            this.HasOptional(t => t.duration)
                .WithMany(t => t.contracts)
                .HasForeignKey(d => d.DurationId);
            this.HasOptional(t => t.funding)
                .WithMany(t => t.contracts)
                .HasForeignKey(d => d.FundingTypeId);

        }
    }
}
