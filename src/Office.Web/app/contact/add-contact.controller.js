(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddContactController', ['$scope', 'contactService', 'notifications', '$window', '$state',
        function ($scope, contactService, notifications, $window, $state) {
            var vm = $scope.vm = {};
            vm.contact = {};

            vm.post = function (item) {
                contactService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                    });
            };
        }]);
})(angular);