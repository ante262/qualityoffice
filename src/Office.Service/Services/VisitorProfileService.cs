﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Office.Service
{
    public class VisitorProfileService : IVisitorProfileService
    {
        #region Constructors

        public VisitorProfileService(IRepository repository, ILookupTablesService lookupTablesService)
        {
            Repository = repository;
            LookupTablesService = lookupTablesService;
        }

        #endregion Constructors

        #region Properties

        protected ILookupTablesService LookupTablesService { get; private set; }
        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IVisitorProfile entity)
        {
            return Repository.DeleteAsync<visitorprofile>(Mapper.Map<visitorprofile>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<visitorprofile>(ID);
        }

        public async Task<List<IVisitorProfile>> GetAsync()
        {
            // include referencing tables into VisitorProfile table
            var result = await Repository.WhereAsync<visitorprofile>()
                          .Include(c => c.foreigninstitution)
                          .Include(c => c.usertitle)
                          .ToListAsync<visitorprofile>();

            return Mapper.Map<List<IVisitorProfile>>(result);
        }

        public async Task<IVisitorProfile> GetAsync(string ID)
        {
            return Mapper.Map<IVisitorProfile>(await Repository.SingleAsync<visitorprofile>(ID));
        }

        public Task<int> InsertAsync(IVisitorProfile entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            var counter = 0;
            if (entity.UserTitle != null && entity.UserTitle.ID == null)
            {
                entity.UserTitle.ID = Guid.NewGuid().ToString("N");
                entity.TitleId = entity.UserTitle.ID;
                counter++;
            }
            if (counter > 0)
            {
                LookupTablesService.ClearCache();
            }

            return Repository.InsertAsync<visitorprofile>(Mapper.Map<visitorprofile>(entity));
        }

        public async Task<int> UpdateAsync(IVisitorProfile entity)
        {
            var lookupItems = await LookupTablesService.GetAsync();

            var counter = 0;
            var unitOfWork = Repository.CreateUnitOfWork();
            if (entity.UserTitle != null
                && !lookupItems.UserTitles.Select(c => c.Name).Contains(entity.UserTitle.Name))
            {
                entity.UserTitle.ID = Guid.NewGuid().ToString("N");
                entity.TitleId = entity.UserTitle.ID;
                var map = Mapper.Map<usertitle>(entity.UserTitle);
                await unitOfWork.AddAsync<usertitle>(map);
                counter++;
            }
            if (entity.UserTitle != null
                && entity.UserTitle.ID == entity.TitleId
                && lookupItems.UserTitles
                .Select(c => c.Name.ToLower())
                .Contains(entity.UserTitle.Name.ToLower()))
            {
                entity.TitleId = lookupItems.UserTitles
                    .Where(c => c.Name.ToLower() == entity.UserTitle.Name.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }

            entity.UserTitle = null;
            await unitOfWork.UpdateAsync<visitorprofile>(Mapper.Map<visitorprofile>(entity));
            if (counter > 0)
            {
                await LookupTablesService.ClearCache();
            }
            return await unitOfWork.CommitAsync();
        }

        #endregion Methods
    }
}