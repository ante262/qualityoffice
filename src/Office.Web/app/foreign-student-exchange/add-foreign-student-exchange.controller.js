(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddForeignStudentExchangeController', ['$scope', 'foreignStudentExchangeService', 'lookupTablesService', 'foreignInstitutionService', 'exchangeOrganizationService', 'userProfileService', 'foreignStudentService', 'notifications', '$window', '$state', 'ngDialog',
        function ($scope, foreignStudentExchangeService, lookupTablesService, foreignInstitutionService, exchangeOrganizationService, userProfileService, foreignStudentService, notifications, $window, $state, ngDialog) {
            var vm = $scope.vm = {};
            vm.foreignStudentExchange = {};
            vm.lookups = [];
            vm.foreignInstitutions = [];
            vm.userProfiles = [];
            vm.foreignStudents = [];
            vm.ExchangeOrganizations = [];
            vm.check = {
                student: false,
                institution: false,
                user: false,
                department: false,
                course: false,
                purpose: false,
                organization: false,
                semester: false,
                duration: false,
                tfunding: false,
                sfunding: false
            };

            vm.addUserProfile = function () {
                ngDialog.open({
                    template: 'app/user-profile/add-user-profile.html',
                    controller: 'AddUserProfileController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addForeignStudent = function () {
                ngDialog.open({
                    template: 'app/foreign-student/add-foreign-student.html',
                    controller: 'AddForeignStudentController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addExchangeOrganization = function () {
                ngDialog.open({
                    template: 'app/exchange-organization/add-exchange-organization.html',
                    controller: 'AddExchangeOrganizationController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchUsers = function () {
                return userProfileService.fetch()
                .error(function (data) {
                })
                .success(function (data) {
                    vm.userProfiles = data;
                });
            };

            vm.fetchForeignStudents = function () {
                return foreignStudentService.fetch()
                .error(function (data) {
                })
                .success(function (data) {
                    vm.foreignStudents = data;
                });
            };

            vm.fetchForeignInstitutions = function () {
                return foreignInstitutionService.fetch()
                .error(function (data) {
                })
                .success(function (data) {
                    vm.foreignInstitutions = data;
                });
            };

            vm.fetchExchangeOrganizations = function () {
                return exchangeOrganizationService.fetch()
                .error(function (data) {
                })
                .success(function (data) {
                    vm.ExchangeOrganizations = data;
                });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('Departments, Courses, VisitPurposes, Durations, Fundings, Semesters')
                .then(function (data) {
                    vm.lookups = data;
                },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchUsers();
                vm.fetchForeignStudents();
                vm.fetchForeignInstitutions();
                vm.fetchExchangeOrganizations();
                vm.fetchLookups();
            }
            vm.fetch();

            vm.post = function (item) {
                if (item.Department || item.Duration || item.TravelFunding || item.FundingOfStay ||
                    item.VisitPurpose || item.Course || item.Semester) {
                    lookupTablesService.clearCache();
                };

                foreignStudentExchangeService.post(item)
                    .success(function (data) {
                        $scope.closeThisDialog();
                    })
                    .error(function (data) {
                        console.log(item);
                    });
            };
        }]);
})(angular);