using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class exchangeorganization
    {
        public exchangeorganization()
        {
            this.foreignstudentexchanges = new List<foreignstudentexchange>();
            this.ourstudentexchanges = new List<ourstudentexchange>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public string Comment { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<foreignstudentexchange> foreignstudentexchanges { get; set; }
        public virtual ICollection<ourstudentexchange> ourstudentexchanges { get; set; }
    }
}
