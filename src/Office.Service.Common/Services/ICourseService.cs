﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface ICourseService
    {
        #region Methods

        Task<int> DeleteAsync(ICourse entity);

        Task<int> DeleteAsync(string ID);

        Task<List<ICourse>> GetAsync();

        Task<ICourse> GetAsync(string ID);

        Task<int> InsertAsync(ICourse entity);

        Task<int> UpdateAsync(ICourse entity);

        #endregion Methods
    }
}