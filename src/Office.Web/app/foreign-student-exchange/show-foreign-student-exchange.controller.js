(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowForeignStudentExchangeController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.foreignStudentExchange = $scope.ngDialogData;
        }]);
})(angular);