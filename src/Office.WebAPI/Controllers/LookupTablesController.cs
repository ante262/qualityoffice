﻿using Office.Model.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/LookupTables")]
    public class LookupTablesController : ApiController
    {
        #region Constructors

        public LookupTablesController(ILookupTablesService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private ILookupTablesService Service { get; set; }

        #endregion Properties

        #region Methods

        [HttpGet]
        [Route("")]
        // Embed string will be in the format of: 'Chairs, Committees' when we want to return only Chairs and
        // Committees. When we want to return all lookup tables we use the string 'All'
        public async Task<HttpResponseMessage> Get(string embed)
        {
            try
            {
                LookupTablesModel result = new LookupTablesModel();
                var resultService = await Service.GetAsync();

                if (resultService != null)
                {
                    if (embed == "All")
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, resultService);
                    }
                    else
                    {
                        var returnTypes = embed.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var returnType in returnTypes)
                        {
                            result[returnType] = resultService[returnType];
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public class LookupTablesModel : ILookupTablesModel
        {
            #region Properties

            public List<IChair> Chairs { get; set; }
            public List<ICommitteeMemberRole> CommitteeMemberRoles { get; set; }
            public List<ICommittee> Committees { get; set; }
            public List<IConferenceType> ConferenceTypes { get; set; }
            public List<IContact> Contacts { get; set; }
            public List<IContractType> ContractTypes { get; set; }
            public List<ICourse> Courses { get; set; }
            public List<IDepartment> Departments { get; set; }
            public List<IDuration> Durations { get; set; }
            public List<IElectingInstitution> ElectingInstitutions { get; set; }
            public List<IExchangeOrganization> ExchangeOrganizations { get; set; }
            public List<IFunding> Fundings { get; set; }
            public List<IResearchGroup> ResearchGroups { get; set; }
            public List<IRole> Roles { get; set; }
            public List<ISemester> Semesters { get; set; }
            public List<IUserAcademicRank> UserAcademicRanks { get; set; }
            public List<IUserTitle> UserTitles { get; set; }
            public List<IVisitPurpose> VisitPurposes { get; set; }
            public List<IVisitType> VisitTypes { get; set; }

            // Modifies the getter and setter so you could access a property with object["propertyName"] syntax
            public object this[string propertyName]
            {
                get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
                set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
            }

            #endregion Properties
        }

        #endregion Classes
    }
}