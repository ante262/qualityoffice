ALTER TABLE `officeapp`.`visitorvisittype` 
DROP FOREIGN KEY `visitorvisittype_visitorvisit`,
DROP FOREIGN KEY `visitorvisittype_visittype`;
ALTER TABLE `officeapp`.`visitorvisittype` 
ADD CONSTRAINT `visitorvisittype_visitorvisit`
  FOREIGN KEY (`VisitorVisitId`)
  REFERENCES `officeapp`.`visitorvisit` (`ID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION,
ADD CONSTRAINT `visitorvisittype_visittype`
  FOREIGN KEY (`VisitTypeId`)
  REFERENCES `officeapp`.`visittype` (`ID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  
  ALTER TABLE `officeapp`.`professorvisittype` 
DROP FOREIGN KEY `professorvisittype_ourprofessorvisit`,
DROP FOREIGN KEY `professorvisittype_visittype`;
ALTER TABLE `officeapp`.`professorvisittype` 
ADD CONSTRAINT `professorvisittype_ourprofessorvisit`
  FOREIGN KEY (`OurProfessorVisitId`)
  REFERENCES `officeapp`.`ourprofessorvisit` (`ID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION,
ADD CONSTRAINT `professorvisittype_visittype`
  FOREIGN KEY (`VisitTypeId`)
  REFERENCES `officeapp`.`visittype` (`ID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;

  ALTER TABLE `officeapp`.`userrole` 
DROP FOREIGN KEY `ApplicationUser_Roles`,
DROP FOREIGN KEY `IdentityRole_Users`;
ALTER TABLE `officeapp`.`userrole` 
ADD CONSTRAINT `ApplicationUser_Roles`
  FOREIGN KEY (`UserId`)
  REFERENCES `officeapp`.`user` (`ID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION,
ADD CONSTRAINT `IdentityRole_Users`
  FOREIGN KEY (`RoleId`)
  REFERENCES `officeapp`.`role` (`ID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;

  ALTER TABLE `officeapp`.`conferenceinstitution` 
DROP FOREIGN KEY `conferenceinstitution_conference`,
DROP FOREIGN KEY `conferenceinstitution_foreigninstitution`;
ALTER TABLE `officeapp`.`conferenceinstitution` 
ADD CONSTRAINT `conferenceinstitution_conference`
  FOREIGN KEY (`ConferenceId`)
  REFERENCES `officeapp`.`conference` (`ID`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `conferenceinstitution_foreigninstitution`
  FOREIGN KEY (`ForeignInstitutionId`)
  REFERENCES `officeapp`.`foreigninstitution` (`ID`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
