﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IOurStudentExchangeService
    {
        #region Methods

        Task<int> DeleteAsync(IOurStudentExchange entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IOurStudentExchange>> GetAsync();

        Task<IOurStudentExchange> GetAsync(string ID);

        Task<int> InsertAsync(IOurStudentExchange entity);

        Task<int> UpdateAsync(IOurStudentExchange entity);

        #endregion Methods
    }
}