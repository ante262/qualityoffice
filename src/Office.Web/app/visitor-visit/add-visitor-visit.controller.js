(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddVisitorVisitController', ['$scope', 'visitorVisitService', 'userProfileService', 'visitorProfileService', 'lookupTablesService', 'notifications', '$window', '$state', 'ngDialog',
        function ($scope, visitorVisitService, userProfileService, visitorProfileService, lookupTablesService, notifications, $window, $state, ngDialog) {
            var vm = $scope.vm = {};
            vm.visitorVisit = {};
            vm.lookups = [];
            vm.userProfiles = [];
            vm.visitorProfiles = [];
            vm.visitorVisit.VisitorVisitTypes = [];
            vm.check = {
                visitor: false,
                user: false,
                department: false,
                type: false,
                duration: false,
                tfunding: false,
                sfunding: false
            };

            vm.addUserProfile = function () {
                ngDialog.open({
                    template: 'app/user-profile/add-user-profile.html',
                    controller: 'AddUserProfileController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addVisitorProfile = function () {
                ngDialog.open({
                    template: 'app/visitor-profile/add-visitor-profile.html',
                    controller: 'AddVisitorProfileController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchUsers = function () {
                return userProfileService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.userProfiles = data;
                    });
            };

            vm.fetchVisitors = function () {
                return visitorProfileService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.visitorProfiles = data;
                    });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('Departments, Durations, Fundings, VisitTypes')
                .then(function (data) {
                    vm.lookups = data;
                },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchUsers();
                vm.fetchVisitors();
                vm.fetchLookups();
            }
            vm.fetch();

            vm.post = function (item) {
                if (item.Department || item.Duration || item.TravelFunding || item.FundingOfStay ||
                    item.VisitTypes || item.NewVisitTypes) {
                    lookupTablesService.clearCache();
                };
                if (vm.visitorVisit.NewVisitTypes) {
                    var res = vm.visitorVisit.NewVisitTypes.split(",");
                    for (var ind = 0; ind < res.length; ind++) {
                        vm.visitorVisit.VisitorVisitTypes.push({ ID: res[ind] });
                    };
                    vm.visitorVisit.NewVisitTypes = null;
                };

                visitorVisitService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log('error', data);
                    });
            };
        }]);
})(angular);