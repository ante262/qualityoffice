(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('OurProfessorVisitController', ['$scope', 'ourProfessorVisitService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, ourProfessorVisitService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.ourProfessorVisits = [];
            vm.ourProfessorVisit = {};
            vm.selectedOurProfessorVisit = null;
            vm.loading = true;

            vm.addOurProfessorVisit = function () {
                ngDialog.open({
                    template: 'app/our-professor-visit/add-our-professor-visit.html',
                    controller: 'AddOurProfessorVisitController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editOurProfessorVisit = function (data) {
                ngDialog.open({
                    template: 'app/our-professor-visit/edit-our-professor-visit.html',
                    controller: 'EditOurProfessorVisitController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.showOurProfessorVisit = function (data) {
                ngDialog.open({
                    template: 'app/our-professor-visit/show-our-professor-visit.html',
                    controller: 'ShowOurProfessorVisitController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return ourProfessorVisitService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('ourProfessorVisit', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get ourProfessorVisit:' + data.message);
                    $state.go('ourProfessorVisit', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.ourProfessorVisits = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedOurProfessorVisit = item;
                ourProfessorVisitService.get(vm.selectedOurProfessorVisit)
                    .success(function (data) {
                        vm.ourProfessorVisit = data;
                    }).error(function (data) {
                        console.log("Unable to get this ourProfessorVisit:" + data.message);
                    });
            };

            vm.delete = function (data) {
                vm.ourProfessorVisit = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Boravak djelatnika '" + vm.ourProfessorVisit.UserProfile + "' će biti obrisan!")) {
                    ourProfessorVisitService.delete(vm.ourProfessorVisit.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);