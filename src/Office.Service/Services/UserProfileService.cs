﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Office.Service
{
    public class UserProfileService : IUserProfileService
    {
        #region Constructors

        public UserProfileService(IRepository repository, ILookupTablesService lookupTablesService)
        {
            Repository = repository;
            LookupTablesService = lookupTablesService;
        }

        #endregion Constructors

        #region Properties

        protected ILookupTablesService LookupTablesService { get; private set; }
        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IUserProfile entity)
        {
            return Repository.DeleteAsync<userprofile>(Mapper.Map<userprofile>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<userprofile>(ID);
        }

        public async Task<List<IUserProfile>> GetAsync()
        {
            // include UserTitle, UAR, RG and Chair tables into UserProfile table
            var result = await Repository.WhereAsync<userprofile>()
                          .Include(ut => ut.usertitle)
                          .Include(uar => uar.useracademicrank)
                          .Include(rg => rg.researchgroup)
                          .Include(c => c.chair)
                          .ToListAsync<userprofile>();

            return Mapper.Map<List<IUserProfile>>(result);
        }

        public async Task<IUserProfile> GetAsync(string ID)
        {
            return Mapper.Map<IUserProfile>(await Repository.SingleAsync<userprofile>(ID));
        }

        public Task<int> InsertAsync(IUserProfile entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            var counter = 0;
            if (entity.UserTitle != null && entity.UserTitle.ID == null)
            {
                entity.UserTitle.ID = Guid.NewGuid().ToString("N");
                entity.TitleId = entity.UserTitle.ID;
                counter++;
            }
            if (entity.UserAcademicRank != null && entity.UserAcademicRank.ID == null)
            {
                entity.UserAcademicRank.ID = Guid.NewGuid().ToString("N");
                entity.AcademicRankId = entity.UserAcademicRank.ID;
                counter++;
            }
            if (entity.Chair != null && entity.Chair.ID == null)
            {
                entity.Chair.ID = Guid.NewGuid().ToString("N");
                entity.ChairId = entity.Chair.ID;
                counter++;
            }
            if (entity.ResearchGroup != null && entity.ResearchGroup.ID == null)
            {
                entity.ResearchGroup.ID = Guid.NewGuid().ToString("N");
                entity.ResearchGroupId = entity.ResearchGroup.ID;
                counter++;
            }
            if (counter > 0)
            {
                LookupTablesService.ClearCache();
            }

            return Repository.InsertAsync<userprofile>(Mapper.Map<userprofile>(entity));
        }

        public async Task<int> UpdateAsync(IUserProfile entity)
        {
            var lookupItems = await LookupTablesService.GetAsync();

            var counter = 0;
            var unitOfWork = Repository.CreateUnitOfWork();
            if (entity.UserTitle != null
                && !lookupItems.UserTitles.Select(c => c.Name).Contains(entity.UserTitle.Name))
            {
                entity.UserTitle.ID = Guid.NewGuid().ToString("N");
                entity.TitleId = entity.UserTitle.ID;
                var map = Mapper.Map<usertitle>(entity.UserTitle);
                await unitOfWork.AddAsync<usertitle>(map);
                counter++;
            }
            if (entity.UserTitle != null
                && entity.UserTitle.ID == entity.TitleId
                && lookupItems.UserTitles
                .Select(c => c.Name.ToLower())
                .Contains(entity.UserTitle.Name.ToLower()))
            {
                entity.TitleId = lookupItems.UserTitles
                    .Where(c => c.Name.ToLower() == entity.UserTitle.Name.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.UserAcademicRank != null
                && !lookupItems.UserAcademicRanks
                .Select(c => c.Name.ToLower())
                .Contains(entity.UserAcademicRank.Name.ToLower()))
            {
                entity.UserAcademicRank.ID = Guid.NewGuid().ToString("N");
                entity.AcademicRankId = entity.UserAcademicRank.ID;
                var map = Mapper.Map<useracademicrank>(entity.UserAcademicRank);
                await unitOfWork.AddAsync<useracademicrank>(map);
                counter++;
            }
            if (entity.UserAcademicRank != null
                && entity.UserAcademicRank.ID == entity.AcademicRankId
                && lookupItems.UserAcademicRanks
                .Select(c => c.Name.ToLower())
                .Contains(entity.UserAcademicRank.Name.ToLower()))
            {
                entity.AcademicRankId = lookupItems.UserAcademicRanks
                    .Where(c => c.Name.ToLower() == entity.UserAcademicRank.Name.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.ResearchGroup != null
                && !lookupItems.ResearchGroups.Select(c => c.Name).Contains(entity.ResearchGroup.Name))
            {
                entity.ResearchGroup.ID = Guid.NewGuid().ToString("N");
                entity.ResearchGroupId = entity.ResearchGroup.ID;
                var map = Mapper.Map<funding>(entity.ResearchGroup);
                await unitOfWork.AddAsync<funding>(map);
                counter++;
            }
            if (entity.ResearchGroup != null
                && entity.ResearchGroup.ID == entity.ResearchGroupId
                && lookupItems.ResearchGroups
                .Select(c => c.Name.ToLower())
                .Contains(entity.ResearchGroup.Name.ToLower()))
            {
                entity.ResearchGroupId = lookupItems.ResearchGroups
                    .Where(c => c.Name.ToLower() == entity.ResearchGroup.Name.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.Chair != null
                && !lookupItems.Chairs.Select(c => c.Name).Contains(entity.Chair.Name))
            {
                entity.Chair.ID = Guid.NewGuid().ToString("N");
                entity.ChairId = entity.Chair.ID;
                var map = Mapper.Map<funding>(entity.Chair);
                await unitOfWork.AddAsync<funding>(map);
                counter++;
            }
            if (entity.Chair != null
                && entity.Chair.ID == entity.ChairId
                && lookupItems.Chairs
                .Select(c => c.Name.ToLower())
                .Contains(entity.Chair.Name.ToLower()))
            {
                entity.ChairId = lookupItems.Chairs
                    .Where(c => c.Name.ToLower() == entity.Chair.Name.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }

            entity.Chair = null;
            entity.UserAcademicRank = null;
            entity.UserTitle = null;
            entity.ResearchGroup = null;
            await unitOfWork.UpdateAsync<userprofile>(Mapper.Map<userprofile>(entity));
            if (counter > 0)
            {
                await LookupTablesService.ClearCache();
            }
            return await unitOfWork.CommitAsync();
        }

        #endregion Methods
    }
}