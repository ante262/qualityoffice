using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class conferenceinstitutionMap : EntityTypeConfiguration<conferenceinstitution>
    {
        public conferenceinstitutionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.ConferenceId)
                .HasMaxLength(128);

            this.Property(t => t.ForeignInstitutionId)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("conferenceinstitution", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ConferenceId).HasColumnName("ConferenceId");
            this.Property(t => t.ForeignInstitutionId).HasColumnName("ForeignInstitutionId");

            // Relationships
            this.HasOptional(t => t.conference)
                .WithMany(t => t.conferenceinstitutions)
                .HasForeignKey(d => d.ConferenceId);
            this.HasOptional(t => t.foreigninstitution)
                .WithMany(t => t.conferenceinstitutions)
                .HasForeignKey(d => d.ForeignInstitutionId);

        }
    }
}
