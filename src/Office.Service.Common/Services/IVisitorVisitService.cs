﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IVisitorVisitService
    {
        #region Methods

        Task<int> DeleteAsync(IVisitorVisit entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IVisitorVisit>> GetAsync();

        Task<IVisitorVisit> GetAsync(string ID);

        Task<int> InsertAsync(IVisitorVisit entity);

        Task<int> UpdateAsync(IVisitorVisit entity);

        #endregion Methods
    }
}