(function (angular) {
    'use strict';
    angular.module('officeApp').controller('EditConferenceController', ['$scope', 'conferenceService', 'lookupTablesService', 'foreignInstitutionService', 'notifications', 'ngDialog',
        function ($scope, conferenceService, lookupTablesService, foreignInstitutionService, notifications, ngDialog) {
            var vm = $scope.vm = {};
            vm.conference = $scope.ngDialogData;

            if (vm.conference && vm.conference.Date) {
                vm.conference.Date = new Date(vm.conference.Date);
            }

            vm.lookups = [];
            vm.locality = [{ id: 1, Type: "Domaći" }, { id: 2, Type: "Međunarodni" }];
            vm.profession = [{ id: 1, Type: "Znanstveni" }, { id: 2, Type: "Stručni" }];
            vm.foreignInstitutions = [];
            vm.check = {
                institution: false,
                type: false
            };

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchForeignInstitutions = function () {
                return foreignInstitutionService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.foreignInstitutions = data;
                    });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('ConferenceTypes')
                    .then(function (data) {
                        vm.lookups = data;
                    },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchForeignInstitutions();
                vm.fetchLookups();
            }
            vm.fetch();

            vm.put = function (item) {
                if (vm.conference.ConferenceInstitutions) {
                    for (var ind = 0; ind < vm.conference.ConferenceInstitutions.length; ind++) {
                        vm.conference.ConferenceInstitutions[ind].ForeignInstitution = null;
                    };
                };
                if (item.ConferenceType) {
                    lookupTablesService.clearCache();
                };

                conferenceService.put(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                    });
            };
        }]);
})(angular);