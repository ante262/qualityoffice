(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddConferenceController', ['$scope', 'conferenceService', 'lookupTablesService', 'foreignInstitutionService', 'notifications', 'ngDialog',
        function ($scope, conferenceService, lookupTablesService, foreignInstitutionService, notifications, ngDialog) {
            var vm = $scope.vm = {};
            vm.conference = {};
            vm.lookups = [];
            vm.locality = [{ id: 1, Type: "Domaći" }, { id: 2, Type: "Međunarodni" }];
            vm.profession = [{ id: 1, Type: "Znanstveni" }, { id: 2, Type: "Stručni" }];
            vm.foreignInstitutions = [];
            vm.conference.ConferenceInstitutions = [];
            vm.check = {
                type: false,
                institution: false
            };

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchForeignInstitutions = function () {
                return foreignInstitutionService.fetch()
                    .error(function (data) {
                    }).success(function (data) {
                        vm.foreignInstitutions = data;
                    });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('ConferenceTypes').then(function (data) {
                    vm.lookups = data;
                },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchForeignInstitutions();
                vm.fetchLookups();
            }
            vm.fetch();

            vm.post = function (item) {
                conferenceService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                    });
            };
        }]);
})(angular);