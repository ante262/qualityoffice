(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ContractController', ['$scope', 'contractService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, contractService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.contracts = [];
            vm.contract = {};
            vm.selectedContract = null;
            vm.loading = true;

            vm.addContract = function () {
                ngDialog.open({
                    template: 'app/contract/add-contract.html',
                    controller: 'AddContractController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editContract = function (data) {
                ngDialog.open({
                    template: 'app/contract/edit-contract.html',
                    controller: 'EditContractController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            }

            vm.showContract = function (data) {
                ngDialog.open({
                    template: 'app/contract/show-contract.html',
                    controller: 'ShowContractController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            }

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return contractService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('contract', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get contract:' + data.message);
                    $state.go('contract', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.contracts = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedContract = item;
                contractService.get(vm.selectedContract)
                    .success(function (data) {
                        vm.contract = data;
                    }).error(function (data) {
                        console.log("Unable to get this contract:" + data.message);
                    });
            };

            vm.delete = function (data) {
                vm.contract = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Ugovor '" + vm.contract.Name + "' će biti obrisan!")) {
                    contractService.delete(vm.contract.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);