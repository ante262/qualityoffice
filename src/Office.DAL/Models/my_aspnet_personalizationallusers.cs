using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class my_aspnet_personalizationallusers
    {
        public string pathId { get; set; }
        public byte[] pageSettings { get; set; }
        public System.DateTime lastUpdatedDate { get; set; }
    }
}
