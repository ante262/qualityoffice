﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;

namespace Office.Model.Mapping
{
    public static class AutoMapperMaps
    {
        #region Methods

        public static void Initialize()
        {
            Mapper.CreateMap<UserPOCO, user>().ReverseMap();
            Mapper.CreateMap<IUser, user>().ReverseMap();
            Mapper.CreateMap<UserPOCO, IUser>().ReverseMap();

            Mapper.CreateMap<RolePOCO, role>().ReverseMap();
            Mapper.CreateMap<IRole, role>().ReverseMap();
            Mapper.CreateMap<RolePOCO, IRole>().ReverseMap();

            Mapper.CreateMap<ChairPOCO, chair>().ReverseMap();
            Mapper.CreateMap<IChair, chair>().ReverseMap();
            Mapper.CreateMap<ChairPOCO, IChair>().ReverseMap();

            Mapper.CreateMap<ResearchGroupPOCO, researchgroup>().ReverseMap();
            Mapper.CreateMap<IResearchGroup, researchgroup>().ReverseMap();
            Mapper.CreateMap<ResearchGroupPOCO, IResearchGroup>().ReverseMap();

            Mapper.CreateMap<UserAcademicRankPOCO, useracademicrank>().ReverseMap();
            Mapper.CreateMap<IUserAcademicRank, useracademicrank>().ReverseMap();
            Mapper.CreateMap<UserAcademicRankPOCO, IUserAcademicRank>().ReverseMap();

            Mapper.CreateMap<UserProfilePOCO, userprofile>().ReverseMap();
            Mapper.CreateMap<IUserProfile, userprofile>().ReverseMap();
            Mapper.CreateMap<UserProfilePOCO, IUserProfile>().ReverseMap();

            Mapper.CreateMap<UserTitlePOCO, usertitle>().ReverseMap();
            Mapper.CreateMap<IUserTitle, usertitle>().ReverseMap();
            Mapper.CreateMap<UserTitlePOCO, IUserTitle>().ReverseMap();

            Mapper.CreateMap<CommitteeMemberPOCO, committeemember>().ReverseMap();
            Mapper.CreateMap<ICommitteeMember, committeemember>().ReverseMap();
            Mapper.CreateMap<CommitteeMemberPOCO, ICommitteeMember>().ReverseMap();

            Mapper.CreateMap<CommitteePOCO, committee>().ReverseMap();
            Mapper.CreateMap<ICommittee, committee>().ReverseMap();
            Mapper.CreateMap<CommitteePOCO, ICommittee>().ReverseMap();

            Mapper.CreateMap<CommitteeMemberRolePOCO, committeememberrole>().ReverseMap();
            Mapper.CreateMap<ICommitteeMemberRole, committeememberrole>().ReverseMap();
            Mapper.CreateMap<CommitteeMemberRolePOCO, ICommitteeMemberRole>().ReverseMap();

            Mapper.CreateMap<ConferenceInstitutionPOCO, conferenceinstitution>().ReverseMap();
            Mapper.CreateMap<IConferenceInstitution, conferenceinstitution>().ReverseMap();
            Mapper.CreateMap<ConferenceInstitutionPOCO, IConferenceInstitution>().ReverseMap();

            Mapper.CreateMap<ConferencePOCO, conference>().ReverseMap();
            Mapper.CreateMap<IConference, conference>().ReverseMap();
            Mapper.CreateMap<ConferencePOCO, IConference>().ReverseMap();

            Mapper.CreateMap<ConferenceTypePOCO, conferencetype>().ReverseMap();
            Mapper.CreateMap<IConferenceType, conferencetype>().ReverseMap();
            Mapper.CreateMap<ConferenceTypePOCO, IConferenceType>().ReverseMap();

            Mapper.CreateMap<ContactPOCO, contact>().ReverseMap();
            Mapper.CreateMap<IContact, contact>().ReverseMap();
            Mapper.CreateMap<ContactPOCO, IContact>().ReverseMap();

            Mapper.CreateMap<ContractPOCO, contract>().ReverseMap();
            Mapper.CreateMap<IContract, contract>().ReverseMap();
            Mapper.CreateMap<ContractPOCO, IContract>().ReverseMap();

            Mapper.CreateMap<ContractTypePOCO, contracttype>().ReverseMap();
            Mapper.CreateMap<IContractType, contracttype>().ReverseMap();
            Mapper.CreateMap<ContractTypePOCO, IContractType>().ReverseMap();

            Mapper.CreateMap<CoursePOCO, course>().ReverseMap();
            Mapper.CreateMap<ICourse, course>().ReverseMap();
            Mapper.CreateMap<CoursePOCO, ICourse>().ReverseMap();

            Mapper.CreateMap<DepartmentPOCO, department>().ReverseMap();
            Mapper.CreateMap<IDepartment, department>().ReverseMap();
            Mapper.CreateMap<DepartmentPOCO, IDepartment>().ReverseMap();

            Mapper.CreateMap<DurationPOCO, duration>().ReverseMap();
            Mapper.CreateMap<IDuration, duration>().ReverseMap();
            Mapper.CreateMap<DurationPOCO, IDuration>().ReverseMap();

            Mapper.CreateMap<ElectingInstitutionPOCO, electinginstitution>().ReverseMap();
            Mapper.CreateMap<IElectingInstitution, electinginstitution>().ReverseMap();
            Mapper.CreateMap<ElectingInstitutionPOCO, IElectingInstitution>().ReverseMap();

            Mapper.CreateMap<ExchangeOrganizationPOCO, exchangeorganization>().ReverseMap();
            Mapper.CreateMap<IExchangeOrganization, exchangeorganization>().ReverseMap();
            Mapper.CreateMap<ExchangeOrganizationPOCO, IExchangeOrganization>().ReverseMap();

            Mapper.CreateMap<ForeignInstitutionPOCO, foreigninstitution>().ReverseMap();
            Mapper.CreateMap<IForeignInstitution, foreigninstitution>().ReverseMap();
            Mapper.CreateMap<ForeignInstitutionPOCO, IForeignInstitution>().ReverseMap();

            Mapper.CreateMap<ForeignStudentExchangePOCO, foreignstudentexchange>().ReverseMap();
            Mapper.CreateMap<IForeignStudentExchange, foreignstudentexchange>().ReverseMap();
            Mapper.CreateMap<ForeignStudentExchangePOCO, IForeignStudentExchange>().ReverseMap();

            Mapper.CreateMap<ForeignStudentPOCO, foreignstudent>().ReverseMap();
            Mapper.CreateMap<IForeignStudent, foreignstudent>().ReverseMap();
            Mapper.CreateMap<ForeignStudentPOCO, IForeignStudent>().ReverseMap();

            Mapper.CreateMap<FundingPOCO, funding>().ReverseMap();
            Mapper.CreateMap<IFunding, funding>().ReverseMap();
            Mapper.CreateMap<FundingPOCO, IFunding>().ReverseMap();

            Mapper.CreateMap<NonPublicationLecturePOCO, nonpublicationlecture>().ReverseMap();
            Mapper.CreateMap<INonPublicationLecture, nonpublicationlecture>().ReverseMap();
            Mapper.CreateMap<NonPublicationLecturePOCO, INonPublicationLecture>().ReverseMap();

            Mapper.CreateMap<OurProfessorVisitPOCO, ourprofessorvisit>().ReverseMap();
            Mapper.CreateMap<IOurProfessorVisit, ourprofessorvisit>().ReverseMap();
            Mapper.CreateMap<OurProfessorVisitPOCO, IOurProfessorVisit>().ReverseMap();

            Mapper.CreateMap<OurStudentExchangePOCO, ourstudentexchange>().ReverseMap();
            Mapper.CreateMap<IOurStudentExchange, ourstudentexchange>().ReverseMap();
            Mapper.CreateMap<OurStudentExchangePOCO, IOurStudentExchange>().ReverseMap();

            Mapper.CreateMap<ProfessorVisitTypePOCO, professorvisittype>().ReverseMap();
            Mapper.CreateMap<IProfessorVisitType, professorvisittype>().ReverseMap();
            Mapper.CreateMap<ProfessorVisitTypePOCO, IProfessorVisitType>().ReverseMap();

            Mapper.CreateMap<SemesterPOCO, semester>().ReverseMap();
            Mapper.CreateMap<ISemester, semester>().ReverseMap();
            Mapper.CreateMap<SemesterPOCO, ISemester>().ReverseMap();

            Mapper.CreateMap<VisitorProfilePOCO, visitorprofile>().ReverseMap();
            Mapper.CreateMap<IVisitorProfile, visitorprofile>().ReverseMap();
            Mapper.CreateMap<VisitorProfilePOCO, IVisitorProfile>().ReverseMap();

            Mapper.CreateMap<VisitorVisitPOCO, visitorvisit>().ReverseMap();
            Mapper.CreateMap<IVisitorVisit, visitorvisit>().ReverseMap();
            Mapper.CreateMap<VisitorVisitPOCO, IVisitorVisit>().ReverseMap();

            Mapper.CreateMap<VisitorVisitTypePOCO, visitorvisittype>().ReverseMap();
            Mapper.CreateMap<IVisitorVisitType, visitorvisittype>().ReverseMap();
            Mapper.CreateMap<VisitorVisitTypePOCO, IVisitorVisitType>().ReverseMap();

            Mapper.CreateMap<VisitPurposePOCO, visitpurpose>().ReverseMap();
            Mapper.CreateMap<IVisitPurpose, visitpurpose>().ReverseMap();
            Mapper.CreateMap<VisitPurposePOCO, IVisitPurpose>().ReverseMap();

            Mapper.CreateMap<VisitTypePOCO, visittype>().ReverseMap();
            Mapper.CreateMap<IVisitType, visittype>().ReverseMap();
            Mapper.CreateMap<VisitTypePOCO, IVisitType>().ReverseMap();

            // Maps for Conference table Get ConferenceType from ConferenceType table
            Mapper.CreateMap<IConference, conference>()
               .ForMember(dest => dest.conferencetype, opts => opts.MapFrom(src => src.ConferenceType))
               .ForMember(dest => dest.conferenceinstitutions, opts => opts.MapFrom(src => src.ConferenceInstitutions))
               .ReverseMap()
               .ForMember(dest => dest.ConferenceType, opts => opts.MapFrom(src => src.conferencetype))
               .ForMember(dest => dest.ConferenceInstitutions, opts => opts.MapFrom(src => src.conferenceinstitutions));

            // Maps for UserProfile table:
            Mapper.CreateMap<IUserProfile, userprofile>()
               .ForMember(dest => dest.usertitle, opts => opts.MapFrom(src => src.UserTitle))
               .ForMember(dest => dest.useracademicrank, opts => opts.MapFrom(src => src.UserAcademicRank))
               .ForMember(dest => dest.chair, opts => opts.MapFrom(src => src.Chair))
               .ForMember(dest => dest.researchgroup, opts => opts.MapFrom(src => src.ResearchGroup))
               .ReverseMap()
               .ForMember(dest => dest.UserTitle, opts => opts.MapFrom(src => src.usertitle))
               .ForMember(dest => dest.UserAcademicRank, opts => opts.MapFrom(src => src.useracademicrank))
               .ForMember(dest => dest.Chair, opts => opts.MapFrom(src => src.chair))
               .ForMember(dest => dest.ResearchGroup, opts => opts.MapFrom(src => src.researchgroup));

            // Maps for ForeignStudentExchange table:
            Mapper.CreateMap<IForeignStudentExchange, foreignstudentexchange>()
               .ForMember(dest => dest.course, opts => opts.MapFrom(src => src.Course))
               .ForMember(dest => dest.department, opts => opts.MapFrom(src => src.Department))
               .ForMember(dest => dest.duration, opts => opts.MapFrom(src => src.Duration))
               .ForMember(dest => dest.exchangeorganization, opts => opts.MapFrom(src => src.ExchangeOrganization))
               .ForMember(dest => dest.foreigninstitution, opts => opts.MapFrom(src => src.ForeignInstitution))
               .ForMember(dest => dest.foreignstudent, opts => opts.MapFrom(src => src.ForeignStudent))
               .ForMember(dest => dest.funding1, opts => opts.MapFrom(src => src.TravelFunding))
               .ForMember(dest => dest.funding, opts => opts.MapFrom(src => src.FundingOfStay))
               .ForMember(dest => dest.semester, opts => opts.MapFrom(src => src.Semester))
               .ForMember(dest => dest.userprofile, opts => opts.MapFrom(src => src.UserProfile))
               .ForMember(dest => dest.visitpurpose, opts => opts.MapFrom(src => src.VisitPurpose))
               .ReverseMap()
               .ForMember(dest => dest.Course, opts => opts.MapFrom(src => src.course))
               .ForMember(dest => dest.Department, opts => opts.MapFrom(src => src.department))
               .ForMember(dest => dest.Duration, opts => opts.MapFrom(src => src.duration))
               .ForMember(dest => dest.ExchangeOrganization, opts => opts.MapFrom(src => src.exchangeorganization))
               .ForMember(dest => dest.ForeignInstitution, opts => opts.MapFrom(src => src.foreigninstitution))
               .ForMember(dest => dest.ForeignStudent, opts => opts.MapFrom(src => src.foreignstudent))
               .ForMember(dest => dest.TravelFunding, opts => opts.MapFrom(src => src.funding1))
               .ForMember(dest => dest.FundingOfStay, opts => opts.MapFrom(src => src.funding))
               .ForMember(dest => dest.Semester, opts => opts.MapFrom(src => src.semester))
               .ForMember(dest => dest.UserProfile, opts => opts.MapFrom(src => src.userprofile))
               .ForMember(dest => dest.VisitPurpose, opts => opts.MapFrom(src => src.visitpurpose));

            // Maps for CommitteeMember table:
            Mapper.CreateMap<ICommitteeMember, committeemember>()
               .ForMember(dest => dest.committee, opts => opts.MapFrom(src => src.Committee))
               .ForMember(dest => dest.committeememberrole, opts => opts.MapFrom(src => src.CommitteeMemberRole))
               .ForMember(dest => dest.electinginstitution, opts => opts.MapFrom(src => src.ElectingInstitution))
               .ForMember(dest => dest.userprofile, opts => opts.MapFrom(src => src.UserProfile))
               .ReverseMap()
               .ForMember(dest => dest.Committee, opts => opts.MapFrom(src => src.committee))
               .ForMember(dest => dest.CommitteeMemberRole, opts => opts.MapFrom(src => src.committeememberrole))
               .ForMember(dest => dest.ElectingInstitution, opts => opts.MapFrom(src => src.electinginstitution))
               .ForMember(dest => dest.UserProfile, opts => opts.MapFrom(src => src.userprofile));

            // Maps for Contract table:
            Mapper.CreateMap<IContract, contract>()
               .ForMember(dest => dest.contracttype, opts => opts.MapFrom(src => src.ContractType))
               .ForMember(dest => dest.funding, opts => opts.MapFrom(src => src.Funding))
               .ForMember(dest => dest.duration, opts => opts.MapFrom(src => src.Duration))
               .ReverseMap()
               .ForMember(dest => dest.ContractType, opts => opts.MapFrom(src => src.contracttype))
               .ForMember(dest => dest.Funding, opts => opts.MapFrom(src => src.funding))
               .ForMember(dest => dest.Duration, opts => opts.MapFrom(src => src.duration));

            // Maps for NonPublicationLecture table Get ConferenceType from ConferenceType table
            Mapper.CreateMap<INonPublicationLecture, nonpublicationlecture>()
               .ForMember(dest => dest.conferencetype, opts => opts.MapFrom(src => src.ConferenceType))
               .ForMember(dest => dest.foreigninstitution, opts => opts.MapFrom(src => src.ForeignInstitution))
               .ReverseMap()
               .ForMember(dest => dest.ConferenceType, opts => opts.MapFrom(src => src.conferencetype))
               .ForMember(dest => dest.ForeignInstitution, opts => opts.MapFrom(src => src.foreigninstitution));

            // Maps for ForeignInstitution table:
            Mapper.CreateMap<IForeignInstitution, foreigninstitution>()
               .ForMember(dest => dest.contract, opts => opts.MapFrom(src => src.Contract))
               .ForMember(dest => dest.contact, opts => opts.MapFrom(src => src.Contact))
               .ForMember(dest => dest.userprofile, opts => opts.MapFrom(src => src.UserProfile))
               .ReverseMap()
               .ForMember(dest => dest.Contract, opts => opts.MapFrom(src => src.contract))
               .ForMember(dest => dest.Contact, opts => opts.MapFrom(src => src.contact))
               .ForMember(dest => dest.UserProfile, opts => opts.MapFrom(src => src.userprofile));

            // Maps for OurProfessorVisit table:
            Mapper.CreateMap<IOurProfessorVisit, ourprofessorvisit>()
               .ForMember(dest => dest.professorvisittypes, opts => opts.MapFrom(src => src.ProfessorVisitTypes))
               .ForMember(dest => dest.department, opts => opts.MapFrom(src => src.Department))
               .ForMember(dest => dest.duration, opts => opts.MapFrom(src => src.Duration))
               .ForMember(dest => dest.foreigninstitution, opts => opts.MapFrom(src => src.ForeignInstitution))
               .ForMember(dest => dest.funding1, opts => opts.MapFrom(src => src.TravelFunding))
               .ForMember(dest => dest.funding, opts => opts.MapFrom(src => src.FundingOfStay))
               .ForMember(dest => dest.userprofile, opts => opts.MapFrom(src => src.UserProfile))
               .ReverseMap()
               .ForMember(dest => dest.ProfessorVisitTypes, opts => opts.MapFrom(src => src.professorvisittypes))
               .ForMember(dest => dest.Department, opts => opts.MapFrom(src => src.department))
               .ForMember(dest => dest.Duration, opts => opts.MapFrom(src => src.duration))
               .ForMember(dest => dest.ForeignInstitution, opts => opts.MapFrom(src => src.foreigninstitution))
               .ForMember(dest => dest.TravelFunding, opts => opts.MapFrom(src => src.funding1))
               .ForMember(dest => dest.FundingOfStay, opts => opts.MapFrom(src => src.funding))
               .ForMember(dest => dest.UserProfile, opts => opts.MapFrom(src => src.userprofile));

            // Maps for OurStudentExchange table:
            Mapper.CreateMap<IOurStudentExchange, ourstudentexchange>()
               .ForMember(dest => dest.department, opts => opts.MapFrom(src => src.Department))
               .ForMember(dest => dest.duration, opts => opts.MapFrom(src => src.Duration))
               .ForMember(dest => dest.foreigninstitution, opts => opts.MapFrom(src => src.ForeignInstitution))
               .ForMember(dest => dest.funding1, opts => opts.MapFrom(src => src.TravelFunding))
               .ForMember(dest => dest.funding, opts => opts.MapFrom(src => src.FundingOfStay))
               .ForMember(dest => dest.semester, opts => opts.MapFrom(src => src.Semester))
               .ForMember(dest => dest.visitpurpose, opts => opts.MapFrom(src => src.VisitPurpose))
               .ReverseMap()
               .ForMember(dest => dest.Department, opts => opts.MapFrom(src => src.department))
               .ForMember(dest => dest.Duration, opts => opts.MapFrom(src => src.duration))
               .ForMember(dest => dest.ForeignInstitution, opts => opts.MapFrom(src => src.foreigninstitution))
               .ForMember(dest => dest.TravelFunding, opts => opts.MapFrom(src => src.funding1))
               .ForMember(dest => dest.FundingOfStay, opts => opts.MapFrom(src => src.funding))
               .ForMember(dest => dest.Semester, opts => opts.MapFrom(src => src.semester))
               .ForMember(dest => dest.VisitPurpose, opts => opts.MapFrom(src => src.visitpurpose));

            // Maps for VisitorProfile table
            Mapper.CreateMap<IVisitorProfile, visitorprofile>()
               .ForMember(dest => dest.usertitle, opts => opts.MapFrom(src => src.UserTitle))
               .ForMember(dest => dest.foreigninstitution, opts => opts.MapFrom(src => src.ForeignInstitution))
               .ReverseMap()
               .ForMember(dest => dest.UserTitle, opts => opts.MapFrom(src => src.usertitle))
               .ForMember(dest => dest.ForeignInstitution, opts => opts.MapFrom(src => src.foreigninstitution));

            // Maps for VisitorVisit table:
            Mapper.CreateMap<IVisitorVisit, visitorvisit>()
               .ForMember(dest => dest.visitorvisittypes, opts => opts.MapFrom(src => src.VisitorVisitTypes))
               .ForMember(dest => dest.department, opts => opts.MapFrom(src => src.Department))
               .ForMember(dest => dest.duration, opts => opts.MapFrom(src => src.Duration))
               .ForMember(dest => dest.visitorprofile, opts => opts.MapFrom(src => src.VisitorProfile))
               .ForMember(dest => dest.funding1, opts => opts.MapFrom(src => src.TravelFunding))
               .ForMember(dest => dest.funding, opts => opts.MapFrom(src => src.FundingOfStay))
               .ForMember(dest => dest.userprofile, opts => opts.MapFrom(src => src.UserProfile))
               .ReverseMap()
               .ForMember(dest => dest.VisitorVisitTypes, opts => opts.MapFrom(src => src.visitorvisittypes))
               .ForMember(dest => dest.Department, opts => opts.MapFrom(src => src.department))
               .ForMember(dest => dest.Duration, opts => opts.MapFrom(src => src.duration))
               .ForMember(dest => dest.VisitorProfile, opts => opts.MapFrom(src => src.visitorprofile))
               .ForMember(dest => dest.TravelFunding, opts => opts.MapFrom(src => src.funding1))
               .ForMember(dest => dest.FundingOfStay, opts => opts.MapFrom(src => src.funding))
               .ForMember(dest => dest.UserProfile, opts => opts.MapFrom(src => src.userprofile));
        }

        #endregion Methods
    }
}