﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace Office.DAL.Models
{
    public interface IofficeappContext : IDisposable
    {
        #region Properties

        DbSet<chair> chairs { get; set; }
        DbSet<committeememberrole> committeememberroles { get; set; }
        DbSet<committeemember> committeemembers { get; set; }
        DbSet<committee> committees { get; set; }
        DbSet<conferenceinstitution> conferenceinstitutions { get; set; }
        DbSet<conference> conferences { get; set; }
        DbSet<conferencetype> conferencetypes { get; set; }
        DbSet<contact> contacts { get; set; }
        DbSet<contract> contracts { get; set; }
        DbSet<contracttype> contracttypes { get; set; }
        DbSet<course> courses { get; set; }
        DbSet<department> departments { get; set; }
        DbSet<duration> durations { get; set; }
        DbSet<electinginstitution> electinginstitutions { get; set; }
        DbSet<exchangeorganization> exchangeorganizations { get; set; }
        DbSet<foreigninstitution> foreigninstitutions { get; set; }
        DbSet<foreignstudentexchange> foreignstudentexchanges { get; set; }
        DbSet<foreignstudent> foreignstudents { get; set; }
        DbSet<funding> fundings { get; set; }
        DbSet<nonpublicationlecture> nonpublicationlectures { get; set; }
        DbSet<ourprofessorvisit> ourprofessorvisits { get; set; }
        DbSet<ourstudentexchange> ourstudentexchanges { get; set; }
        DbSet<professorvisittype> professorvisittypes { get; set; }
        DbSet<researchgroup> researchgroups { get; set; }
        DbSet<role> roles { get; set; }
        DbSet<semester> semesters { get; set; }
        DbSet<useracademicrank> useracademicranks { get; set; }
        DbSet<userclaim> userclaims { get; set; }
        DbSet<userlogin> userlogins { get; set; }
        DbSet<userprofile> userprofiles { get; set; }
        DbSet<user> users { get; set; }
        DbSet<usertitle> usertitles { get; set; }
        DbSet<visitorprofile> visitorprofiles { get; set; }
        DbSet<visitorvisit> visitorvisits { get; set; }
        DbSet<visitorvisittype> visitorvisittypes { get; set; }
        DbSet<visitpurpose> visitpurposes { get; set; }
        DbSet<visittype> visittypes { get; set; }

        #endregion Properties

        #region Methods

        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        Task<int> SaveChangesAsync();

        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        #endregion Methods
    }
}