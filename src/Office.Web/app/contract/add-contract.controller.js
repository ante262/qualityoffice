(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddContractController', ['$scope', 'contractService', 'lookupTablesService', 'notifications',
        function ($scope, contractService, lookupTablesService, notifications) {
            var vm = $scope.vm = {};
            vm.contract = {};
            vm.lookups = [];
            vm.check = {
                type: false,
                duration: false,
                funding: false
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('ContractTypes, Durations, Fundings')
                    .then(function (data) {
                        vm.lookups = data;
                    },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };
            vm.fetchLookups();

            vm.fetch = function () {
                vm.fetchLookups();
            }
            vm.fetch();

            vm.post = function (item) {
                if (item.ContractType || item.Duration || item.Funding) {
                    lookupTablesService.clearCache();
                };

                contractService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                    });
            };
        }]);
})(angular);