﻿using System;

namespace Office.Model.Common
{
    public interface IForeignStudentExchange
    {
        #region Properties

        Nullable<short> AcademicYear { get; set; }
        string Comment { get; set; }
        ICourse Course { get; set; }
        string CourseId { get; set; }
        IDepartment Department { get; set; }
        string DepartmentId { get; set; }
        IDuration Duration { get; set; }
        string DurationId { get; set; }
        IExchangeOrganization ExchangeOrganization { get; set; }
        string ExchangeOrganizationId { get; set; }
        string ExchangeResult { get; set; }
        IForeignInstitution ForeignInstitution { get; set; }
        string ForeignInstitutionId { get; set; }
        IForeignStudent ForeignStudent { get; set; }
        string ForeignStudentId { get; set; }
        IFunding FundingOfStay { get; set; }
        string FundingOfStayId { get; set; }
        string ID { get; set; }
        ISemester Semester { get; set; }
        string SemesterId { get; set; }
        Nullable<System.DateTime> StartDate { get; set; }
        IFunding TravelFunding { get; set; }
        string TravelFundingId { get; set; }
        IUserProfile UserProfile { get; set; }
        string UserProfileId { get; set; }
        IVisitPurpose VisitPurpose { get; set; }
        string VisitPurposeId { get; set; }

        #endregion Properties
    }
}