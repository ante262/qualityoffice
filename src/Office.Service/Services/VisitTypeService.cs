﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Office.Service
{
    public class VisitTypeService : IVisitTypeService
    {
        #region Constructors

        public VisitTypeService(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IVisitType entity)
        {
            return Repository.DeleteAsync<visittype>(Mapper.Map<visittype>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<visittype>(ID);
        }

        public async Task<List<IVisitType>> GetAsync()
        {
            return Mapper.Map<List<IVisitType>>(
                await Repository.WhereAsync<visittype>()
                        .ToListAsync<visittype>()
                );
        }

        public async Task<IVisitType> GetAsync(string ID)
        {
            return Mapper.Map<IVisitType>(await Repository.SingleAsync<visittype>(ID));
        }

        public Task<int> InsertAsync(IVisitType entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            return Repository.InsertAsync<visittype>(Mapper.Map<visittype>(entity));
        }

        public Task<int> UpdateAsync(IVisitType entity)
        {
            return Repository.UpdateAsync<visittype>(Mapper.Map<visittype>(entity));
        }

        #endregion Methods
    }
}