﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IChairService
    {
        #region Methods

        Task<int> DeleteAsync(IChair entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IChair>> GetAsync();

        Task<IChair> GetAsync(string ID);

        Task<int> InsertAsync(IChair entity);

        Task<int> UpdateAsync(IChair entity);

        #endregion Methods
    }
}