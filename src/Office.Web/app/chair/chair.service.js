﻿(function (angular) {
    'use strict';
    angular.module('officeApp').service('chairService', ['$http', 'baseUrl','$httpParamSerializerJQLike',
        function ($http, baseUrl, $httpParamSerializerJQLike) {
            var uarUrl = "Chair";

            return {
                //get all chairs
                fetch: function () {
                    return $http.get(baseUrl + uarUrl);
                },
                //get chair by id
                get: function (id) {
                    return $http.get(baseUrl + uarUrl + "/" + id);
                },
                //post chair
                post: function (data) {
                    var req = {
                        method: 'POST',
                        url: baseUrl + uarUrl,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //put chair
                put: function (data) {
                    var req = {
                        method: 'PUT',
                        url: baseUrl + uarUrl + "/" + data.ID,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $httpParamSerializerJQLike(data)
                    };
                    return $http(req);
                },

                //delete chair
                delete: function (item) {
                    var req = {
                        method: 'DELETE',
                        url: baseUrl + uarUrl + "/" + item,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    };
                    return $http(req);
                }
            };
        }]);
})(angular);