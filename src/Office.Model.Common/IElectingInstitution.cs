﻿using System;

namespace Office.Model.Common
{
    public interface IElectingInstitution
    {
        #region Properties

        string Abbreviation { get; set; }
        string ID { get; set; }
        string Name { get; set; }

        #endregion Properties
    }
}