﻿using System.Linq;
using System.Threading.Tasks;

namespace Office.Repository.Common
{
    public interface IRepository
    {
        #region Methods

        IUnitOfWork CreateUnitOfWork();

        #endregion Methods

        #region Methods

        Task<int> AddAsync<T>(T entity) where T : class;

        Task<int> DeleteAsync<T>(T entity) where T : class;

        Task<int> DeleteAsync<T>(string ID) where T : class;

        Task<int> InsertAsync<T>(T entity) where T : class;

        Task<T> SingleAsync<T>(string ID) where T : class;

        Task<int> UpdateAsync<T>(T entity) where T : class;

        // Task<IQueryable<T>>?
        IQueryable<T> WhereAsync<T>() where T : class;

        #endregion Methods

        // useranswer, userrole
    }
}