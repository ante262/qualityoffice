(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddForeignStudentController', ['$scope', 'foreignStudentService', 'notifications', '$window', '$state',
        function ($scope, foreignStudentService, notifications, $window, $state) {
            var vm = $scope.vm = {};
            vm.foreignStudent = {};

            vm.post = function (item) {
                foreignStudentService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log('error', data);
                    });
            };
        }]);
})(angular);