(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('VisitorProfileController', ['$scope', 'visitorProfileService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, visitorProfileService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.visitorProfiles = [];
            vm.visitorProfile = {};
            vm.selectedVisitorProfile = null;
            vm.loading = true;

            vm.addVisitorProfile = function () {
                ngDialog.open({
                    template: 'app/visitor-profile/add-visitor-profile.html',
                    controller: 'AddVisitorProfileController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editVisitorProfile = function (data) {
                ngDialog.open({
                    template: 'app/visitor-profile/edit-visitor-profile.html',
                    controller: 'EditVisitorProfileController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.showVisitorProfile = function (data) {
                ngDialog.open({
                    template: 'app/visitor-profile/show-visitor-profile.html',
                    controller: 'ShowVisitorProfileController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            };

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return visitorProfileService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('visitorProfile', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get visitorProfile:' + data.message);
                    $state.go('visitorProfile', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.visitorProfiles = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedVisitorProfile = item;
                visitorProfileService.get(vm.selectedVisitorProfile)
                    .success(function (data) {
                        vm.visitorProfile = data;
                    }).error(function (data) {
                        console.log("Unable to get this visitorProfile:" + data.message);
                    });
            };

            vm.delete = function (data) {
                vm.visitorProfile = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Profil korisnika '" + vm.visitorProfile.Name + " " + vm.visitorProfile.Surname + "' će biti obrisan!")) {
                    visitorProfileService.delete(vm.visitorProfile.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);