(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddVisitorProfileController', ['$scope', 'visitorProfileService', 'foreignInstitutionService', 'lookupTablesService', 'notifications', '$window', '$state', 'ngDialog',
        function ($scope, visitorProfileService, foreignInstitutionService, lookupTablesService, notifications, $window, $state, ngDialog) {
            var vm = $scope.vm = {};
            vm.visitorProfile = {};
            vm.lookups = [];
            vm.foreignInstitutions = [];
            vm.check = {
                title: false,
                institution: false
            };

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchForeignInstitutions = function () {
                return foreignInstitutionService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.foreignInstitutions = data;
                    });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('UserTitles')
                    .then(function (data) {
                        vm.lookups = data;
                    },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchForeignInstitutions();
                vm.fetchLookups();
            }
            vm.fetch();

            vm.post = function (item) {
                if (item.UserTitle) {
                    lookupTablesService.clearCache();
                };

                visitorProfileService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log('error', data);
                    });
            };
        }]);
})(angular);