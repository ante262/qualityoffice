using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class visitorvisittype
    {
        public string ID { get; set; }
        public string VisitorVisitId { get; set; }
        public string VisitTypeId { get; set; }
        public virtual visitorvisit visitorvisit { get; set; }
        public virtual visittype visittype { get; set; }
    }
}
