﻿using System;

namespace Office.Model.Common
{
    public interface IUserProfile
    {
        #region Properties

        string AcademicRankId { get; set; }

        // Added to return Chair table with UserProfile table
        IChair Chair { get; set; }

        string ChairId { get; set; }
        string Comment { get; set; }
        string Email { get; set; }
        Nullable<sbyte> Floor { get; set; }
        string ID { get; set; }
        string Name { get; set; }
        string PicturePath { get; set; }

        // Added to return ResearchGroup table with UserProfile table
        IResearchGroup ResearchGroup { get; set; }

        string ResearchGroupId { get; set; }
        string Room { get; set; }
        string Surname { get; set; }
        string TelephoneNumber { get; set; }
        string TitleId { get; set; }
        string URL { get; set; }

        // Added to return UserAcademicRank table with UserProfile table
        IUserAcademicRank UserAcademicRank { get; set; }

        // Added to return UserTitle table with UserProfile table
        IUserTitle UserTitle { get; set; }

        #endregion Properties
    }
}