﻿using System;
using System.Collections.Generic;

namespace Office.Model.Common
{
    public interface IOurProfessorVisit
    {
        #region Properties

        Nullable<short> AcademicYear { get; set; }
        string Comment { get; set; }
        string ConferenceTitle { get; set; }
        IDepartment Department { get; set; }
        string DepartmentId { get; set; }
        IDuration Duration { get; set; }
        string DurationId { get; set; }
        IForeignInstitution ForeignInstitution { get; set; }
        string ForeignInstitutionId { get; set; }
        IFunding FundingOfStay { get; set; }
        string FundingOfStayId { get; set; }
        string ID { get; set; }
        IList<IProfessorVisitType> ProfessorVisitTypes { get; set; }
        Nullable<System.DateTime> StartDate { get; set; }
        IFunding TravelFunding { get; set; }
        string TravelFundingId { get; set; }
        string URL { get; set; }
        IUserProfile UserProfile { get; set; }
        string UserProfileId { get; set; }
        string VisitResult { get; set; }

        #endregion Properties
    }
}