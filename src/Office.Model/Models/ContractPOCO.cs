﻿using Office.Model.Common;
using System;

namespace Office.Model
{
    public partial class ContractPOCO : IContract
    {
        #region Properties

        public string Comment { get; set; }
        public IContractType ContractType { get; set; }
        public string ContractTypeId { get; set; }
        public string Description { get; set; }
        public IDuration Duration { get; set; }
        public string DurationId { get; set; }
        public IFunding Funding { get; set; }
        public string FundingTypeId { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public string URL { get; set; }

        #endregion Properties
    }
}