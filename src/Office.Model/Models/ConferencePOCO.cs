﻿using Office.Model.Common;
using System;
using System.Collections.Generic;

namespace Office.Model
{
    public partial class ConferencePOCO : IConference
    {
        #region Properties

        public string Comment { get; set; }

        public IList<IConferenceInstitution> ConferenceInstitutions { get; set; }

        // Added to return ConferenceType table with Conference table
        public IConferenceType ConferenceType { get; set; }

        public string ConferenceTypeId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string EnglishName { get; set; }
        public string ID { get; set; }
        public string Locality { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public string Profession { get; set; }
        public string URL { get; set; }

        #endregion Properties
    }
}