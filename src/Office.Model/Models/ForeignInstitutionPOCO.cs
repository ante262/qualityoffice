﻿using Office.Model.Common;

using System;

namespace Office.Model
{
    public partial class ForeignInstitutionPOCO : IForeignInstitution
    {
        #region Properties

        public string City { get; set; }
        public IContact Contact { get; set; }
        public string ContactId { get; set; }
        public IContract Contract { get; set; }
        public string ContractId { get; set; }
        public string Description { get; set; }
        public bool Foreign { get; set; }
        public string ID { get; set; }
        public string InstitutionName { get; set; }
        public bool IsContract { get; set; }
        public string State { get; set; }
        public string URL { get; set; }
        public IUserProfile UserProfile { get; set; }
        public string UserProfileId { get; set; }

        #endregion Properties
    }
}