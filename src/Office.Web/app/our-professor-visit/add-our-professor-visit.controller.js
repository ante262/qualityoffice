(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddOurProfessorVisitController', ['$scope', 'ourProfessorVisitService', 'userProfileService', 'foreignInstitutionService', 'lookupTablesService', 'notifications', '$window', '$state', 'ngDialog',
        function ($scope, ourProfessorVisitService, userProfileService, foreignInstitutionService, lookupTablesService, notifications, $window, $state, ngDialog) {
            var vm = $scope.vm = {};
            vm.ourProfessorVisit = {};
            vm.lookups = [];
            vm.userProfiles = [];
            vm.foreignInstitutions = [];
            vm.ourProfessorVisit.ProfessorVisitTypes = [];
            vm.check = {
                user: false,
                department: false,
                type: false,
                institution: false,
                duration: false,
                tfunding: false,
                sfunding: false
            };

            vm.addUserProfile = function () {
                ngDialog.open({
                    template: 'app/user-profile/add-user-profile.html',
                    controller: 'AddUserProfileController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchUsers = function () {
                return userProfileService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.userProfiles = data;
                    });
            };

            vm.fetchForeignInstitutions = function () {
                return foreignInstitutionService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.foreignInstitutions = data;
                    });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('Departments, Durations, Fundings, VisitTypes')
                .then(function (data) {
                    vm.lookups = data;
                },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchUsers();
                vm.fetchForeignInstitutions();
                vm.fetchLookups();
            }
            vm.fetch();

            vm.post = function (item) {
                if (item.Department || item.Duration || item.TravelFunding || item.FundingOfStay ||
                    item.VisitTypes || item.NewVisitTypes) {
                    lookupTablesService.clearCache();
                };

                if (vm.ourProfessorVisit.NewVisitTypes) {
                    var res = vm.ourProfessorVisit.NewVisitTypes.split(",");
                    for (var ind = 0; ind < res.length; ind++) {
                        vm.ourProfessorVisit.ProfessorVisitTypes.push({ ID: res[ind] });
                    };
                    vm.ourProfessorVisit.NewVisitTypes = null;
                };

                ourProfessorVisitService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log('error', data);
                    });
            };
        }]);
})(angular);