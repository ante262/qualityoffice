(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowForeignStudentController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.foreignStudent = $scope.ngDialogData;
        }]);
})(angular);