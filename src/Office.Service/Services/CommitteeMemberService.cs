﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Office.Service
{
    public class CommitteeMemberService : ICommitteeMemberService
    {
        #region Constructors

        public CommitteeMemberService(IRepository repository, ILookupTablesService lookupTablesService)
        {
            Repository = repository;
            LookupTablesService = lookupTablesService;
        }

        #endregion Constructors

        #region Properties

        protected ILookupTablesService LookupTablesService { get; private set; }
        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(ICommitteeMember entity)
        {
            return Repository.DeleteAsync<committeemember>(Mapper.Map<committeemember>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<committeemember>(ID);
        }

        public async Task<List<ICommitteeMember>> GetAsync()
        {
            // include ConferenceType table into Conference table
            var result = await Repository.WhereAsync<committeemember>()
                          .Include(c => c.committee)
                          .Include(cr => cr.committeememberrole)
                          .Include(u => u.userprofile)
                          .Include(ei => ei.electinginstitution)
                          .ToListAsync<committeemember>();

            return Mapper.Map<List<ICommitteeMember>>(result);
        }

        public async Task<ICommitteeMember> GetAsync(string ID)
        {
            return Mapper.Map<ICommitteeMember>(await Repository.SingleAsync<committeemember>(ID));
        }

        public Task<int> InsertAsync(ICommitteeMember entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            var counter = 0;
            if (entity.Committee != null && entity.Committee.ID == null)
            {
                entity.Committee.ID = Guid.NewGuid().ToString("N");
                entity.CommitteeId = entity.Committee.ID;
                counter++;
            }
            if (entity.CommitteeMemberRole != null && entity.CommitteeMemberRole.ID == null)
            {
                entity.CommitteeMemberRole.ID = Guid.NewGuid().ToString("N");
                entity.CommitteeMemberRoleId = entity.CommitteeMemberRole.ID;
                counter++;
            }
            if (entity.ElectingInstitution != null && entity.ElectingInstitution.ID == null)
            {
                entity.ElectingInstitution.ID = Guid.NewGuid().ToString("N");
                entity.ElectingInstitutionId = entity.ElectingInstitution.ID;
                counter++;
            }
            if (counter > 0)
            {
                LookupTablesService.ClearCache();
            }

            return Repository.InsertAsync<committeemember>(Mapper.Map<committeemember>(entity));
        }

        public async Task<int> UpdateAsync(ICommitteeMember entity)
        {
            var lookupItems = await LookupTablesService.GetAsync();

            var counter = 0;
            var unitOfWork = Repository.CreateUnitOfWork();
            if (entity.Committee != null
                && !lookupItems.Committees.Select(c => c.Name).Contains(entity.Committee.Name))
            {
                entity.Committee.ID = Guid.NewGuid().ToString("N");
                entity.CommitteeId = entity.Committee.ID;
                var map = Mapper.Map<committee>(entity.Committee);
                await unitOfWork.AddAsync<committee>(map);
                counter++;
            }
            if (entity.Committee != null
                && entity.Committee.ID == entity.CommitteeId
                && lookupItems.Committees
                .Select(c => c.Name.ToLower())
                .Contains(entity.Committee.Name.ToLower()))
            {
                entity.CommitteeId = lookupItems.Committees
                    .Where(c => c.Name.ToLower() == entity.Committee.Name.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.CommitteeMemberRole != null
                && !lookupItems.CommitteeMemberRoles
                .Select(c => c.Name.ToLower())
                .Contains(entity.CommitteeMemberRole.Name.ToLower()))
            {
                entity.CommitteeMemberRole.ID = Guid.NewGuid().ToString("N");
                entity.CommitteeMemberRoleId = entity.CommitteeMemberRole.ID;
                var map = Mapper.Map<committeememberrole>(entity.CommitteeMemberRole);
                await unitOfWork.AddAsync<committeememberrole>(map);
                counter++;
            }
            if (entity.CommitteeMemberRole != null
                && entity.CommitteeMemberRole.ID == entity.CommitteeMemberRoleId
                && lookupItems.CommitteeMemberRoles
                .Select(c => c.Name.ToLower())
                .Contains(entity.CommitteeMemberRole.Name.ToLower()))
            {
                entity.CommitteeMemberRoleId = lookupItems.CommitteeMemberRoles
                    .Where(c => c.Name.ToLower() == entity.CommitteeMemberRole.Name.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.ElectingInstitution != null
                && !lookupItems.ElectingInstitutions.Select(c => c.Name).Contains(entity.ElectingInstitution.Name))
            {
                entity.ElectingInstitution.ID = Guid.NewGuid().ToString("N");
                entity.ElectingInstitutionId = entity.ElectingInstitution.ID;
                var map = Mapper.Map<electinginstitution>(entity.ElectingInstitution);
                await unitOfWork.AddAsync<electinginstitution>(map);
                counter++;
            }
            if (entity.ElectingInstitution != null
                && entity.ElectingInstitution.ID == entity.ElectingInstitutionId
                && lookupItems.ElectingInstitutions
                .Select(c => c.Name.ToLower())
                .Contains(entity.ElectingInstitution.Name.ToLower()))
            {
                entity.ElectingInstitutionId = lookupItems.ElectingInstitutions
                    .Where(c => c.Name.ToLower() == entity.ElectingInstitution.Name.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }

            entity.Committee = null;
            entity.CommitteeMemberRole = null;
            entity.ElectingInstitution = null;
            await unitOfWork.UpdateAsync<committeemember>(Mapper.Map<committeemember>(entity));
            if (counter > 0)
            {
                await LookupTablesService.ClearCache();
            }
            return await unitOfWork.CommitAsync();
        }

        #endregion Methods
    }
}