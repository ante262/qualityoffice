using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class my_aspnet_sitemap
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Roles { get; set; }
        public Nullable<int> ParentId { get; set; }
    }
}
