using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class visitorprofileMap : EntityTypeConfiguration<visitorprofile>
    {
        public visitorprofileMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Surname)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.TitleId)
                .HasMaxLength(128);

            this.Property(t => t.Email)
                .HasMaxLength(128);

            this.Property(t => t.ForeignInstitutionId)
                .HasMaxLength(128);

            this.Property(t => t.URL)
                .HasMaxLength(250);

            this.Property(t => t.PicturePath)
                .HasMaxLength(500);

            this.Property(t => t.TelephoneNumber)
                .HasMaxLength(128);

            this.Property(t => t.Comment)
                .HasMaxLength(1073741823);

            // Table & Column Mappings
            this.ToTable("visitorprofile", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Surname).HasColumnName("Surname");
            this.Property(t => t.TitleId).HasColumnName("TitleId");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.ForeignInstitutionId).HasColumnName("ForeignInstitutionId");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.PicturePath).HasColumnName("PicturePath");
            this.Property(t => t.TelephoneNumber).HasColumnName("TelephoneNumber");
            this.Property(t => t.Comment).HasColumnName("Comment");

            // Relationships
            this.HasOptional(t => t.foreigninstitution)
                .WithMany(t => t.visitorprofiles)
                .HasForeignKey(d => d.ForeignInstitutionId);
            this.HasOptional(t => t.usertitle)
                .WithMany(t => t.visitorprofiles)
                .HasForeignKey(d => d.TitleId);

        }
    }
}
