﻿using System;

namespace Office.Model.Common
{
    public interface INonPublicationLecture
    {
        #region Properties

        string City { get; set; }
        string Comment { get; set; }
        IConferenceType ConferenceType { get; set; }
        string ConferenceTypeId { get; set; }
        Nullable<System.DateTime> Date { get; set; }
        string Description { get; set; }
        string EnglishTitle { get; set; }
        IForeignInstitution ForeignInstitution { get; set; }
        string ForeignInstitutionId { get; set; }
        string ID { get; set; }
        string Location { get; set; }
        string Name { get; set; }
        string Period { get; set; }
        string Profession { get; set; }
        string State { get; set; }
        string Summary { get; set; }
        string Surname { get; set; }
        string Title { get; set; }
        string URL { get; set; }

        #endregion Properties
    }
}