﻿using AutoMapper;
using Office.Model;
using Office.Model.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/Contract")]
    public class ContractController : ApiController
    {
        #region Constructors

        public ContractController(IContractService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private IContractService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/Contract/
        [HttpDelete]
        [Route("{ID}")]
        public async Task<IHttpActionResult> Delete(string ID)
        {
            try
            {
                var result = await Service.DeleteAsync(ID);
                if (result == 1) return Ok("Deleted");
                else return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        // GET: api/Contract
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<ContractModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/Contract/5
        [HttpGet]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Get(string ID)
        {
            var result = await Service.GetAsync(ID);

            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<ContractModel>(result));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        // POST: api/Contract
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post(ContractModel entity)
        {
            try
            {
                var result = await Service.InsertAsync(Mapper.Map<IContract>(entity));

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Add operation error.");

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/Contract/5
        [HttpPut]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Put(string ID, ContractModel entity)
        {
            try
            {
                if (ID != entity.ID)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "IDs do not match.");
                }

                var result = await Service.UpdateAsync(Mapper.Map<IContract>(entity));
                if (result > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "Failed.");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public partial class ContractModel
        {
            #region Properties

            public string Comment { get; set; }
            public ContractTypePOCO ContractType { get; set; }
            public string ContractTypeId { get; set; }
            public string Description { get; set; }
            public DurationPOCO Duration { get; set; }
            public string DurationId { get; set; }
            public FundingPOCO Funding { get; set; }
            public string FundingTypeId { get; set; }
            public string ID { get; set; }
            public string Name { get; set; }
            public Nullable<System.DateTime> StartDate { get; set; }
            public string URL { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}