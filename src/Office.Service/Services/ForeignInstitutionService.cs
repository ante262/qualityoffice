﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Office.Service
{
    public class ForeignInstitutionService : IForeignInstitutionService
    {
        #region Constructors

        public ForeignInstitutionService(IRepository repository, ILookupTablesService lookupTablesService)
        {
            Repository = repository;
            LookupTablesService = lookupTablesService;
        }

        #endregion Constructors

        #region Properties

        protected ILookupTablesService LookupTablesService { get; private set; }
        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IForeignInstitution entity)
        {
            return Repository.DeleteAsync<foreigninstitution>(Mapper.Map<foreigninstitution>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<foreigninstitution>(ID);
        }

        public async Task<List<IForeignInstitution>> GetAsync()
        {
            // include Contact, Contract and UserProfile tables into ForeignInstitution table
            var result = await Repository.WhereAsync<foreigninstitution>()
                          .Include(c => c.contact)
                          .Include(c => c.contract)
                          .Include(u => u.userprofile)
                          .ToListAsync<foreigninstitution>();

            return Mapper.Map<List<IForeignInstitution>>(result);
        }

        public async Task<IForeignInstitution> GetAsync(string ID)
        {
            return Mapper.Map<IForeignInstitution>(await Repository.SingleAsync<foreigninstitution>(ID));
        }

        public Task<int> InsertAsync(IForeignInstitution entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            var counter = 0;
            if (entity.Contact != null && entity.Contact.ID == null)
            {
                entity.Contact.ID = Guid.NewGuid().ToString("N");
                entity.ContactId = entity.Contact.ID;
                counter++;
            }
            if (counter > 0)
            {
                LookupTablesService.ClearCache();
            }

            return Repository.InsertAsync<foreigninstitution>(Mapper.Map<foreigninstitution>(entity));
        }

        public Task<int> UpdateAsync(IForeignInstitution entity)
        {
            return Repository.UpdateAsync<foreigninstitution>(Mapper.Map<foreigninstitution>(entity));
        }

        #endregion Methods
    }
}