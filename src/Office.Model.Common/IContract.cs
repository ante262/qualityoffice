﻿using System;

namespace Office.Model.Common
{
    public interface IContract
    {
        #region Properties

        string Comment { get; set; }
        IContractType ContractType { get; set; }
        string ContractTypeId { get; set; }
        string Description { get; set; }
        IDuration Duration { get; set; }
        string DurationId { get; set; }
        IFunding Funding { get; set; }
        string FundingTypeId { get; set; }
        string ID { get; set; }
        string Name { get; set; }
        Nullable<System.DateTime> StartDate { get; set; }
        string URL { get; set; }

        #endregion Properties
    }
}