(function (angular) {
    'use strict';
    angular.module('officeApp').controller('EditForeignStudentController', ['$scope', 'foreignStudentService', 'notifications', '$window', '$state', '$stateParams',
        function ($scope, foreignStudentService, notifications, $window, $state, $stateParams) {
            var vm = $scope.vm = {};
            vm.foreignStudent = $scope.ngDialogData;

            vm.put = function (item) {
                foreignStudentService.put(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                        $state.go('foreignStudent', { 'notification': notifications.entity.editError });
                    });
            };
        }]);
})(angular);