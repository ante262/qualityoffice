﻿(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('EditRoleController', ['$scope', 'roleService', 'notifications', '$window', '$state', '$stateParams',
        function ($scope, roleService, notifications, $window, $state, $stateParams) {
            var vm = $scope.vm = {};
            vm.role = $scope.ngDialogData;

            vm.put = function (item) {
                vm.role.Name = item.Name;
                vm.role.Description = item.Description;
                vm.role.Abbreviation = item.Abbreviation;
                vm.role.ID = item.ID;

                roleService.put(vm.role).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(data);
                        $state.go('role', { 'notification': notifications.entity.editError });
                    });
            };
        }]);
})(angular);