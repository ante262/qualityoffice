﻿using System;

namespace Office.Model.Common
{
    public interface IVisitorProfile
    {
        #region Properties

        string Comment { get; set; }
        string Email { get; set; }
        IForeignInstitution ForeignInstitution { get; set; }
        string ForeignInstitutionId { get; set; }
        string ID { get; set; }
        string Name { get; set; }
        string PicturePath { get; set; }
        string Surname { get; set; }
        string TelephoneNumber { get; set; }
        string TitleId { get; set; }
        string URL { get; set; }
        IUserTitle UserTitle { get; set; }

        #endregion Properties
    }
}