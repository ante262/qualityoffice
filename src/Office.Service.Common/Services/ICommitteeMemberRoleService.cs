﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IcommitteeMemberRoleService
    {
        #region Methods

        Task<int> DeleteAsync(ICommitteeMemberRole entity);

        Task<int> DeleteAsync(string ID);

        Task<List<ICommitteeMemberRole>> GetAsync();

        Task<ICommitteeMemberRole> GetAsync(string ID);

        Task<int> InsertAsync(ICommitteeMemberRole entity);

        Task<int> UpdateAsync(ICommitteeMemberRole entity);

        #endregion Methods
    }
}