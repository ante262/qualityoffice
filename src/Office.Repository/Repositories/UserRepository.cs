﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Office.Repository
{
    public class UserRepository : IUserRepository
    {
        #region Constructors

        public UserRepository(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public virtual Task<int> DeleteAsync(IUser entity)
        {
            return Repository.DeleteAsync<user>(Mapper.Map<user>(entity));
        }

        public virtual Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<user>(ID);
        }

        public virtual async Task<List<IUser>> GetAsync()
        {
            return Mapper.Map<List<IUser>>(
                await Repository.WhereAsync<user>()
                        .ToListAsync<user>()
                );
        }

        public virtual async Task<IUser> GetAsync(string ID)
        {
            return Mapper.Map<IUser>(await Repository.SingleAsync<user>(ID));
        }

        public virtual Task<int> InsertAsync(IUser entity)
        {
            return Repository.InsertAsync<user>(Mapper.Map<user>(entity));
        }

        public virtual Task<int> UpdateAsync(IUser entity)
        {
            return Repository.UpdateAsync<user>(Mapper.Map<user>(entity));
        }

        #endregion Methods
    }
}