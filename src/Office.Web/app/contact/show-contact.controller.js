(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowContactController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.contact = $scope.ngDialogData;
        }]);
})(angular);