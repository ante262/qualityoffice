using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class my_aspnet_personalizationperuser
    {
        public int id { get; set; }
        public int applicationId { get; set; }
        public string pathId { get; set; }
        public Nullable<int> userId { get; set; }
        public byte[] pageSettings { get; set; }
        public System.DateTime lastUpdatedDate { get; set; }
    }
}
