using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class visitorvisit
    {
        public visitorvisit()
        {
            this.visitorvisittypes = new List<visitorvisittype>();
        }

        public string ID { get; set; }
        public string VisitorProfileId { get; set; }
        public string DepartmentId { get; set; }
        public Nullable<short> AcademicYear { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public string DurationId { get; set; }
        public string UserProfileId { get; set; }
        public string TravelFundingId { get; set; }
        public string FundingOfStayId { get; set; }
        public string VisitResult { get; set; }
        public string Comment { get; set; }
        public virtual department department { get; set; }
        public virtual duration duration { get; set; }
        public virtual funding funding { get; set; }
        public virtual funding funding1 { get; set; }
        public virtual userprofile userprofile { get; set; }
        public virtual visitorprofile visitorprofile { get; set; }
        public virtual ICollection<visitorvisittype> visitorvisittypes { get; set; }
    }
}
