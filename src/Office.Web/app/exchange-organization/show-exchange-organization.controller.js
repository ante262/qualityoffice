(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowExchangeOrganizationController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.exchangeOrganization = $scope.ngDialogData;
        }]);
})(angular);