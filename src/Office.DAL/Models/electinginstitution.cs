using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class electinginstitution
    {
        public electinginstitution()
        {
            this.committeemembers = new List<committeemember>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<committeemember> committeemembers { get; set; }
    }
}
