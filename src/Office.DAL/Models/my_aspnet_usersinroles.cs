using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class my_aspnet_usersinroles
    {
        public int userId { get; set; }
        public int roleId { get; set; }
    }
}
