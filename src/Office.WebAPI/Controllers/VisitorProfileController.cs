﻿using AutoMapper;
using Office.Model;
using Office.Model.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/VisitorProfile")]
    public class VisitorProfileController : ApiController
    {
        #region Constructors

        public VisitorProfileController(IVisitorProfileService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private IVisitorProfileService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/VisitorProfile/
        [HttpDelete]
        [Route("{ID}")]
        public async Task<IHttpActionResult> Delete(string ID)
        {
            try
            {
                var result = await Service.DeleteAsync(ID);
                if (result == 1) return Ok("Deleted");
                else return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        // GET: api/VisitorProfile
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<VisitorProfileModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/VisitorProfile/5
        [HttpGet]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Get(string ID)
        {
            var result = await Service.GetAsync(ID);

            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<VisitorProfileModel>(result));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        // POST: api/VisitorProfile
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post(VisitorProfileModel entity)
        {
            try
            {
                var result = await Service.InsertAsync(Mapper.Map<IVisitorProfile>(entity));

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Add operation error.");

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/VisitorProfile/5
        [HttpPut]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Put(string ID, VisitorProfileModel entity)
        {
            try
            {
                if (ID != entity.ID)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "IDs do not match.");
                }

                var result = await Service.UpdateAsync(Mapper.Map<IVisitorProfile>(entity));
                if (result > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "Failed.");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public partial class ForeignInstitutionModel
        {
            #region Properties

            public string InstitutionName { get; set; }

            #endregion Properties
        }

        public partial class VisitorProfileModel
        {
            #region Properties

            public string Comment { get; set; }
            public string Email { get; set; }
            public ForeignInstitutionModel ForeignInstitution { get; set; }
            public string ForeignInstitutionId { get; set; }
            public string ID { get; set; }
            public string Name { get; set; }
            public string PicturePath { get; set; }
            public string Surname { get; set; }
            public string TelephoneNumber { get; set; }
            public string TitleId { get; set; }
            public string URL { get; set; }
            public UserTitlePOCO UserTitle { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}