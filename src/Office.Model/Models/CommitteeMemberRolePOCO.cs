﻿using Office.Model.Common;

using System;

namespace Office.Model
{
    public partial class CommitteeMemberRolePOCO : ICommitteeMemberRole
    {
        #region Properties

        public string Abbreviation { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }

        #endregion Properties
    }
}