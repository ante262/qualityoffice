﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface ICommitteeService
    {
        #region Methods

        Task<int> DeleteAsync(ICommittee entity);

        Task<int> DeleteAsync(string ID);

        Task<List<ICommittee>> GetAsync();

        Task<ICommittee> GetAsync(string ID);

        Task<int> InsertAsync(ICommittee entity);

        Task<int> UpdateAsync(ICommittee entity);

        #endregion Methods
    }
}