﻿using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service
{
    public class RoleService : IRoleService
    {
        #region Constructors

        public RoleService(IRoleRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRoleRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IRole entity)
        {
            return Repository.DeleteAsync(entity);
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync(ID);
        }

        public Task<List<IRole>> GetAsync()
        {
            return Repository.GetAsync();
        }

        public Task<IRole> GetAsync(string ID)
        {
            return Repository.GetAsync(ID);
        }

        public Task<int> InsertAsync(IRole entity)
        {
            return Repository.InsertAsync(entity);
        }

        public Task<int> UpdateAsync(IRole entity)
        {
            return Repository.UpdateAsync(entity);
        }

        #endregion Methods
    }
}