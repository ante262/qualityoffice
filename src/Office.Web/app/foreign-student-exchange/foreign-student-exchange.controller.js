(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ForeignStudentExchangeController', ['$scope', 'foreignStudentExchangeService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, foreignStudentExchangeService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.foreignStudentExchanges = [];
            vm.foreignStudentExchange = {};
            vm.selectedForeignStudentExchange = null;
            vm.loading = true;

            vm.addForeignStudentExchange = function () {
                ngDialog.open({
                    template: 'app/foreign-student-exchange/add-foreign-student-exchange.html',
                    controller: 'AddForeignStudentExchangeController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editForeignStudentExchange = function (data) {
                ngDialog.open({
                    template: 'app/foreign-student-exchange/edit-foreign-student-exchange.html',
                    controller: 'EditForeignStudentExchangeController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.showForeignStudentExchange = function (data) {
                ngDialog.open({
                    template: 'app/foreign-student-exchange/show-foreign-student-exchange.html',
                    controller: 'ShowForeignStudentExchangeController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
            };

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return foreignStudentExchangeService.fetch()
                .error(function (data) {
                    if (data === null) {
                        $state.go('foreignStudentExchange', { 'notification': notifications.entity.serverError });
                    };
                    $state.go('foreignStudentExchange', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.foreignStudentExchanges = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedForeignStudentExchange = item;
                foreignStudentExchangeService.get(vm.selectedForeignStudentExchange)
                .success(function (data) {
                    vm.foreignStudentExchange = data;
                })
                .error(function (data) {
                    console.log("Unable to get this foreignStudentExchange:" + data.message);
                });
            };

            vm.delete = function (data) {
                vm.foreignStudentExchange = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Boravak studenta '" + vm.foreignStudentExchange.Name + "' će biti obrisan!")) {
                    foreignStudentExchangeService.delete(vm.foreignStudentExchange.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);