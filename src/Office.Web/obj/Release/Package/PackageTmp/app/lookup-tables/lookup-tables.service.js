﻿(function (angular) {
    'use strict';
    angular.module('officeApp').service('lookupTablesService', ['$http', 'baseUrl', '$q',
        function ($http, baseUrl, $q) {
            var lookupUrl = 'LookupTables';
            var lookups = {};
            var cache = true;
            
            return {
                clearCache: function() {
                    cache = false;
                },
                fetch: function (query) {
                    var req = {
                        method: 'GET',
                        url: baseUrl + lookupUrl,
                        params: { embed: query },
                        cache: cache,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    };

                    //Creating a custom promise so that the controller always knows how to handle result(as a promise)
                    var deferred = $q.defer();

                    if (lookups.hasOwnProperty(query) && req.cache) {
                        deferred.resolve(lookups[query]);
                        deferred.reject({
                            message: "This is never going to happen."
                        });
                        //Returning promise instead of raw data for reasons above
                        //console.log("Imam ovaj query u cacheu i vraćam: ", lookups[query]);
                        return deferred.promise;
                    }
                    else if (lookups.hasOwnProperty('All') && req.cache) {
                        var lookupTypes = query.split(", ");
                        var result = {};

                        for (var i = 0; i < lookupTypes.length; i++) {
                            result[lookupTypes[i]] = lookups.All[lookupTypes[i]];
                        }

                        lookups[query] = result;
                        deferred.resolve(result);
                        //console.log("Nemam ovaj query u cache ali imam sve lookupe pa iz toga izvlačim potrebne podatke: ", result);
                        return deferred.promise;
                    }
                    else {
                        //Returned promise instead of data because controller does not know how to 'await' otherwise
                        return $http(req).then(
                            function (result) {
                                //This part removes the null object properties and returns only what was asked
                                if (query == 'All') {
                                    lookups[query] = result.data;
                                }
                                else {
                                    var lookupTypes = query.split(", ");
                                    var tempObj = {};

                                    for (var i = 0; i < lookupTypes.length; i++) {
                                        tempObj[lookupTypes[i]] = result.data[lookupTypes[i]];
                                    }
                                    lookups[query] = tempObj;
                                }
                                //Part end
                                //console.log("Vraćam sa back-enda ove podatke: ", result.data);
                                cache = true;

                                return lookups[query];
                            },
                            function (error) {
                                return error;
                            });
                    };
                }
            }
        }]);
})(angular);