using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class my_aspnet_personalizationperuserMap : EntityTypeConfiguration<my_aspnet_personalizationperuser>
    {
        public my_aspnet_personalizationperuserMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.pathId)
                .HasMaxLength(36);

            this.Property(t => t.pageSettings)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("my_aspnet_personalizationperuser", "officeapp");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.applicationId).HasColumnName("applicationId");
            this.Property(t => t.pathId).HasColumnName("pathId");
            this.Property(t => t.userId).HasColumnName("userId");
            this.Property(t => t.pageSettings).HasColumnName("pageSettings");
            this.Property(t => t.lastUpdatedDate).HasColumnName("lastUpdatedDate");
        }
    }
}
