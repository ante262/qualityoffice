﻿using AutoMapper;
using Office.Model;
using Office.Model.Common;
using Office.Model.Mapping;
using Office.WebAPI.Controllers;

namespace Office.WebAPI.App_Start
{
    public static class AutoMapperConfig
    {
        #region Methods

        public static void Initialize()
        {
            // Model
            AutoMapperMaps.Initialize();

            // RoleController
            Mapper.CreateMap<RoleController.RoleModel, RolePOCO>().ReverseMap();
            Mapper.CreateMap<RoleController.RoleModel, IRole>().ReverseMap();

            // UserContorller
            Mapper.CreateMap<UserController.UserModel, UserPOCO>().ReverseMap();
            Mapper.CreateMap<UserController.UserModel, IUser>().ReverseMap();

            // ChairController
            Mapper.CreateMap<ChairController.ChairModel, ChairPOCO>().ReverseMap();
            Mapper.CreateMap<ChairController.ChairModel, IChair>().ReverseMap();

            // CourseContorller
            Mapper.CreateMap<CourseController.CourseModel, CoursePOCO>().ReverseMap();
            Mapper.CreateMap<CourseController.CourseModel, ICourse>().ReverseMap();

            // ConferenceController
            Mapper.CreateMap<ConferenceController.ConferenceModel, ConferencePOCO>().ReverseMap();
            Mapper.CreateMap<ConferenceController.ConferenceModel, IConference>().ReverseMap();
            Mapper.CreateMap<ConferenceController.ConferenceInstitutionModel, IConferenceInstitution>().ReverseMap();
            Mapper.CreateMap<ConferenceController.ForeignInstitutionModel, IForeignInstitution>().ReverseMap();

            // ConferenceTypeController
            Mapper.CreateMap<ConferenceTypeController.ConferenceTypeModel, ConferenceTypePOCO>().ReverseMap();
            Mapper.CreateMap<ConferenceTypeController.ConferenceTypeModel, IConferenceType>().ReverseMap();

            // UserProfileController
            Mapper.CreateMap<UserProfileController.UserProfileModel, UserProfilePOCO>().ReverseMap();
            Mapper.CreateMap<UserProfileController.UserProfileModel, IUserProfile>().ReverseMap();

            // ForeignStudentExchangeController
            Mapper.CreateMap<ForeignStudentExchangeController.ForeignStudentExchangeModel, ForeignStudentExchangePOCO>().ReverseMap();
            Mapper.CreateMap<ForeignStudentExchangeController.ForeignStudentExchangeModel, IForeignStudentExchange>().ReverseMap();
            Mapper.CreateMap<ForeignStudentExchangeController.CourseModel, ICourse>().ReverseMap();
            Mapper.CreateMap<ForeignStudentExchangeController.DepartmentModel, IDepartment>().ReverseMap();
            Mapper.CreateMap<ForeignStudentExchangeController.ExchangeOrganizationModel, IExchangeOrganization>().ReverseMap();
            Mapper.CreateMap<ForeignStudentExchangeController.ForeignInstitutionModel, IForeignInstitution>().ReverseMap();
            Mapper.CreateMap<ForeignStudentExchangeController.ForeignStudentModel, IForeignStudent>().ReverseMap();
            Mapper.CreateMap<ForeignStudentExchangeController.UserProfileModel, IUserProfile>().ReverseMap();

            // CommitteeMemberController
            Mapper.CreateMap<CommitteeMemberController.CommitteeMemberModel, CommitteeMemberPOCO>().ReverseMap();
            Mapper.CreateMap<CommitteeMemberController.CommitteeMemberModel, ICommitteeMember>().ReverseMap();
            Mapper.CreateMap<CommitteeMemberController.CommitteeModel, ICommittee>().ReverseMap();
            Mapper.CreateMap<CommitteeMemberController.ElectingInstitutionModel, IElectingInstitution>().ReverseMap();
            Mapper.CreateMap<CommitteeMemberController.UserProfileModel, IUserProfile>().ReverseMap();

            // ContractController
            Mapper.CreateMap<ContractController.ContractModel, ContractPOCO>().ReverseMap();
            Mapper.CreateMap<ContractController.ContractModel, IContract>().ReverseMap();

            // ContactController
            Mapper.CreateMap<ContactController.ContactModel, ContactPOCO>().ReverseMap();
            Mapper.CreateMap<ContactController.ContactModel, IContact>().ReverseMap();

            // ExchangeOrganizationController
            Mapper.CreateMap<ExchangeOrganizationController.ExchangeOrganizationModel, ExchangeOrganizationPOCO>().ReverseMap();
            Mapper.CreateMap<ExchangeOrganizationController.ExchangeOrganizationModel, IExchangeOrganization>().ReverseMap();

            // ForeignStudentController
            Mapper.CreateMap<ForeignStudentController.ForeignStudentModel, ForeignStudentPOCO>().ReverseMap();
            Mapper.CreateMap<ForeignStudentController.ForeignStudentModel, IForeignStudent>().ReverseMap();

            // NonPublicationLectureController
            Mapper.CreateMap<NonPublicationLectureController.NonPublicationLectureModel, NonPublicationLecturePOCO>().ReverseMap();
            Mapper.CreateMap<NonPublicationLectureController.NonPublicationLectureModel, INonPublicationLecture>().ReverseMap();
            Mapper.CreateMap<NonPublicationLectureController.ForeignInstitutionModel, IForeignInstitution>().ReverseMap();

            // ForeignInstitutionController
            Mapper.CreateMap<ForeignInstitutionController.ForeignInstitutionModel, ForeignInstitutionPOCO>().ReverseMap();
            Mapper.CreateMap<ForeignInstitutionController.ForeignInstitutionModel, IForeignInstitution>().ReverseMap();
            Mapper.CreateMap<ForeignInstitutionController.UserProfileModel, IUserProfile>().ReverseMap();
            Mapper.CreateMap<ForeignInstitutionController.ContactModel, IContact>().ReverseMap();
            Mapper.CreateMap<ForeignInstitutionController.ContractModel, IContract>().ReverseMap();

            // OurProfessorVisitController
            Mapper.CreateMap<OurProfessorVisitController.OurProfessorVisitModel, OurProfessorVisitPOCO>().ReverseMap();
            Mapper.CreateMap<OurProfessorVisitController.OurProfessorVisitModel, IOurProfessorVisit>().ReverseMap();
            Mapper.CreateMap<OurProfessorVisitController.DepartmentModel, IDepartment>().ReverseMap();
            Mapper.CreateMap<OurProfessorVisitController.ForeignInstitutionModel, IForeignInstitution>().ReverseMap();
            Mapper.CreateMap<OurProfessorVisitController.UserProfileModel, IUserProfile>().ReverseMap();

            // OurStudentExchangeController
            Mapper.CreateMap<OurStudentExchangeController.OurStudentExchangeModel, OurStudentExchangePOCO>().ReverseMap();
            Mapper.CreateMap<OurStudentExchangeController.OurStudentExchangeModel, IOurStudentExchange>().ReverseMap();
            Mapper.CreateMap<OurStudentExchangeController.DepartmentModel, IDepartment>().ReverseMap();
            Mapper.CreateMap<OurStudentExchangeController.ExchangeOrganizationModel, IExchangeOrganization>().ReverseMap();
            Mapper.CreateMap<OurStudentExchangeController.ForeignInstitutionModel, IForeignInstitution>().ReverseMap();

            // VisitorProfileController
            Mapper.CreateMap<VisitorProfileController.VisitorProfileModel, VisitorProfilePOCO>().ReverseMap();
            Mapper.CreateMap<VisitorProfileController.VisitorProfileModel, IVisitorProfile>().ReverseMap();
            Mapper.CreateMap<VisitorProfileController.ForeignInstitutionModel, IForeignInstitution>().ReverseMap();

            // VisitorVisitController
            Mapper.CreateMap<VisitorVisitController.VisitorVisitModel, VisitorVisitPOCO>().ReverseMap();
            Mapper.CreateMap<VisitorVisitController.VisitorVisitModel, IVisitorVisit>().ReverseMap();
            Mapper.CreateMap<VisitorVisitController.DepartmentModel, IDepartment>().ReverseMap();
            Mapper.CreateMap<VisitorVisitController.UserProfileModel, IUserProfile>().ReverseMap();
            Mapper.CreateMap<VisitorVisitController.VisitorProfileModel, IVisitorProfile>().ReverseMap();
            Mapper.CreateMap<VisitorVisitController.ForeignInstitutionModel, IForeignInstitution>().ReverseMap();

            // VisitTypeController
            Mapper.CreateMap<VisitTypeController.VisitTypeModel, VisitTypePOCO>().ReverseMap();
            Mapper.CreateMap<VisitTypeController.VisitTypeModel, IVisitType>().ReverseMap();

            // LookupTablesController
            Mapper.CreateMap<LookupTablesController.LookupTablesModel, ILookupTablesModel>().ReverseMap();
        }

        #endregion Methods
    }
}