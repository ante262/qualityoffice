using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class nonpublicationlectureMap : EntityTypeConfiguration<nonpublicationlecture>
    {
        public nonpublicationlectureMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Name)
                .HasMaxLength(128);

            this.Property(t => t.Surname)
                .HasMaxLength(128);

            this.Property(t => t.Location)
                .HasMaxLength(128);

            this.Property(t => t.City)
                .HasMaxLength(128);

            this.Property(t => t.State)
                .HasMaxLength(128);

            this.Property(t => t.ConferenceTypeId)
                .HasMaxLength(128);

            this.Property(t => t.Period)
                .HasMaxLength(128);

            this.Property(t => t.Title)
                .HasMaxLength(128);

            this.Property(t => t.EnglishTitle)
                .HasMaxLength(128);

            this.Property(t => t.Profession)
                .HasMaxLength(128);

            this.Property(t => t.Summary)
                .HasMaxLength(1073741823);

            this.Property(t => t.Description)
                .HasMaxLength(1073741823);

            this.Property(t => t.URL)
                .HasMaxLength(250);

            this.Property(t => t.Comment)
                .HasMaxLength(1073741823);

            this.Property(t => t.ForeignInstitutionId)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("nonpublicationlecture", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Surname).HasColumnName("Surname");
            this.Property(t => t.Location).HasColumnName("Location");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ConferenceTypeId).HasColumnName("ConferenceTypeId");
            this.Property(t => t.Period).HasColumnName("Period");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.EnglishTitle).HasColumnName("EnglishTitle");
            this.Property(t => t.Profession).HasColumnName("Profession");
            this.Property(t => t.Summary).HasColumnName("Summary");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.ForeignInstitutionId).HasColumnName("ForeignInstitutionId");

            // Relationships
            this.HasOptional(t => t.conferencetype)
                .WithMany(t => t.nonpublicationlectures)
                .HasForeignKey(d => d.ConferenceTypeId);
            this.HasOptional(t => t.foreigninstitution)
                .WithMany(t => t.nonpublicationlectures)
                .HasForeignKey(d => d.ForeignInstitutionId);

        }
    }
}
