using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class foreigninstitution
    {
        public foreigninstitution()
        {
            this.conferenceinstitutions = new List<conferenceinstitution>();
            this.foreignstudentexchanges = new List<foreignstudentexchange>();
            this.nonpublicationlectures = new List<nonpublicationlecture>();
            this.ourprofessorvisits = new List<ourprofessorvisit>();
            this.ourstudentexchanges = new List<ourstudentexchange>();
            this.visitorprofiles = new List<visitorprofile>();
        }

        public string ID { get; set; }
        public string InstitutionName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string URL { get; set; }
        public string Description { get; set; }
        public Nullable<bool> Foreign { get; set; }
        public string ContactId { get; set; }
        public string UserProfileId { get; set; }
        public Nullable<bool> IsContract { get; set; }
        public string ContractId { get; set; }
        public virtual ICollection<conferenceinstitution> conferenceinstitutions { get; set; }
        public virtual contact contact { get; set; }
        public virtual contract contract { get; set; }
        public virtual userprofile userprofile { get; set; }
        public virtual ICollection<foreignstudentexchange> foreignstudentexchanges { get; set; }
        public virtual ICollection<nonpublicationlecture> nonpublicationlectures { get; set; }
        public virtual ICollection<ourprofessorvisit> ourprofessorvisits { get; set; }
        public virtual ICollection<ourstudentexchange> ourstudentexchanges { get; set; }
        public virtual ICollection<visitorprofile> visitorprofiles { get; set; }
    }
}
