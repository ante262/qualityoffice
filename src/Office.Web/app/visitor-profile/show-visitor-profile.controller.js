(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowVisitorProfileController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.visitorProfile = $scope.ngDialogData;
            
        }]);
})(angular);