﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IForeignStudentExchangeService
    {
        #region Methods

        Task<int> DeleteAsync(IForeignStudentExchange entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IForeignStudentExchange>> GetAsync();

        Task<IForeignStudentExchange> GetAsync(string ID);

        Task<int> InsertAsync(IForeignStudentExchange entity);

        Task<int> UpdateAsync(IForeignStudentExchange entity);

        #endregion Methods
    }
}