using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class committeememberMap : EntityTypeConfiguration<committeemember>
    {
        public committeememberMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.UserProfileId)
                .HasMaxLength(128);

            this.Property(t => t.Title)
                .HasMaxLength(128);

            this.Property(t => t.CommitteeId)
                .HasMaxLength(128);

            this.Property(t => t.CommitteeMemberRoleId)
                .HasMaxLength(128);

            this.Property(t => t.PeriodOfElection)
                .HasMaxLength(128);

            this.Property(t => t.ElectingInstitutionId)
                .HasMaxLength(128);

            this.Property(t => t.Comment)
                .HasMaxLength(1073741823);

            // Table & Column Mappings
            this.ToTable("committeemember", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.UserProfileId).HasColumnName("UserProfileId");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.CommitteeId).HasColumnName("CommitteeId");
            this.Property(t => t.CommitteeMemberRoleId).HasColumnName("CommitteeMemberRoleId");
            this.Property(t => t.PeriodOfElection).HasColumnName("PeriodOfElection");
            this.Property(t => t.DateOfElection).HasColumnName("DateOfElection");
            this.Property(t => t.ElectingInstitutionId).HasColumnName("ElectingInstitutionId");
            this.Property(t => t.Comment).HasColumnName("Comment");

            // Relationships
            this.HasOptional(t => t.committee)
                .WithMany(t => t.committeemembers)
                .HasForeignKey(d => d.CommitteeId);
            this.HasOptional(t => t.committeememberrole)
                .WithMany(t => t.committeemembers)
                .HasForeignKey(d => d.CommitteeMemberRoleId);
            this.HasOptional(t => t.electinginstitution)
                .WithMany(t => t.committeemembers)
                .HasForeignKey(d => d.ElectingInstitutionId);
            this.HasOptional(t => t.userprofile)
                .WithMany(t => t.committeemembers)
                .HasForeignKey(d => d.UserProfileId);

        }
    }
}
