﻿using System;

namespace Office.Model.Common
{
    public interface IConferenceInstitution
    {
        #region Properties

        string ConferenceId { get; set; }
        IForeignInstitution ForeignInstitution { get; set; }
        string ForeignInstitutionId { get; set; }
        string ID { get; set; }

        #endregion Properties
    }
}