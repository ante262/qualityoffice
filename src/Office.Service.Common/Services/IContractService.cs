﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IContractService
    {
        #region Methods

        Task<int> DeleteAsync(IContract entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IContract>> GetAsync();

        Task<IContract> GetAsync(string ID);

        Task<int> InsertAsync(IContract entity);

        Task<int> UpdateAsync(IContract entity);

        #endregion Methods
    }
}