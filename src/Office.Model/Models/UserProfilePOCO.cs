﻿using Office.Model.Common;
using System;

namespace Office.Model
{
    public partial class UserProfilePOCO : IUserProfile
    {
        #region Properties

        public string AcademicRankId { get; set; }

        // Added to return Chair table with UserProfile table
        public IChair Chair { get; set; }

        public string ChairId { get; set; }
        public string Comment { get; set; }
        public string Email { get; set; }
        public Nullable<sbyte> Floor { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public string PicturePath { get; set; }

        // Added to return ResearchGroup table with UserProfile table
        public IResearchGroup ResearchGroup { get; set; }

        public string ResearchGroupId { get; set; }
        public string Room { get; set; }
        public string Surname { get; set; }
        public string TelephoneNumber { get; set; }
        public string TitleId { get; set; }
        public string URL { get; set; }

        // Added to return UserAcademicRank table with UserProfile table
        public IUserAcademicRank UserAcademicRank { get; set; }

        // Added to return UserTitle table with UserProfile table
        public IUserTitle UserTitle { get; set; }

        #endregion Properties
    }
}