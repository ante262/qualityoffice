﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IForeignInstitutionService
    {
        #region Methods

        Task<int> DeleteAsync(IForeignInstitution entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IForeignInstitution>> GetAsync();

        Task<IForeignInstitution> GetAsync(string ID);

        Task<int> InsertAsync(IForeignInstitution entity);

        Task<int> UpdateAsync(IForeignInstitution entity);

        #endregion Methods
    }
}