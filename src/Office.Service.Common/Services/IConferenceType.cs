﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IConferenceTypeService
    {
        #region Methods

        Task<int> DeleteAsync(IConferenceType entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IConferenceType>> GetAsync();

        Task<IConferenceType> GetAsync(string ID);

        Task<int> InsertAsync(IConferenceType entity);

        Task<int> UpdateAsync(IConferenceType entity);

        #endregion Methods
    }
}