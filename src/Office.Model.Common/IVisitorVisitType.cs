﻿using System;

namespace Office.Model.Common
{
    public interface IVisitorVisitType
    {
        #region Properties

        string ID { get; set; }
        string VisitorVisitId { get; set; }
        IVisitType VisitType { get; set; }
        string VisitTypeId { get; set; }

        #endregion Properties
    }
}