﻿(function (angular) {
    'use strict';
    angular.module('officeApp').service('roleService', ['$http', 'baseUrl',
        function ($http, baseUrl) {
            var uarUrl = "Role";

            return {
                //get all roles
                fetch: function () {
                    return $http.get(baseUrl + uarUrl);
                },
                //get role by id
                get: function (id) {
                    return $http.get(baseUrl + uarUrl + "/" + id);
                },
                //post role
                post: function (data) {
                    var req = {
                        method: 'POST',
                        url: baseUrl + uarUrl,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        transformRequest: function (obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: data
                    };
                    return $http(req);
                },

                //put role
                put: function (data) {
                    var req = {
                        method: 'PUT',
                        url: baseUrl + uarUrl + "/" + data.ID,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        transformRequest: function (obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: data
                    };
                    return $http(req);
                },

                //delete role
                delete: function (item) {
                    var req = {
                        method: 'DELETE',
                        url: baseUrl + uarUrl + "/" + item,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    };
                    return $http(req);
                }
            };
        }]);
})(angular);