﻿using Ninject.Modules;
using Office.Model.Common;

using System;

namespace Office.Model
{
    public class DIModule : NinjectModule
    {
        #region Methods

        public override void Load()
        {
            Bind<IChair>().To<ChairPOCO>();
            Bind<ICommitteeMember>().To<CommitteeMemberPOCO>();
            Bind<ICommitteeMemberRole>().To<CommitteeMemberRolePOCO>();
            Bind<ICommittee>().To<CommitteePOCO>();
            Bind<IConferenceInstitution>().To<ConferenceInstitutionPOCO>();
            Bind<IConference>().To<ConferencePOCO>();
            Bind<IConferenceType>().To<ConferenceTypePOCO>();
            Bind<IContact>().To<ContactPOCO>();
            Bind<IContract>().To<ContractPOCO>();
            Bind<IContractType>().To<ContractTypePOCO>();
            Bind<ICourse>().To<CoursePOCO>();
            Bind<IDepartment>().To<DepartmentPOCO>();
            Bind<IDuration>().To<DurationPOCO>();
            Bind<IElectingInstitution>().To<ElectingInstitutionPOCO>();
            Bind<IExchangeOrganization>().To<ExchangeOrganizationPOCO>();
            Bind<IForeignInstitution>().To<ForeignInstitutionPOCO>();
            Bind<IForeignStudentExchange>().To<ForeignStudentExchangePOCO>();
            Bind<IForeignStudent>().To<ForeignStudentPOCO>();
            Bind<IFunding>().To<FundingPOCO>();
            Bind<INonPublicationLecture>().To<NonPublicationLecturePOCO>();
            Bind<IOurProfessorVisit>().To<OurProfessorVisitPOCO>();
            Bind<IOurStudentExchange>().To<OurStudentExchangePOCO>();
            Bind<IProfessorVisitType>().To<ProfessorVisitTypePOCO>();
            Bind<IResearchGroup>().To<ResearchGroupPOCO>();
            Bind<IRole>().To<RolePOCO>();
            Bind<ISemester>().To<SemesterPOCO>();
            Bind<IUserAcademicRank>().To<UserAcademicRankPOCO>();
            Bind<IUser>().To<UserPOCO>();
            Bind<IUserProfile>().To<UserProfilePOCO>();
            Bind<IUserTitle>().To<UserTitlePOCO>();
            Bind<IVisitorProfile>().To<VisitorProfilePOCO>();
            Bind<IVisitorVisit>().To<VisitorVisitPOCO>();
            Bind<IVisitorVisitType>().To<VisitorVisitTypePOCO>();
            Bind<IVisitPurpose>().To<VisitPurposePOCO>();
            Bind<IVisitType>().To<VisitTypePOCO>();
        }

        #endregion Methods
    }
}