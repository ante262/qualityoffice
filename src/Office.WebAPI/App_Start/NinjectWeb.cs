[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Office.WebAPI.App_Start.NinjectWeb), "Start")]

namespace Office.WebAPI.App_Start
{
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject.Web;

    public static class NinjectWeb
    {
        #region Methods

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
        }

        #endregion Methods
    }
}