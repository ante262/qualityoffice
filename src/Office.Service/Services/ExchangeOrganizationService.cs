﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Office.Service
{
    public class ExchangeOrganizationService : IExchangeOrganizationService
    {
        #region Constructors

        public ExchangeOrganizationService(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IExchangeOrganization entity)
        {
            return Repository.DeleteAsync<exchangeorganization>(Mapper.Map<exchangeorganization>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<exchangeorganization>(ID);
        }

        public async Task<List<IExchangeOrganization>> GetAsync()
        {
            return Mapper.Map<List<IExchangeOrganization>>(
                await Repository.WhereAsync<exchangeorganization>()
                        .ToListAsync<exchangeorganization>()
                );
        }

        public async Task<IExchangeOrganization> GetAsync(string ID)
        {
            return Mapper.Map<IExchangeOrganization>(await Repository.SingleAsync<exchangeorganization>(ID));
        }

        public Task<int> InsertAsync(IExchangeOrganization entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            return Repository.InsertAsync<exchangeorganization>(Mapper.Map<exchangeorganization>(entity));
        }

        public Task<int> UpdateAsync(IExchangeOrganization entity)
        {
            return Repository.UpdateAsync<exchangeorganization>(Mapper.Map<exchangeorganization>(entity));
        }

        #endregion Methods
    }
}