﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IVisitTypeService
    {
        #region Methods

        Task<int> DeleteAsync(IVisitType entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IVisitType>> GetAsync();

        Task<IVisitType> GetAsync(string ID);

        Task<int> InsertAsync(IVisitType entity);

        Task<int> UpdateAsync(IVisitType entity);

        #endregion Methods
    }
}