(function (angular) {
    'use strict';
    angular.module('officeApp').controller('EditOurStudentExchangeController', ['$scope', 'ourStudentExchangeService', 'lookupTablesService', 'foreignInstitutionService', 'exchangeOrganizationService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, ourStudentExchangeService, lookupTablesService, foreignInstitutionService, exchangeOrganizationService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.ourStudentExchange = $scope.ngDialogData;
            if (vm.ourStudentExchange && vm.ourStudentExchange.StartDate) {
                vm.ourStudentExchange.StartDate = new Date(vm.ourStudentExchange.StartDate);
            }
            vm.lookups = [];
            vm.foreignInstitutions = [];
            vm.exchangeOrganizations = [];
            vm.check = {
                institution: false,
                department: false,
                purpose: false,
                organization: false,
                semester: false,
                duration: false,
                tfunding: false,
                sfunding: false
            };

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addExchangeOrganization = function () {
                ngDialog.open({
                    template: 'app/exchange-organization/add-exchange-organization.html',
                    controller: 'AddExchangeOrganizationController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchForeignInstitutions = function () {
                return foreignInstitutionService.fetch()
                .error(function (data) {
                })
                .success(function (data) {
                    vm.foreignInstitutions = data;
                });
            };

            vm.fetchExchangeOrganizations = function () {
                return exchangeOrganizationService.fetch()
                .error(function (data) {
                })
                .success(function (data) {
                    vm.exchangeOrganizations = data;
                });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('Departments, VisitPurposes, Durations, Fundings, Semesters')
                .then(function (data) {
                    vm.lookups = data;
                },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchForeignInstitutions();
                vm.fetchExchangeOrganizations();
                vm.fetchLookups();
            }
            vm.fetch();

            vm.put = function (item) {
                if (item.Department || item.Duration || item.TravelFunding || item.FundingOfStay ||
                    item.VisitPurpose || item.Semester) {
                    lookupTablesService.clearCache();
                };
                vm.ourStudentExchange.ForeignInstitution = null;
                vm.ourStudentExchange.ExchangeOrganization = null;

                ourStudentExchangeService.put(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                        $state.go('ourStudentExchange', { 'notification': notifications.entity.editError });
                    });
            };
        }]);
})(angular);