﻿(function (angular) {
    'use strict';
    angular.module('officeApp', [
        'ui.router',
        'LocalStorageModule',
        'smart-table',
        'angularjs-dropdown-multiselect',
        'ngDialog'
    ])
    .config(['ngDialogProvider', function (ngDialogProvider) {
        ngDialogProvider.setDefaults({
            className: 'ngdialog-theme-default',
            showClose: true,
            closeByEscape: true
        });
    }]);
})(angular);