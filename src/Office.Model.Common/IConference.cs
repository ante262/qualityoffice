﻿using System;
using System.Collections.Generic;

namespace Office.Model.Common
{
    public interface IConference
    {
        #region Properties

        string Comment { get; set; }

        IList<IConferenceInstitution> ConferenceInstitutions { get; set; }

        // Added to return ConferenceType table with Conference table
        IConferenceType ConferenceType { get; set; }

        string ConferenceTypeId { get; set; }
        Nullable<System.DateTime> Date { get; set; }
        string EnglishName { get; set; }
        string ID { get; set; }
        string Locality { get; set; }
        string Location { get; set; }
        string Name { get; set; }
        string Profession { get; set; }
        string URL { get; set; }

        #endregion Properties
    }
}