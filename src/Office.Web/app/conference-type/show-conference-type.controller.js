﻿(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ShowConferenceTypeController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.conferenceType = $scope.ngDialogData;
        }]);
})(angular);