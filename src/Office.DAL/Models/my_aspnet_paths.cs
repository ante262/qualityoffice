using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class my_aspnet_paths
    {
        public int applicationId { get; set; }
        public string pathId { get; set; }
        public string path { get; set; }
        public string loweredPath { get; set; }
    }
}
