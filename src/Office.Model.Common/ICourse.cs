﻿using System;

namespace Office.Model.Common
{
    public interface ICourse
    {
        #region Properties

        string Abbreviation { get; set; }
        string Comment { get; set; }
        string ID { get; set; }
        string Name { get; set; }
        string URL { get; set; }

        #endregion Properties
    }
}