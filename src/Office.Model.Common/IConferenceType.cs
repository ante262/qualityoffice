﻿using System;

namespace Office.Model.Common
{
    public interface IConferenceType
    {
        #region Properties

        string Abbreviation { get; set; }
        string ID { get; set; }
        string Type { get; set; }

        #endregion Properties
    }
}