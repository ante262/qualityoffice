﻿using Ninject.Extensions.Factory;
using Ninject.Modules;
using Office.DAL;
using Office.DAL.Models;
using Office.Repository.Common;

using System;

namespace Office.Repository
{
    public class DIModule : NinjectModule
    {
        #region Methods

        public override void Load()
        {
            Bind<IofficeappContext>().To<officeappContext>();
            Bind<IRepository>().To<Repository>();
            Bind<IUnitOfWork>().To<UnitOfWork>();
            Bind<IUnitOfWorkFactory>().ToFactory();
            Bind<IRepositoryFactory>().ToFactory();

            Bind<IRoleRepository>().To<RoleRepository>();
            Bind<IUserRepository>().To<UserRepository>();
        }

        #endregion Methods
    }
}