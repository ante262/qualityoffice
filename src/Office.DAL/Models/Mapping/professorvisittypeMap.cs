using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class professorvisittypeMap : EntityTypeConfiguration<professorvisittype>
    {
        public professorvisittypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.OurProfessorVisitId)
                .HasMaxLength(128);

            this.Property(t => t.VisitTypeId)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("professorvisittype", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.OurProfessorVisitId).HasColumnName("OurProfessorVisitId");
            this.Property(t => t.VisitTypeId).HasColumnName("VisitTypeId");

            // Relationships
            this.HasOptional(t => t.ourprofessorvisit)
                .WithMany(t => t.professorvisittypes)
                .HasForeignKey(d => d.OurProfessorVisitId);
            this.HasOptional(t => t.visittype)
                .WithMany(t => t.professorvisittypes)
                .HasForeignKey(d => d.VisitTypeId);

        }
    }
}
