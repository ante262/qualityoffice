(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowConferenceController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.conference = $scope.ngDialogData;
        }]);
})(angular);