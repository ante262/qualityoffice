﻿using AutoMapper;
using Office.Model;
using Office.Model.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/Conference")]
    public class ConferenceController : ApiController
    {
        #region Constructors

        public ConferenceController(IConferenceService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private IConferenceService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/Conference/
        [HttpDelete]
        [Route("{ID}")]
        public async Task<IHttpActionResult> Delete(string ID)
        {
            try
            {
                var result = await Service.DeleteAsync(ID);
                if (result == 1) return Ok("Deleted");
                else return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        // GET: api/Conference
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<ConferenceModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/Conference/5
        [HttpGet]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Get(string ID)
        {
            var result = await Service.GetAsync(ID);

            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<ConferenceModel>(result));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        // POST: api/Conference
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post(ConferenceModel entity)
        {
            try
            {
                var result = await Service.InsertAsync(Mapper.Map<IConference>(entity));

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Add operation error.");

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/Conference/5
        [HttpPut]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Put(string ID, ConferenceModel entity)
        {
            try
            {
                if (ID != entity.ID)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "IDs do not match.");
                }

                var result = await Service.UpdateAsync(Mapper.Map<IConference>(entity));
                if (result > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "Failed.");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public partial class ConferenceInstitutionModel
        {
            #region Properties

            public string ConferenceId { get; set; }
            public ForeignInstitutionModel ForeignInstitution { get; set; }
            public string ForeignInstitutionId { get; set; }
            public string ID { get; set; }

            #endregion Properties
        }

        public partial class ConferenceModel
        {
            #region Properties

            public string Comment { get; set; }

            public IList<ConferenceInstitutionModel> ConferenceInstitutions { get; set; }

            // Added to return ConferenceType table with Conference table
            public ConferenceTypePOCO ConferenceType { get; set; }

            public string ConferenceTypeId { get; set; }
            public Nullable<System.DateTime> Date { get; set; }
            public string EnglishName { get; set; }
            public string ID { get; set; }
            public string Locality { get; set; }
            public string Location { get; set; }
            public string Name { get; set; }
            public string Profession { get; set; }
            public string URL { get; set; }

            #endregion Properties
        }

        public partial class ForeignInstitutionModel
        {
            #region Properties

            public string ID { get; set; }
            public string InstitutionName { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}