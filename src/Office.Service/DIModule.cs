﻿using Ninject.Modules;
using Office.Service.Common;

using System;

namespace Office.Service
{
    public class DIModule : NinjectModule
    {
        #region Methods

        public override void Load()
        {
            Bind<IRoleService>().To<RoleService>();
            Bind<IUserService>().To<UserService>();
            Bind<ICourseService>().To<CourseService>();
            Bind<IChairService>().To<ChairService>();
            Bind<IConferenceService>().To<ConferenceService>();
            Bind<IUserProfileService>().To<UserProfileService>();
            Bind<IForeignStudentExchangeService>().To<ForeignStudentExchangeService>();
            Bind<IForeignStudentService>().To<ForeignStudentService>();
            Bind<ICommitteeMemberService>().To<CommitteeMemberService>();
            Bind<IContractService>().To<ContractService>();
            Bind<IContactService>().To<ContactService>();
            Bind<IExchangeOrganizationService>().To<ExchangeOrganizationService>();
            Bind<INonPublicationLectureService>().To<NonPublicationLectureService>();
            Bind<IForeignInstitutionService>().To<ForeignInstitutionService>();
            Bind<IOurProfessorVisitService>().To<OurProfessorVisitService>();
            Bind<IOurStudentExchangeService>().To<OurStudentExchangeService>();
            Bind<IVisitorProfileService>().To<VisitorProfileService>();
            Bind<IVisitorVisitService>().To<VisitorVisitService>();
            Bind<ILookupTablesService>().To<LookupTablesService>();
            Bind<IVisitTypeService>().To<VisitTypeService>();
            Bind<IConferenceTypeService>().To<ConferenceTypeService>();
        }

        #endregion Methods
    }
}