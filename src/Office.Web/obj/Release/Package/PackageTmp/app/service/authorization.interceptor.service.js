﻿(function (angular) {
    'use strict';
    angular.module('officeApp').service('authorizationInterceptorService', ['$q', 'localStorageService', '$injector',
        function ($q, localStorageService, $injector) {
            return {
                request: function (config) {
                    config.headers = config.headers || {};

                    var authorizationData = localStorageService.get('authorizationData');

                    if (authorizationData) {
                        config.headers.Authorization = 'Bearer ' + authorizationData.token;
                    }

                    return config;
                },
                responseError: function (response) {
                    if (response.status === 401) {
                        $injector.get('authorizationService').logOut();
                        $injector.get('$state').go('login');
                    }
                    return $q.reject(response);
                }
            } // return
        }]);
})(angular);