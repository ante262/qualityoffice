﻿using System.Collections.Generic;

namespace Office.Model.Common
{
    public interface ILookupTablesModel
    {
        #region Properties

        List<IChair> Chairs { get; set; }
        List<ICommitteeMemberRole> CommitteeMemberRoles { get; set; }
        List<ICommittee> Committees { get; set; }
        List<IConferenceType> ConferenceTypes { get; set; }
        List<IContact> Contacts { get; set; }
        List<IContractType> ContractTypes { get; set; }
        List<ICourse> Courses { get; set; }
        List<IDepartment> Departments { get; set; }
        List<IDuration> Durations { get; set; }
        List<IElectingInstitution> ElectingInstitutions { get; set; }
        List<IExchangeOrganization> ExchangeOrganizations { get; set; }
        List<IFunding> Fundings { get; set; }
        List<IResearchGroup> ResearchGroups { get; set; }
        List<IRole> Roles { get; set; }
        List<ISemester> Semesters { get; set; }
        List<IUserAcademicRank> UserAcademicRanks { get; set; }
        List<IUserTitle> UserTitles { get; set; }
        List<IVisitPurpose> VisitPurposes { get; set; }
        List<IVisitType> VisitTypes { get; set; }

        object this[string propertyName] { get; set; }

        #endregion Properties
    }
}