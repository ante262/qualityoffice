(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('NonPublicationLectureController', ['$scope', 'nonPublicationLectureService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, nonPublicationLectureService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.nonPublicationLectures = [];
            vm.nonPublicationLecture = {};
            vm.selectedNonPublicationLecture = null;
            vm.loading = true;

            vm.addNonPublicationLecture = function () {
                ngDialog.open({
                    template: 'app/non-publication-lecture/add-non-publication-lecture.html',
                    controller: 'AddNonPublicationLectureController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editNonPublicationLecture = function (data) {
                ngDialog.open({
                    template: 'app/non-publication-lecture/edit-non-publication-lecture.html',
                    controller: 'EditNonPublicationLectureController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.showNonPublicationLecture = function (data) {
                ngDialog.open({
                    template: 'app/non-publication-lecture/show-non-publication-lecture.html',
                    controller: 'ShowNonPublicationLectureController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            };

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return nonPublicationLectureService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('nonPublicationLecture', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get nonPublicationLecture:' + data.message);
                    $state.go('nonPublicationLecture', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.nonPublicationLectures = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedNonPublicationLecture = item;
                nonPublicationLectureService.get(vm.selectedNonPublicationLecture)
                    .success(function (data) {
                        vm.nonPublicationLecture = data;
                    }).error(function (data) {
                        console.log("Unable to get this nonPublicationLecture:" + data.message);
                    });
            };

            vm.delete = function (data) {
                vm.nonPublicationLecture = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Predavanje '" + vm.nonPublicationLecture.Title + "' će biti obrisano!")) {
                    nonPublicationLectureService.delete(vm.nonPublicationLecture.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);