﻿using AutoMapper;
using Office.Model;
using Office.Model.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/OurStudentExchange")]
    public class OurStudentExchangeController : ApiController
    {
        #region Constructors

        public OurStudentExchangeController(IOurStudentExchangeService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private IOurStudentExchangeService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/OurStudentExchange/
        [HttpDelete]
        [Route("{ID}")]
        public async Task<IHttpActionResult> Delete(string ID)
        {
            try
            {
                var result = await Service.DeleteAsync(ID);
                if (result == 1) return Ok("Deleted");
                else return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        // GET: api/OurStudentExchange
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<OurStudentExchangeModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/OurStudentExchange/5
        [HttpGet]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Get(string ID)
        {
            var result = await Service.GetAsync(ID);

            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<OurStudentExchangeModel>(result));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        // POST: api/OurStudentExchange
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post(OurStudentExchangeModel entity)
        {
            try
            {
                var result = await Service.InsertAsync(Mapper.Map<IOurStudentExchange>(entity));

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Add operation error.");

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/OurStudentExchange/5
        [HttpPut]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Put(string ID, OurStudentExchangeModel entity)
        {
            try
            {
                if (ID != entity.ID)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "IDs do not match.");
                }

                var result = await Service.UpdateAsync(Mapper.Map<IOurStudentExchange>(entity));
                if (result > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "Failed.");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public partial class DepartmentModel
        {
            #region Properties

            public string Name { get; set; }

            #endregion Properties
        }

        public partial class ExchangeOrganizationModel
        {
            #region Properties

            public string Name { get; set; }

            #endregion Properties
        }

        public partial class ForeignInstitutionModel
        {
            #region Properties

            public string InstitutionName { get; set; }

            #endregion Properties
        }

        public partial class OurStudentExchangeModel
        {
            #region Properties

            public Nullable<short> AcademicYear { get; set; }
            public string Comment { get; set; }
            public DepartmentModel Department { get; set; }
            public string DepartmentId { get; set; }
            public DurationPOCO Duration { get; set; }
            public string DurationId { get; set; }
            public ExchangeOrganizationModel ExchangeOrganization { get; set; }
            public string ExchangeOrganizationId { get; set; }
            public string ExchangeResult { get; set; }
            public ForeignInstitutionModel ForeignInstitution { get; set; }
            public string ForeignInstitutionId { get; set; }
            public FundingPOCO FundingOfStay { get; set; }
            public string FundingOfStayId { get; set; }
            public string ID { get; set; }
            public string Name { get; set; }
            public SemesterPOCO Semester { get; set; }
            public string SemesterId { get; set; }
            public Nullable<System.DateTime> StartDate { get; set; }
            public string Surname { get; set; }
            public FundingPOCO TravelFunding { get; set; }
            public string TravelFundingId { get; set; }
            public VisitPurposePOCO VisitPurpose { get; set; }
            public string VisitPurposeId { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}