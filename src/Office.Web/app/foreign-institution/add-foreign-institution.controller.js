(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddForeignInstitutionController', ['$scope', 'foreignInstitutionService', 'contactService', 'userProfileService', 'contractService', 'notifications', '$window', '$state', 'ngDialog',
        function ($scope, foreignInstitutionService, contactService, userProfileService, contractService, notifications, $window, $state, ngDialog) {
            var vm = $scope.vm = {};
            vm.foreignInstitution = {};
            vm.contacts = [];
            vm.userProfiles = [];
            vm.contracts = [];
            vm.foreignInstitution.State = "Hrvatska";
            vm.check = {
                contact: false,
                user: false,
                contract: false
            };

            vm.addUserProfile = function () {
                ngDialog.open({
                    template: 'app/user-profile/add-user-profile.html',
                    controller: 'AddUserProfileController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addContact = function () {
                ngDialog.open({
                    template: 'app/contact/add-contact.html',
                    controller: 'AddContactController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addContract = function () {
                ngDialog.open({
                    template: 'app/contract/add-contract.html',
                    controller: 'AddContractController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchUsers = function () {
                return userProfileService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.userProfiles = data;
                    });
            };

            vm.fetchContracts = function () {
                return contractService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.contracts = data;
                    });
            };

            vm.fetchContacts = function () {
                return contactService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.contacts = data;
                    });
            };

            vm.fetch = function () {
                vm.fetchUsers();
                vm.fetchContracts();
                vm.fetchContacts();
            }
            vm.fetch();

            vm.post = function (item) {
                if (!item.Foreign) {
                    item.State = "Hrvatska";
                }

                foreignInstitutionService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                    });
            };
        }]);
})(angular);