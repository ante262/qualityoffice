﻿(function (angular) {
    'use strict';
    angular.module('officeApp').controller('RegisterUserController', ['$scope', 'authorizationService', 'notifications', '$state',
        function ($scope, authorizationService, notifications, $state) {
            var vm = $scope.vm = {};
            vm.registerUser = {};
            vm.confirmEmail = '';

            vm.message = $state.params.notification;
            if ($state.params.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            }

            vm.register = function (item) {
                vm.notification = "";
                vm.fadeClass = 'is-hidden';
                vm.loading = true;
                if (vm.registerUser.password == vm.registerUser.confirmPassword && vm.registerUser.email == vm.confirmEmail) {
                    authorizationService.register(item)
                        .success(function (data) {
                            $state.go('login');
                        }).error(function (data) {
                            vm.loading = false;
                            vm.fadeClass = "";
                            $state.go('register', { 'notification': notifications.entity.registerError });
                        });
                } else {
                    vm.loading = false;
                    vm.fadeClass = "";
                    vm.class = 'list-group-item-danger';
                    vm.iconClass = 'glyphicon-alert';
                    vm.notification = notifications.entity.registerInputError;
                }
            };
        }]);
})(angular);