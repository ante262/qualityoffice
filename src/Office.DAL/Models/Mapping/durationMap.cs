using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class durationMap : EntityTypeConfiguration<duration>
    {
        public durationMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Length)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.Abbreviation)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("duration", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Length).HasColumnName("Length");
            this.Property(t => t.Abbreviation).HasColumnName("Abbreviation");
        }
    }
}
