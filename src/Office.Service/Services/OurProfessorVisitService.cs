﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Office.Service
{
    public class OurProfessorVisitService : IOurProfessorVisitService
    {
        #region Constructors

        public OurProfessorVisitService(IRepository repository, ILookupTablesService lookupTablesService)
        {
            Repository = repository;
            LookupTablesService = lookupTablesService;
        }

        #endregion Constructors

        #region Properties

        protected ILookupTablesService LookupTablesService { get; private set; }

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public async Task<int> AddChoiceRangeForDeleteAsync(IUnitOfWork unitOfWork, string[] ids)
        {
            var result = 0;
            foreach (var id in ids)
            {
                result += await unitOfWork.DeleteAsync<professorvisittype>(id);
            }
            return result;
        }

        public async Task<int> AddChoiceRangeForInsertAsync(IUnitOfWork unitOfWork, string ourProfessorVisitId, IProfessorVisitType[] entities)
        {
            var result = 0;
            var newCollection = entities.Where(c => c.ID != null);
            var oldCollection = entities.Where(c => c.VisitTypeId != null);
            foreach (var element in newCollection)
            {
                var visitTypeItem = new visittype();
                visitTypeItem.ID = Guid.NewGuid().ToString("N");
                visitTypeItem.Type = element.ID;
                result += await unitOfWork.AddAsync<visittype>(Mapper.Map<visittype>(visitTypeItem));
                element.ID = Guid.NewGuid().ToString("N");
                element.OurProfessorVisitId = ourProfessorVisitId;
                element.VisitTypeId = visitTypeItem.ID;
            }
            foreach (var element in oldCollection)
            {
                element.ID = Guid.NewGuid().ToString("N");
                element.OurProfessorVisitId = ourProfessorVisitId;
                element.VisitType = null;
                var map = Mapper.Map<professorvisittype>(element);
                result += await unitOfWork.AddAsync<professorvisittype>(map);
            }
            return result;
        }

        public async Task<int> AddChoiceRangeForUpdateAsync(IUnitOfWork unitOfWork, IProfessorVisitType[] entities)
        {
            var result = 0;
            foreach (var entity in entities)
            {
                result += await unitOfWork.UpdateAsync<professorvisittype>(Mapper.Map<professorvisittype>(entity));
            }
            return result;
        }

        public Task<int> AddForUpdateAsync(IUnitOfWork unitOfWork, IOurProfessorVisit entity)
        {
            var map = Mapper.Map<ourprofessorvisit>(entity);
            map.professorvisittypes = null;
            return unitOfWork.UpdateAsync<ourprofessorvisit>(map);
        }

        public Task<int> DeleteAsync(IOurProfessorVisit entity)
        {
            return Repository.DeleteAsync<ourprofessorvisit>(Mapper.Map<ourprofessorvisit>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<ourprofessorvisit>(ID);
        }

        public async Task<List<IOurProfessorVisit>> GetAsync()
        {
            // include referencing tables into OurProfessorVisit table
            var result = await Repository.WhereAsync<ourprofessorvisit>()
                          .Include(c => c.department)
                          .Include(c => c.duration)
                          .Include(c => c.foreigninstitution)
                          .Include(c => c.funding)
                          .Include(c => c.funding1)
                          .Include(c => c.userprofile)
                          .Include(c => c.professorvisittypes.Select(b => b.visittype))
                          .ToListAsync<ourprofessorvisit>();

            return Mapper.Map<List<IOurProfessorVisit>>(result);
        }

        public async Task<IOurProfessorVisit> GetAsync(string ID)
        {
            return Mapper.Map<IOurProfessorVisit>(await Repository.SingleAsync<ourprofessorvisit>(ID));
        }

        public async Task<int> InsertAsync(IOurProfessorVisit entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            var counter = 0;
            if (entity.Department != null && entity.Department.ID == null)
            {
                entity.Department.ID = Guid.NewGuid().ToString("N");
                entity.DepartmentId = entity.Department.ID;
                counter++;
            }
            if (entity.Duration != null && entity.Duration.ID == null)
            {
                entity.Duration.ID = Guid.NewGuid().ToString("N");
                entity.DurationId = entity.Duration.ID;
                counter++;
            }
            if (entity.FundingOfStay != null && entity.FundingOfStay.ID == null)
            {
                entity.FundingOfStay.ID = Guid.NewGuid().ToString("N");
                entity.FundingOfStayId = entity.FundingOfStay.ID;
                counter++;
            }
            if (entity.TravelFunding != null && entity.TravelFunding.ID == null)
            {
                entity.TravelFunding.ID = Guid.NewGuid().ToString("N");
                entity.TravelFundingId = entity.TravelFunding.ID;
                counter++;
            }
            var collectionForInsert = entity.ProfessorVisitTypes.ToArray();
            if (collectionForInsert.Where(c => c.ID != null).Count() > 0) { counter++; };
            var unitOfWork = Repository.CreateUnitOfWork();
            await this.AddChoiceRangeForInsertAsync(unitOfWork, entity.ID, collectionForInsert);
            var map = Mapper.Map<ourprofessorvisit>(entity);
            map.professorvisittypes = null;
            await unitOfWork.AddAsync<ourprofessorvisit>(map);
            if (counter > 0) { await LookupTablesService.ClearCache(); };
            return await unitOfWork.CommitAsync();
        }

        public async Task<int> UpdateAsync(IOurProfessorVisit entity)
        {
            var lookupItems = await LookupTablesService.GetAsync();
            var existing = await Repository.SingleAsync<ourprofessorvisit>(entity.ID);

            var counter = 0;
            var unitOfWork = Repository.CreateUnitOfWork();
            if (entity.Department != null
                && !lookupItems.Departments.Select(c => c.Name).Contains(entity.Department.Name))
            {
                entity.Department.ID = Guid.NewGuid().ToString("N");
                entity.DepartmentId = entity.Department.ID;
                var map = Mapper.Map<department>(entity.Department);
                await unitOfWork.AddAsync<department>(map);
                counter++;
            }
            if (entity.Department != null
                && entity.Department.ID == entity.DepartmentId
                && lookupItems.Departments
                .Select(c => c.Name.ToLower())
                .Contains(entity.Department.Name.ToLower()))
            {
                entity.DepartmentId = lookupItems.Departments
                    .Where(c => c.Name.ToLower() == entity.Department.Name.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.Duration != null
                && !lookupItems.Durations
                .Select(c => c.Length.ToLower())
                .Contains(entity.Duration.Length.ToLower()))
            {
                entity.Duration.ID = Guid.NewGuid().ToString("N");
                entity.DurationId = entity.Duration.ID;
                var map = Mapper.Map<duration>(entity.Duration);
                await unitOfWork.AddAsync<duration>(map);
                counter++;
            }
            if (entity.Duration != null
                && entity.Duration.ID == entity.DurationId
                && lookupItems.Durations
                .Select(c => c.Length.ToLower())
                .Contains(entity.Duration.Length.ToLower()))
            {
                entity.DurationId = lookupItems.Durations
                    .Where(c => c.Length.ToLower() == entity.Duration.Length.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.TravelFunding != null
                && !lookupItems.Fundings.Select(c => c.Type).Contains(entity.TravelFunding.Type))
            {
                entity.TravelFunding.ID = Guid.NewGuid().ToString("N");
                entity.TravelFundingId = entity.TravelFunding.ID;
                var map = Mapper.Map<funding>(entity.TravelFunding);
                await unitOfWork.AddAsync<funding>(map);
                counter++;
            }
            if (entity.TravelFunding != null
                && entity.TravelFunding.ID == entity.TravelFundingId
                && lookupItems.Fundings
                .Select(c => c.Type.ToLower())
                .Contains(entity.TravelFunding.Type.ToLower()))
            {
                entity.TravelFundingId = lookupItems.Fundings
                    .Where(c => c.Type.ToLower() == entity.TravelFunding.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            if (entity.FundingOfStay != null
                && !lookupItems.Fundings.Select(c => c.Type).Contains(entity.FundingOfStay.Type))
            {
                entity.FundingOfStay.ID = Guid.NewGuid().ToString("N");
                entity.FundingOfStayId = entity.FundingOfStay.ID;
                var map = Mapper.Map<funding>(entity.FundingOfStay);
                await unitOfWork.AddAsync<funding>(map);
                counter++;
            }
            if (entity.FundingOfStay != null
                && entity.FundingOfStay.ID == entity.FundingOfStayId
                && lookupItems.Fundings
                .Select(c => c.Type.ToLower())
                .Contains(entity.FundingOfStay.Type.ToLower()))
            {
                entity.FundingOfStayId = lookupItems.Fundings
                    .Where(c => c.Type.ToLower() == entity.FundingOfStay.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }

            var collectionForInsert = entity.ProfessorVisitTypes
                .Where(choice => existing.professorvisittypes
                    .SingleOrDefault(c => c.ID == choice.ID) == null)
                    .ToArray();
            var collectionForUpdate = entity.ProfessorVisitTypes
                .Where(choice => existing.professorvisittypes
                    .SingleOrDefault(c => c.ID == choice.ID) != null)
                    .ToArray();
            var collectionForDelete = existing.professorvisittypes
                .Where(choice => entity.ProfessorVisitTypes
                    .SingleOrDefault(c => c.ID == choice.ID) == null)
                    .Select(c => c.ID)
                    .ToArray();

            entity.Department = null;
            entity.Duration = null;
            entity.TravelFunding = null;
            entity.FundingOfStay = null;
            await this.AddChoiceRangeForDeleteAsync(unitOfWork, collectionForDelete);
            await this.AddChoiceRangeForUpdateAsync(unitOfWork, collectionForUpdate);
            await this.AddChoiceRangeForInsertAsync(unitOfWork, entity.ID, collectionForInsert);
            await this.AddForUpdateAsync(unitOfWork, entity);
            if (counter > 0)
            {
                await LookupTablesService.ClearCache();
            }
            return await unitOfWork.CommitAsync();
        }

        #endregion Methods
    }
}