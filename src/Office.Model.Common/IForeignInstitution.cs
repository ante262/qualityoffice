﻿using System;

namespace Office.Model.Common
{
    public interface IForeignInstitution
    {
        #region Properties

        string City { get; set; }
        IContact Contact { get; set; }
        string ContactId { get; set; }
        IContract Contract { get; set; }
        string ContractId { get; set; }
        string Description { get; set; }
        bool Foreign { get; set; }
        string ID { get; set; }
        string InstitutionName { get; set; }
        bool IsContract { get; set; }
        string State { get; set; }
        string URL { get; set; }
        IUserProfile UserProfile { get; set; }
        string UserProfileId { get; set; }

        #endregion Properties
    }
}