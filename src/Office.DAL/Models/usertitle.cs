using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class usertitle
    {
        public usertitle()
        {
            this.userprofiles = new List<userprofile>();
            this.visitorprofiles = new List<visitorprofile>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<userprofile> userprofiles { get; set; }
        public virtual ICollection<visitorprofile> visitorprofiles { get; set; }
    }
}
