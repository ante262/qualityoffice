﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Office.Service
{
    public class CourseService : ICourseService
    {
        #region Constructors

        public CourseService(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(ICourse entity)
        {
            return Repository.DeleteAsync<course>(Mapper.Map<course>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<course>(ID);
        }

        public async Task<List<ICourse>> GetAsync()
        {
            return Mapper.Map<List<ICourse>>(
                await Repository.WhereAsync<course>()
                        .ToListAsync<course>()
                );
        }

        public async Task<ICourse> GetAsync(string ID)
        {
            return Mapper.Map<ICourse>(await Repository.SingleAsync<course>(ID));
        }

        public Task<int> InsertAsync(ICourse entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            return Repository.InsertAsync<course>(Mapper.Map<course>(entity));
        }

        public Task<int> UpdateAsync(ICourse entity)
        {
            return Repository.UpdateAsync<course>(Mapper.Map<course>(entity));
        }

        #endregion Methods
    }
}