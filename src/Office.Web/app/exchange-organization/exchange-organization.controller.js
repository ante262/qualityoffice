(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ExchangeOrganizationController', ['$scope', 'exchangeOrganizationService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, exchangeOrganizationService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.exchangeOrganizations = [];
            vm.exchangeOrganization = {};
            vm.selectedExchangeOrganization = null;
            vm.loading = true;

            vm.addExchangeOrganization = function () {
                ngDialog.open({
                    template: 'app/exchange-organization/add-exchange-organization.html',
                    controller: 'AddExchangeOrganizationController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editExchangeOrganization = function (data) {
                ngDialog.open({
                    template: 'app/exchange-organization/edit-exchange-organization.html',
                    controller: 'EditExchangeOrganizationController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.showExchangeOrganization = function (data) {
                ngDialog.open({
                    template: 'app/exchange-organization/show-exchange-organization.html',
                    controller: 'ShowExchangeOrganizationController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            };

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return exchangeOrganizationService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('exchangeOrganization', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get exchangeOrganization:' + data.message);
                    $state.go('exchangeOrganization', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.exchangeOrganizations = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedExchangeOrganization = item;
                exchangeOrganizationService.get(vm.selectedExchangeOrganization)
                    .success(function (data) {
                        vm.exchangeOrganization = data;
                    }).error(function (data) {
                        console.log("Unable to get this exchangeOrganization:" + data.message);
                    });
            };

            vm.delete = function (data) {
                vm.exchangeOrganization = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Organizacija '" + vm.exchangeOrganization.Name + "' će biti obrisana!")) {
                    exchangeOrganizationService.delete(vm.exchangeOrganization.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);