﻿(function (angular) {
    'use strict';
    angular.module('officeApp').controller('AddChairController', ['$scope', 'chairService', 'notifications', '$window', '$state','ngDialog',
        function ($scope, chairService, notifications, $window, $state, ngDialog) {
            var vm = $scope.vm = {};
            vm.chair = {};

            vm.post = function (item) {
                chairService.post(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(data);
                    });
            };
        }]);
})(angular);