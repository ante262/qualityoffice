﻿using Office.Model.Common;

using System;

namespace Office.Model
{
    public partial class ConferenceTypePOCO : IConferenceType
    {
        #region Properties

        public string Abbreviation { get; set; }
        public string ID { get; set; }
        public string Type { get; set; }

        #endregion Properties
    }
}