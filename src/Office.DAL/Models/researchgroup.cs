using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class researchgroup
    {
        public researchgroup()
        {
            this.userprofiles = new List<userprofile>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string URL { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<userprofile> userprofiles { get; set; }
    }
}
