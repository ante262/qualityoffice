﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace Office.Service
{
    public class LookupTablesService : ILookupTablesService
    {
        #region Fields

        public static readonly ObjectCache lookupCache = MemoryCache.Default;

        protected IRepository Repository { get; private set; }

        protected ILookupTablesModel result { get; private set; }

        public async Task<object> ClearCache()
        {
            return await Task.FromResult(lookupCache.Remove("lookupResult"));
        }

        #endregion Fields

        #region Constructors

        public LookupTablesService(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Methods

        public async Task<ILookupTablesModel> GetAsync()
        {
            //if (resset == true)
            //{
            //    result = null;
            //}
            result = (ILookupTablesModel)lookupCache.Get("lookupResult");

            if (result == null)
            {
                result = new LookupTablesModel();

                result.Chairs = Mapper.Map<List<IChair>>(
                    await Repository.WhereAsync<chair>()
                    .ToListAsync<chair>()
                );
                result.CommitteeMemberRoles = Mapper.Map<List<ICommitteeMemberRole>>(
                    await Repository.WhereAsync<committeememberrole>()
                    .ToListAsync<committeememberrole>()
                );
                result.Committees = Mapper.Map<List<ICommittee>>(
                    await Repository.WhereAsync<committee>()
                    .ToListAsync<committee>()
                );
                result.ConferenceTypes = Mapper.Map<List<IConferenceType>>(
                    await Repository.WhereAsync<conferencetype>()
                    .ToListAsync<conferencetype>()
                );
                result.Contacts = Mapper.Map<List<IContact>>(
                    await Repository.WhereAsync<contact>()
                    .ToListAsync<contact>()
                );
                result.ContractTypes = Mapper.Map<List<IContractType>>(
                    await Repository.WhereAsync<contracttype>()
                    .ToListAsync<contracttype>()
                );
                result.Courses = Mapper.Map<List<ICourse>>(
                     await Repository.WhereAsync<course>()
                     .ToListAsync<course>()
                );
                result.Departments = Mapper.Map<List<IDepartment>>(
                    await Repository.WhereAsync<department>()
                    .ToListAsync<department>()
                );
                result.Durations = Mapper.Map<List<IDuration>>(
                    await Repository.WhereAsync<duration>()
                    .ToListAsync<duration>()
                );
                result.ElectingInstitutions = Mapper.Map<List<IElectingInstitution>>(
                    await Repository.WhereAsync<electinginstitution>()
                    .ToListAsync<electinginstitution>()
                );
                result.ExchangeOrganizations = Mapper.Map<List<IExchangeOrganization>>(
                     await Repository.WhereAsync<exchangeorganization>()
                     .ToListAsync<exchangeorganization>()
                );
                result.Fundings = Mapper.Map<List<IFunding>>(
                    await Repository.WhereAsync<funding>()
                    .ToListAsync<funding>()
                );
                result.ResearchGroups = Mapper.Map<List<IResearchGroup>>(
                    await Repository.WhereAsync<researchgroup>()
                    .ToListAsync<researchgroup>()
                );
                result.Roles = Mapper.Map<List<IRole>>(
                    await Repository.WhereAsync<role>()
                    .ToListAsync<role>()
                );
                result.Semesters = Mapper.Map<List<ISemester>>(
                     await Repository.WhereAsync<semester>()
                     .ToListAsync<semester>()
                );
                result.UserAcademicRanks = Mapper.Map<List<IUserAcademicRank>>(
                    await Repository.WhereAsync<useracademicrank>()
                    .ToListAsync<useracademicrank>()
                );
                result.UserTitles = Mapper.Map<List<IUserTitle>>(
                    await Repository.WhereAsync<usertitle>()
                    .ToListAsync<usertitle>()
                );
                result.VisitPurposes = Mapper.Map<List<IVisitPurpose>>(
                    await Repository.WhereAsync<visitpurpose>()
                    .ToListAsync<visitpurpose>()
                );
                result.VisitTypes = Mapper.Map<List<IVisitType>>(
                    await Repository.WhereAsync<visittype>()
                    .ToListAsync<visittype>()
                );

                // return Mapper.Map<List<ILookupTablesModel>>(result);

                lookupCache.Add("lookupResult", result, DateTimeOffset.UtcNow.AddHours(24));
            }
            return result;
        }

        #endregion Methods

        #region Classes

        public class LookupTablesModel : ILookupTablesModel
        {
            #region Properties

            public List<IChair> Chairs { get; set; }
            public List<ICommitteeMemberRole> CommitteeMemberRoles { get; set; }
            public List<ICommittee> Committees { get; set; }
            public List<IConferenceType> ConferenceTypes { get; set; }
            public List<IContact> Contacts { get; set; }
            public List<IContractType> ContractTypes { get; set; }
            public List<ICourse> Courses { get; set; }
            public List<IDepartment> Departments { get; set; }
            public List<IDuration> Durations { get; set; }
            public List<IElectingInstitution> ElectingInstitutions { get; set; }
            public List<IExchangeOrganization> ExchangeOrganizations { get; set; }
            public List<IFunding> Fundings { get; set; }
            public List<IResearchGroup> ResearchGroups { get; set; }
            public List<IRole> Roles { get; set; }
            public List<ISemester> Semesters { get; set; }
            public List<IUserAcademicRank> UserAcademicRanks { get; set; }
            public List<IUserTitle> UserTitles { get; set; }
            public List<IVisitPurpose> VisitPurposes { get; set; }
            public List<IVisitType> VisitTypes { get; set; }

            public object this[string propertyName]
            {
                get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
                set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
            }

            #endregion Properties
        }

        #endregion Classes
    }
}