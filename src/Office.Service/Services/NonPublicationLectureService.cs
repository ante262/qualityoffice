﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Office.Service
{
    public class NonPublicationLectureService : INonPublicationLectureService
    {
        #region Constructors

        public NonPublicationLectureService(IRepository repository, ILookupTablesService lookupTablesService)
        {
            Repository = repository;
            LookupTablesService = lookupTablesService;
        }

        #endregion Constructors

        #region Properties

        protected ILookupTablesService LookupTablesService { get; private set; }
        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(INonPublicationLecture entity)
        {
            return Repository.DeleteAsync<nonpublicationlecture>(Mapper.Map<nonpublicationlecture>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<nonpublicationlecture>(ID);
        }

        public async Task<List<INonPublicationLecture>> GetAsync()
        {
            // include ConferenceType table into NonPublicationLecture table
            var result = await Repository.WhereAsync<nonpublicationlecture>()
                          .Include(c => c.foreigninstitution)
                          .Include(c => c.conferencetype)
                          .ToListAsync<nonpublicationlecture>();

            return Mapper.Map<List<INonPublicationLecture>>(result);
        }

        public async Task<INonPublicationLecture> GetAsync(string ID)
        {
            return Mapper.Map<INonPublicationLecture>(await Repository.SingleAsync<nonpublicationlecture>(ID));
        }

        public Task<int> InsertAsync(INonPublicationLecture entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            var counter = 0;
            if (entity.ConferenceType != null && entity.ConferenceType.ID == null)
            {
                entity.ConferenceType.ID = Guid.NewGuid().ToString("N");
                entity.ConferenceTypeId = entity.ConferenceType.ID;
                counter++;
            }
            if (counter > 0)
            {
                LookupTablesService.ClearCache();
            }

            return Repository.InsertAsync<nonpublicationlecture>(Mapper.Map<nonpublicationlecture>(entity));
        }

        public async Task<int> UpdateAsync(INonPublicationLecture entity)
        {
            var lookupItems = await LookupTablesService.GetAsync();

            var counter = 0;
            var unitOfWork = Repository.CreateUnitOfWork();
            if (entity.ConferenceType != null
                && !lookupItems.ConferenceTypes.Select(c => c.Type).Contains(entity.ConferenceType.Type))
            {
                entity.ConferenceType.ID = Guid.NewGuid().ToString("N");
                entity.ConferenceTypeId = entity.ConferenceType.ID;
                var map = Mapper.Map<conferencetype>(entity.ConferenceType);
                await unitOfWork.AddAsync<conferencetype>(map);
                counter++;
            }
            if (entity.ConferenceType != null
                && entity.ConferenceType.ID == entity.ConferenceTypeId
                && lookupItems.ConferenceTypes
                .Select(c => c.Type.ToLower())
                .Contains(entity.ConferenceType.Type.ToLower()))
            {
                entity.ConferenceTypeId = lookupItems.ConferenceTypes
                    .Where(c => c.Type.ToLower() == entity.ConferenceType.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }

            entity.ConferenceType = null;
            await unitOfWork.UpdateAsync<nonpublicationlecture>(Mapper.Map<nonpublicationlecture>(entity));
            if (counter > 0)
            {
                await LookupTablesService.ClearCache();
            }
            return await unitOfWork.CommitAsync();
        }

        #endregion Methods
    }
}