﻿using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service
{
    public class UserService : IUserService
    {
        #region Fields

        protected IUnitOfWork UnitOfWork;

        #endregion Fields

        #region Constructors

        public UserService(IUserRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IUserRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IUser entity)
        {
            return Repository.DeleteAsync(entity);
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync(ID);
        }

        public Task<List<IUser>> GetAsync()
        {
            return Repository.GetAsync();
        }

        public Task<IUser> GetAsync(string ID)
        {
            return Repository.GetAsync(ID);
        }

        #endregion Methods
    }
}