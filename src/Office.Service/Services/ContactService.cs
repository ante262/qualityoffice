﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Office.Service
{
    public class ContactService : IContactService
    {
        #region Constructors

        public ContactService(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IContact entity)
        {
            return Repository.DeleteAsync<contact>(Mapper.Map<contact>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<contact>(ID);
        }

        public async Task<List<IContact>> GetAsync()
        {
            var result = await Repository.WhereAsync<contact>()
                          .ToListAsync<contact>();

            return Mapper.Map<List<IContact>>(result);
        }

        public async Task<IContact> GetAsync(string ID)
        {
            return Mapper.Map<IContact>(await Repository.SingleAsync<contact>(ID));
        }

        public Task<int> InsertAsync(IContact entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");

            return Repository.InsertAsync<contact>(Mapper.Map<contact>(entity));
        }

        public Task<int> UpdateAsync(IContact entity)
        {
            return Repository.UpdateAsync<contact>(Mapper.Map<contact>(entity));
        }

        #endregion Methods
    }
}