﻿using Office.Model.Common;
using System;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface ILookupTablesService
    {
        #region Methods

        Task<object> ClearCache();

        Task<ILookupTablesModel> GetAsync();

        #endregion Methods
    }
}