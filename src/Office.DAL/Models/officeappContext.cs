using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Office.DAL.Models.Mapping;

namespace Office.DAL.Models
{
    public partial class officeappContext : DbContext, IofficeappContext
    {
        static officeappContext()
        {
            Database.SetInitializer<officeappContext>(null);
        }

        public officeappContext()
            : base("Name=officeappContext")
        {
        }

        public DbSet<chair> chairs { get; set; }
        public DbSet<committee> committees { get; set; }
        public DbSet<committeemember> committeemembers { get; set; }
        public DbSet<committeememberrole> committeememberroles { get; set; }
        public DbSet<conference> conferences { get; set; }
        public DbSet<conferenceinstitution> conferenceinstitutions { get; set; }
        public DbSet<conferencetype> conferencetypes { get; set; }
        public DbSet<contact> contacts { get; set; }
        public DbSet<contract> contracts { get; set; }
        public DbSet<contracttype> contracttypes { get; set; }
        public DbSet<course> courses { get; set; }
        public DbSet<department> departments { get; set; }
        public DbSet<duration> durations { get; set; }
        public DbSet<electinginstitution> electinginstitutions { get; set; }
        public DbSet<exchangeorganization> exchangeorganizations { get; set; }
        public DbSet<foreigninstitution> foreigninstitutions { get; set; }
        public DbSet<foreignstudent> foreignstudents { get; set; }
        public DbSet<foreignstudentexchange> foreignstudentexchanges { get; set; }
        public DbSet<funding> fundings { get; set; }
        public DbSet<my_aspnet_applications> my_aspnet_applications { get; set; }
        public DbSet<my_aspnet_membership> my_aspnet_membership { get; set; }
        public DbSet<my_aspnet_paths> my_aspnet_paths { get; set; }
        public DbSet<my_aspnet_personalizationallusers> my_aspnet_personalizationallusers { get; set; }
        public DbSet<my_aspnet_personalizationperuser> my_aspnet_personalizationperuser { get; set; }
        public DbSet<my_aspnet_profiles> my_aspnet_profiles { get; set; }
        public DbSet<my_aspnet_roles> my_aspnet_roles { get; set; }
        public DbSet<my_aspnet_sessioncleanup> my_aspnet_sessioncleanup { get; set; }
        public DbSet<my_aspnet_sessions> my_aspnet_sessions { get; set; }
        public DbSet<my_aspnet_sitemap> my_aspnet_sitemap { get; set; }
        public DbSet<my_aspnet_users> my_aspnet_users { get; set; }
        public DbSet<my_aspnet_usersinroles> my_aspnet_usersinroles { get; set; }
        public DbSet<nonpublicationlecture> nonpublicationlectures { get; set; }
        public DbSet<ourprofessorvisit> ourprofessorvisits { get; set; }
        public DbSet<ourstudentexchange> ourstudentexchanges { get; set; }
        public DbSet<professorvisittype> professorvisittypes { get; set; }
        public DbSet<researchgroup> researchgroups { get; set; }
        public DbSet<role> roles { get; set; }
        public DbSet<semester> semesters { get; set; }
        public DbSet<user> users { get; set; }
        public DbSet<useracademicrank> useracademicranks { get; set; }
        public DbSet<userclaim> userclaims { get; set; }
        public DbSet<userlogin> userlogins { get; set; }
        public DbSet<userprofile> userprofiles { get; set; }
        public DbSet<usertitle> usertitles { get; set; }
        public DbSet<visitorprofile> visitorprofiles { get; set; }
        public DbSet<visitorvisit> visitorvisits { get; set; }
        public DbSet<visitorvisittype> visitorvisittypes { get; set; }
        public DbSet<visitpurpose> visitpurposes { get; set; }
        public DbSet<visittype> visittypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new chairMap());
            modelBuilder.Configurations.Add(new committeeMap());
            modelBuilder.Configurations.Add(new committeememberMap());
            modelBuilder.Configurations.Add(new committeememberroleMap());
            modelBuilder.Configurations.Add(new conferenceMap());
            modelBuilder.Configurations.Add(new conferenceinstitutionMap());
            modelBuilder.Configurations.Add(new conferencetypeMap());
            modelBuilder.Configurations.Add(new contactMap());
            modelBuilder.Configurations.Add(new contractMap());
            modelBuilder.Configurations.Add(new contracttypeMap());
            modelBuilder.Configurations.Add(new courseMap());
            modelBuilder.Configurations.Add(new departmentMap());
            modelBuilder.Configurations.Add(new durationMap());
            modelBuilder.Configurations.Add(new electinginstitutionMap());
            modelBuilder.Configurations.Add(new exchangeorganizationMap());
            modelBuilder.Configurations.Add(new foreigninstitutionMap());
            modelBuilder.Configurations.Add(new foreignstudentMap());
            modelBuilder.Configurations.Add(new foreignstudentexchangeMap());
            modelBuilder.Configurations.Add(new fundingMap());
            modelBuilder.Configurations.Add(new my_aspnet_applicationsMap());
            modelBuilder.Configurations.Add(new my_aspnet_membershipMap());
            modelBuilder.Configurations.Add(new my_aspnet_pathsMap());
            modelBuilder.Configurations.Add(new my_aspnet_personalizationallusersMap());
            modelBuilder.Configurations.Add(new my_aspnet_personalizationperuserMap());
            modelBuilder.Configurations.Add(new my_aspnet_profilesMap());
            modelBuilder.Configurations.Add(new my_aspnet_rolesMap());
            modelBuilder.Configurations.Add(new my_aspnet_sessioncleanupMap());
            modelBuilder.Configurations.Add(new my_aspnet_sessionsMap());
            modelBuilder.Configurations.Add(new my_aspnet_sitemapMap());
            modelBuilder.Configurations.Add(new my_aspnet_usersMap());
            modelBuilder.Configurations.Add(new my_aspnet_usersinrolesMap());
            modelBuilder.Configurations.Add(new nonpublicationlectureMap());
            modelBuilder.Configurations.Add(new ourprofessorvisitMap());
            modelBuilder.Configurations.Add(new ourstudentexchangeMap());
            modelBuilder.Configurations.Add(new professorvisittypeMap());
            modelBuilder.Configurations.Add(new researchgroupMap());
            modelBuilder.Configurations.Add(new roleMap());
            modelBuilder.Configurations.Add(new semesterMap());
            modelBuilder.Configurations.Add(new userMap());
            modelBuilder.Configurations.Add(new useracademicrankMap());
            modelBuilder.Configurations.Add(new userclaimMap());
            modelBuilder.Configurations.Add(new userloginMap());
            modelBuilder.Configurations.Add(new userprofileMap());
            modelBuilder.Configurations.Add(new usertitleMap());
            modelBuilder.Configurations.Add(new visitorprofileMap());
            modelBuilder.Configurations.Add(new visitorvisitMap());
            modelBuilder.Configurations.Add(new visitorvisittypeMap());
            modelBuilder.Configurations.Add(new visitpurposeMap());
            modelBuilder.Configurations.Add(new visittypeMap());
        }
    }
}
