(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowNonPublicationLectureController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.nonPublicationLecture = $scope.ngDialogData;
        }]);
})(angular);