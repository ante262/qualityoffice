﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Office.Service
{
    public class ChairService : IChairService
    {
        #region Constructors

        public ChairService(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IChair entity)
        {
            return Repository.DeleteAsync<chair>(Mapper.Map<chair>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<chair>(ID);
        }

        public async Task<List<IChair>> GetAsync()
        {
            return Mapper.Map<List<IChair>>(
                await Repository.WhereAsync<chair>()
                        .ToListAsync<chair>()
                );
        }

        public async Task<IChair> GetAsync(string ID)
        {
            return Mapper.Map<IChair>(await Repository.SingleAsync<chair>(ID));
        }

        public Task<int> InsertAsync(IChair entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            return Repository.InsertAsync<chair>(Mapper.Map<chair>(entity));
        }

        public Task<int> UpdateAsync(IChair entity)
        {
            return Repository.UpdateAsync<chair>(Mapper.Map<chair>(entity));
        }

        #endregion Methods
    }
}