using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class visittype
    {
        public visittype()
        {
            this.professorvisittypes = new List<professorvisittype>();
            this.visitorvisittypes = new List<visitorvisittype>();
        }

        public string ID { get; set; }
        public string Type { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<professorvisittype> professorvisittypes { get; set; }
        public virtual ICollection<visitorvisittype> visitorvisittypes { get; set; }
    }
}
