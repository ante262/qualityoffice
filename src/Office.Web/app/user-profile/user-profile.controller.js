(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('UserProfileController', ['$scope', 'userProfileService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, userProfileService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.userProfiles = [];
            vm.userProfile = {};
            vm.selectedUserProfile = null;
            vm.loading = true;

            vm.addUserProfile = function () {
                ngDialog.open({
                    template: 'app/user-profile/add-user-profile.html',
                    controller: 'AddUserProfileController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editUserProfile = function (data) {
                ngDialog.open({
                    template: 'app/user-profile/edit-user-profile.html',
                    controller: 'EditUserProfileController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.showUserProfile = function (data) {
                ngDialog.open({
                    template: 'app/user-profile/show-user-profile.html',
                    controller: 'ShowUserProfileController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
                .closePromise.then(function (data) {
                });
            };

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return userProfileService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('userProfile', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get userProfile:' + data.message);
                    $state.go('userProfile', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.userProfiles = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedUserProfile = item;
                userProfileService.get(vm.selectedUserProfile)
                    .success(function (data) {
                        vm.userProfile = data;
                    }).error(function (data) {
                        console.log("Unable to get this userProfile:" + data.message);
                    });
            };

            vm.delete = function (data) {
                vm.userProfile = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Korisnik '" + vm.userProfile.Name + " " + vm.userProfile.SurName + "' će biti obrisan!")) {
                    userProfileService.delete(vm.userProfile.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);