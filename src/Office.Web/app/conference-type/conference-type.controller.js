﻿(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ConferenceTypeController', ['$scope', 'conferenceTypeService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, conferenceTypeService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.conferenceTypes = [];
            vm.conferenceType = {};
            vm.selectedConferenceType = null;
            vm.loading = true;

            vm.addConferenceType = function () {
                ngDialog.open({
                    template: 'app/conference-type/add-conference-type.html',
                    controller: 'AddConferenceTypeController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editConferenceType = function (data) {
                ngDialog.open({
                    template: 'app/conference-type/edit-conference-type.html',
                    controller: 'EditConferenceTypeController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            }

            vm.showConferenceType = function (data) {
                ngDialog.open({
                    template: 'app/conference-type/show-conference-type.html',
                    controller: 'ShowConferenceTypeController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            }

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return conferenceTypeService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('conferenceType', { 'notification': notifications.entity.serverError });
                    };
                    //console.log('Unable to get conferenceType:' + data.message);
                    $state.go('conferenceType', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.conferenceTypes = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedConferenceType = item;
                conferenceTypeService.get(vm.selectedConferenceType).success(function (data) {
                    vm.conferenceType = data;
                }).error(function (data) {
                    console.log("Unable to get this conferenceType:" + data.message);
                });
            };

            vm.delete = function (data) {
                vm.conferenceType = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Vrsta '" + vm.conferenceType.Type + "' će biti obrisana!")) {
                    conferenceTypeService.delete(vm.conferenceType.ID)
                        .success(function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        })
                        .error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        })
                        .finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);