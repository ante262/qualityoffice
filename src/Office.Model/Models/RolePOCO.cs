﻿using Office.Model.Common;
using System;

namespace Office.Model
{
    public partial class RolePOCO : IRole
    {
        #region Properties

        public string Description { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }

        #endregion Properties
    }
}