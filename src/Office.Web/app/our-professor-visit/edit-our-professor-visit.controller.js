(function (angular) {
    'use strict';
    angular.module('officeApp').controller('EditOurProfessorVisitController', ['$scope', 'ourProfessorVisitService', 'userProfileService', 'foreignInstitutionService', 'lookupTablesService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, ourProfessorVisitService, userProfileService, foreignInstitutionService, lookupTablesService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.ourProfessorVisit = $scope.ngDialogData;
            if (vm.ourProfessorVisit && vm.ourProfessorVisit.StartDate) {
                vm.ourProfessorVisit.StartDate = new Date(vm.ourProfessorVisit.StartDate);
            }
            vm.lookups = [];
            vm.foreignInstitutions = [];
            vm.userProfiles = [];
            vm.check = {
                user: false,
                department: false,
                type: false,
                institution: false,
                duration: false,
                tfunding: false,
                sfunding: false
            }

            vm.addUserProfile = function () {
                ngDialog.open({
                    template: 'app/user-profile/add-user-profile.html',
                    controller: 'AddUserProfileController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchUsers = function () {
                return userProfileService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.userProfiles = data;
                    });
            };

            vm.fetchForeignInstitutions = function () {
                return foreignInstitutionService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.foreignInstitutions = data;
                    });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('Departments, Durations, Fundings, VisitTypes')
                    .then(function (data) {
                        vm.lookups = data;
                    },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchUsers();
                vm.fetchForeignInstitutions();
                vm.fetchLookups();
            }
            vm.fetch();

            vm.put = function (item) {
                if (item.Department || item.Duration || item.TravelFunding || item.FundingOfStay ||
                    item.VisitTypes || item.NewVisitTypes) {
                    lookupTablesService.clearCache();
                };
                if (vm.ourProfessorVisit.NewVisitTypes) {
                    var res = vm.ourProfessorVisit.NewVisitTypes.split(",");
                    for (var ind = 0; ind < res.length; ind++) {
                        vm.ourProfessorVisit.ProfessorVisitTypes.push({ ID: res[ind] });
                    };
                    vm.ourProfessorVisit.NewVisitTypes = null;
                };
                if (vm.ourProfessorVisit.ProfessorVisitTypes) {
                    for (var ind = 0; ind < vm.ourProfessorVisit.ProfessorVisitTypes.length; ind++) {
                        vm.ourProfessorVisit.ProfessorVisitTypes[ind].VisitType = null;
                    };
                };
                vm.ourProfessorVisit.ForeignInstitution = null;
                vm.ourProfessorVisit.UserProfile = null;

                ourProfessorVisitService.put(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                        $state.go('ourProfessorVisit', { 'notification': notifications.entity.editError });
                    });
            };
        }]);
})(angular);