﻿using System;
using System.Threading.Tasks;

namespace Office.Repository.Common
{
    public interface IUnitOfWork : IDisposable
    {
        #region Methods

        Task<int> AddAsync<T>(T entity) where T : class;

        Task<int> CommitAsync();

        Task<int> DeleteAsync<T>(T entity) where T : class;

        Task<int> DeleteAsync<T>(string ID) where T : class;

        Task<int> UpdateAsync<T>(T entity) where T : class;

        #endregion Methods
    }
}