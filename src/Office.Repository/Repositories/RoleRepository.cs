﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Office.Repository
{
    public class RoleRepository : IRoleRepository
    {
        #region Constructors

        public RoleRepository(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public virtual Task<int> DeleteAsync(IRole entity)
        {
            return Repository.DeleteAsync<role>(Mapper.Map<role>(entity));
        }

        public virtual Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<role>(ID);
        }

        public virtual async Task<List<IRole>> GetAsync()
        {
            return Mapper.Map<List<IRole>>(
                await Repository.WhereAsync<role>()
                        .ToListAsync<role>()
                );
        }

        public virtual async Task<IRole> GetAsync(string ID)
        {
            return Mapper.Map<IRole>(await Repository.SingleAsync<role>(ID));
        }

        public virtual Task<int> InsertAsync(IRole entity)
        {
            return Repository.InsertAsync<role>(Mapper.Map<role>(entity));
        }

        public virtual Task<int> UpdateAsync(IRole entity)
        {
            return Repository.UpdateAsync<role>(Mapper.Map<role>(entity));
        }

        #endregion Methods
    }
}