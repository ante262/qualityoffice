﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Office.WebAPI.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        #region Constructors

        public ApplicationDbContext()
            : base("officeappContext", throwIfV1Schema: false)
        {
        }

        #endregion Constructors

        #region Methods

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Map Tables
            modelBuilder.Entity<ApplicationUser>().ToTable("user");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("userlogin");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("userclaim");
            modelBuilder.Entity<IdentityRole>().ToTable("role");
            modelBuilder.Entity<IdentityUserRole>().ToTable("userrole");

            // Map Columns
            modelBuilder.Entity<ApplicationUser>().Property(u => u.Id).HasColumnName("ID");
            modelBuilder.Entity<IdentityRole>().Property(u => u.Id).HasColumnName("ID");
            modelBuilder.Entity<IdentityUserClaim>().Property(u => u.Id).HasColumnName("ID");
        }

        #endregion Methods
    }

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit
    // http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        #region Methods

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        #endregion Methods
    }
}