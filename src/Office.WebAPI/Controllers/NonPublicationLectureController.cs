﻿using AutoMapper;
using Office.Model;
using Office.Model.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/NonPublicationLecture")]
    public class NonPublicationLectureController : ApiController
    {
        #region Constructors

        public NonPublicationLectureController(INonPublicationLectureService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private INonPublicationLectureService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/NonPublicationLecture/
        [HttpDelete]
        [Route("{ID}")]
        public async Task<IHttpActionResult> Delete(string ID)
        {
            try
            {
                var result = await Service.DeleteAsync(ID);
                if (result == 1) return Ok("Deleted");
                else return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        // GET: api/NonPublicationLecture
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<NonPublicationLectureModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/NonPublicationLecture/5
        [HttpGet]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Get(string ID)
        {
            var result = await Service.GetAsync(ID);

            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<NonPublicationLectureModel>(result));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        // POST: api/NonPublicationLecture
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post(NonPublicationLectureModel entity)
        {
            try
            {
                var result = await Service.InsertAsync(Mapper.Map<INonPublicationLecture>(entity));

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Add operation error.");

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/NonPublicationLecture/5
        [HttpPut]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Put(string ID, NonPublicationLectureModel entity)
        {
            try
            {
                if (ID != entity.ID)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "IDs do not match.");
                }

                var result = await Service.UpdateAsync(Mapper.Map<INonPublicationLecture>(entity));
                if (result > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "Failed.");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public partial class ForeignInstitutionModel
        {
            #region Properties

            public string InstitutionName { get; set; }

            #endregion Properties
        }

        public partial class NonPublicationLectureModel
        {
            #region Properties

            public string City { get; set; }
            public string Comment { get; set; }
            public ConferenceTypePOCO ConferenceType { get; set; }
            public string ConferenceTypeId { get; set; }
            public Nullable<System.DateTime> Date { get; set; }
            public string Description { get; set; }
            public string EnglishTitle { get; set; }
            public ForeignInstitutionModel ForeignInstitution { get; set; }
            public string ForeignInstitutionId { get; set; }
            public string ID { get; set; }
            public string Location { get; set; }
            public string Name { get; set; }
            public string Period { get; set; }
            public string Profession { get; set; }
            public string State { get; set; }
            public string Summary { get; set; }
            public string Surname { get; set; }
            public string Title { get; set; }
            public string URL { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}