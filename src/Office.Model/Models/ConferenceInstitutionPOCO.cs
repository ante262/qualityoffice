﻿using Office.Model.Common;

using System;

namespace Office.Model
{
    public partial class ConferenceInstitutionPOCO : IConferenceInstitution
    {
        #region Properties

        public string ConferenceId { get; set; }
        public IForeignInstitution ForeignInstitution { get; set; }
        public string ForeignInstitutionId { get; set; }
        public string ID { get; set; }

        #endregion Properties
    }
}