ALTER TABLE `officeapp`.`nonpublicationlecture` 
ADD COLUMN `ForeignInstitutionId` VARCHAR(128) NULL DEFAULT NULL AFTER `Comment`,
ADD INDEX `nonpublicationlecture_foreigninstitution_idx` (`ForeignInstitutionId` ASC);
ALTER TABLE `officeapp`.`nonpublicationlecture` 
ADD CONSTRAINT `nonpublicationlecture_foreigninstitution`
  FOREIGN KEY (`ForeignInstitutionId`)
  REFERENCES `officeapp`.`foreigninstitution` (`ID`)
  ON DELETE RESTRICT
  ON UPDATE NO ACTION;