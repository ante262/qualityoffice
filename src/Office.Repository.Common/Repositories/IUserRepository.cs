﻿using Office.Model.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Repository.Common
{
    public interface IUserRepository
    {
        #region Methods

        Task<int> DeleteAsync(IUser entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IUser>> GetAsync();

        Task<IUser> GetAsync(string ID);

        Task<int> InsertAsync(IUser entity);

        Task<int> UpdateAsync(IUser entity);

        #endregion Methods
    }
}