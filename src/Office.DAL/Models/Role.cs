using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class role
    {
        public role()
        {
            this.users = new List<user>();
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<user> users { get; set; }
    }
}
