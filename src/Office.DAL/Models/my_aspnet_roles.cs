using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class my_aspnet_roles
    {
        public int id { get; set; }
        public int applicationId { get; set; }
        public string name { get; set; }
    }
}
