﻿using Office.Model.Common;
using System;

namespace Office.Model
{
    public partial class DurationPOCO : IDuration
    {
        #region Properties

        public string Abbreviation { get; set; }
        public string ID { get; set; }
        public string Length { get; set; }

        #endregion Properties
    }
}