using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class user
    {
        public user()
        {
            this.userclaims = new List<userclaim>();
            this.userlogins = new List<userlogin>();
            this.roles = new List<role>();
        }

        public string ID { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public virtual ICollection<userclaim> userclaims { get; set; }
        public virtual ICollection<userlogin> userlogins { get; set; }
        public virtual ICollection<role> roles { get; set; }
    }
}
