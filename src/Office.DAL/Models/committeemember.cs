using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class committeemember
    {
        public string ID { get; set; }
        public string UserProfileId { get; set; }
        public string Title { get; set; }
        public string CommitteeId { get; set; }
        public string CommitteeMemberRoleId { get; set; }
        public string PeriodOfElection { get; set; }
        public Nullable<System.DateTime> DateOfElection { get; set; }
        public string ElectingInstitutionId { get; set; }
        public string Comment { get; set; }
        public virtual committee committee { get; set; }
        public virtual committeememberrole committeememberrole { get; set; }
        public virtual electinginstitution electinginstitution { get; set; }
        public virtual userprofile userprofile { get; set; }
    }
}
