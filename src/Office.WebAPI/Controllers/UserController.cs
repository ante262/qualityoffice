﻿using AutoMapper;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        #region Constructors

        public UserController(IUserService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private IUserService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/User/
        [HttpDelete]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Delete(string ID)
        {
            try
            {
                if (await Service.DeleteAsync(ID) == 1)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Deleted.");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (InvalidOperationException e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        //metode:
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<UserModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/User/5
        [HttpGet]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Get(string ID)
        {
            try
            {
                var result = await Service.GetAsync(ID);
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<UserModel>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public class UserModel
        {
            #region Properties

            //public int AccessFailedCount { get; set; }

            public string Email { get; set; }

            //public bool EmailConfirmed { get; set; }

            public string ID { get; set; }

            //public bool LockoutEnabled { get; set; }

            //public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }

            //public string PasswordHash { get; set; }

            //public string PhoneNumber { get; set; }

            //public bool PhoneNumberConfirmed { get; set; }

            //public string SecurityStamp { get; set; }

            //public bool TwoFactorEnabled { get; set; }

            public string UserName { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}