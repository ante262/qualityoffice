﻿using AutoMapper;
using Office.Model.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/ForeignInstitution")]
    public class ForeignInstitutionController : ApiController
    {
        #region Constructors

        public ForeignInstitutionController(IForeignInstitutionService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private IForeignInstitutionService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/ForeignInstitution/
        [HttpDelete]
        [Route("{ID}")]
        public async Task<IHttpActionResult> Delete(string ID)
        {
            try
            {
                var result = await Service.DeleteAsync(ID);
                if (result == 1) return Ok("Deleted");
                else return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        // GET: api/ForeignInstitution
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<ForeignInstitutionModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/ForeignInstitution/5
        [HttpGet]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Get(string ID)
        {
            var result = await Service.GetAsync(ID);

            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<ForeignInstitutionModel>(result));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        // POST: api/ForeignInstitution
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post(ForeignInstitutionModel entity)
        {
            try
            {
                var result = await Service.InsertAsync(Mapper.Map<IForeignInstitution>(entity));

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Add operation error.");

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/ForeignInstitution/5
        [HttpPut]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Put(string ID, ForeignInstitutionModel entity)
        {
            try
            {
                if (ID != entity.ID)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "IDs do not match.");
                }

                var result = await Service.UpdateAsync(Mapper.Map<IForeignInstitution>(entity));
                if (result == 1)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "Failed.");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public partial class ContactModel
        {
            #region Properties

            public string Name { get; set; }
            public string Surname { get; set; }

            #endregion Properties
        }

        public partial class ContractModel
        {
            #region Properties

            public string Name { get; set; }

            #endregion Properties
        }

        public partial class ForeignInstitutionModel
        {
            #region Properties

            public string City { get; set; }
            public ContactModel Contact { get; set; }
            public string ContactId { get; set; }
            public ContractModel Contract { get; set; }
            public string ContractId { get; set; }
            public string Description { get; set; }
            public bool Foreign { get; set; }
            public string ID { get; set; }
            public string InstitutionName { get; set; }
            public bool IsContract { get; set; }
            public string State { get; set; }
            public string URL { get; set; }
            public UserProfileModel UserProfile { get; set; }
            public string UserProfileId { get; set; }

            #endregion Properties
        }

        public partial class UserProfileModel
        {
            #region Properties

            public string Name { get; set; }
            public string Surname { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}