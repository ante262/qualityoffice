﻿using AutoMapper;
using Office.DAL.Models;
using Office.Model.Common;
using Office.Repository.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Office.Service
{
    public class ConferenceService : IConferenceService
    {
        #region Constructors

        public ConferenceService(IRepository repository, ILookupTablesService lookupTablesService)
        {
            Repository = repository;
            LookupTablesService = lookupTablesService;
        }

        #endregion Constructors

        #region Properties

        protected ILookupTablesService LookupTablesService { get; private set; }
        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public async Task<int> AddChoiceRangeForDeleteAsync(IUnitOfWork unitOfWork, string[] ids)
        {
            var result = 0;
            foreach (var id in ids)
            {
                result += await unitOfWork.DeleteAsync<conferenceinstitution>(id);
            }
            return result;
        }

        public async Task<int> AddChoiceRangeForInsertAsync(IUnitOfWork unitOfWork, string conferenceId, IConferenceInstitution[] entities)
        {
            var result = 0;
            foreach (var entity in entities)
            {
                entity.ConferenceId = conferenceId;
                entity.ID = Guid.NewGuid().ToString("N");
                var map = Mapper.Map<conferenceinstitution>(entity);
                result += await unitOfWork.AddAsync<conferenceinstitution>(map);
            }
            return result;
        }

        public async Task<int> AddChoiceRangeForUpdateAsync(IUnitOfWork unitOfWork, IConferenceInstitution[] entities)
        {
            var result = 0;
            foreach (var entity in entities)
            {
                result += await unitOfWork.UpdateAsync<conferenceinstitution>(Mapper.Map<conferenceinstitution>(entity));
            }
            return result;
        }

        public Task<int> AddForUpdateAsync(IUnitOfWork unitOfWork, IConference entity)
        {
            var map = Mapper.Map<conference>(entity);
            map.conferenceinstitutions = null;
            return unitOfWork.UpdateAsync<conference>(map);
        }

        public Task<int> DeleteAsync(IConference entity)
        {
            return Repository.DeleteAsync<conference>(Mapper.Map<conference>(entity));
        }

        public Task<int> DeleteAsync(string ID)
        {
            return Repository.DeleteAsync<conference>(ID);
        }

        public async Task<List<IConference>> GetAsync()
        {
            // include ConferenceType table into Conference table
            var result = await Repository.WhereAsync<conference>()
                          .Include(c => c.conferencetype)
                          .Include(ci => ci.conferenceinstitutions.Select(fi => fi.foreigninstitution))
                          .ToListAsync<conference>();

            return Mapper.Map<List<IConference>>(result);
        }

        public async Task<IConference> GetAsync(string ID)
        {
            return Mapper.Map<IConference>(await Repository.SingleAsync<conference>(ID));
        }

        public Task<int> InsertAsync(IConference entity)
        {
            entity.ID = Guid.NewGuid().ToString("N");
            var counter = 0;
            if (entity.ConferenceType != null && entity.ConferenceType.ID == null)
            {
                entity.ConferenceType.ID = Guid.NewGuid().ToString("N");
                entity.ConferenceTypeId = entity.ConferenceType.ID;
                counter++;
            }
            if (entity.ConferenceInstitutions != null)
            {
                foreach (var element in entity.ConferenceInstitutions)
                {
                    element.ID = Guid.NewGuid().ToString("N");
                    element.ConferenceId = entity.ID;
                }
            }

            if (counter > 0)
            {
                LookupTablesService.ClearCache();
            }

            return Repository.InsertAsync<conference>(Mapper.Map<conference>(entity));
        }

        public async Task<int> UpdateAsync(IConference entity)
        {
            var existing = await Repository.SingleAsync<conference>(entity.ID);
            var lookupItems = await LookupTablesService.GetAsync();

            var counter = 0;
            var unitOfWork = Repository.CreateUnitOfWork();
            if (entity.ConferenceType != null
                && !lookupItems.ConferenceTypes.Select(c => c.Type).Contains(entity.ConferenceType.Type))
            {
                entity.ConferenceType.ID = Guid.NewGuid().ToString("N");
                entity.ConferenceTypeId = entity.ConferenceType.ID;
                var map = Mapper.Map<conferencetype>(entity.ConferenceType);
                await unitOfWork.AddAsync<conferencetype>(map);
                counter++;
            }
            if (entity.ConferenceType != null
                && entity.ConferenceType.ID == entity.ConferenceTypeId
                && lookupItems.ConferenceTypes
                .Select(c => c.Type.ToLower())
                .Contains(entity.ConferenceType.Type.ToLower()))
            {
                entity.ConferenceTypeId = lookupItems.ConferenceTypes
                    .Where(c => c.Type.ToLower() == entity.ConferenceType.Type.ToLower())
                        .Select(c => c.ID).FirstOrDefault();
            }
            entity.ConferenceType = null;

            var collectionForInsert = entity.ConferenceInstitutions
                .Where(choice => existing.conferenceinstitutions
                    .SingleOrDefault(c => c.ID == choice.ID) == null)
                    .ToArray();
            var collectionForUpdate = entity.ConferenceInstitutions
                .Where(choice => existing.conferenceinstitutions
                    .SingleOrDefault(c => c.ID == choice.ID) != null)
                    .ToArray();
            var collectionForDelete = existing.conferenceinstitutions
                .Where(choice => entity.ConferenceInstitutions
                    .SingleOrDefault(c => c.ID == choice.ID) == null)
                    .Select(c => c.ID)
                    .ToArray();

            await this.AddChoiceRangeForDeleteAsync(unitOfWork, collectionForDelete);
            await this.AddChoiceRangeForUpdateAsync(unitOfWork, collectionForUpdate);
            await this.AddChoiceRangeForInsertAsync(unitOfWork, entity.ID, collectionForInsert);
            await this.AddForUpdateAsync(unitOfWork, entity);
            if (counter > 0)
            {
                await LookupTablesService.ClearCache();
            }
            return await unitOfWork.CommitAsync();
        }

        #endregion Methods
    }
}