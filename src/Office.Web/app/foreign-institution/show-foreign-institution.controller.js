(function (angular) {
    'use strict';
    angular.module('officeApp').controller('ShowForeignInstitutionController', ['$scope',
        function ($scope) {
            var vm = $scope.vm = {};
            vm.foreignInstitution = $scope.ngDialogData;
        }]);
})(angular);