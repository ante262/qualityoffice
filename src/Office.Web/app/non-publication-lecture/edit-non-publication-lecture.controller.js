(function (angular) {
    'use strict';
    angular.module('officeApp').controller('EditNonPublicationLectureController', ['$scope', 'nonPublicationLectureService', 'lookupTablesService', 'foreignInstitutionService', 'notifications', '$window', '$state', '$stateParams',
        function ($scope, nonPublicationLectureService, lookupTablesService, foreignInstitutionService, notifications, $window, $state, $stateParams) {
            var vm = $scope.vm = {};
            vm.nonPublicationLecture = $scope.ngDialogData;
            if (vm.nonPublicationLecture && vm.nonPublicationLecture.Date) {
                vm.nonPublicationLecture.Date = new Date(vm.nonPublicationLecture.Date);
            }
            vm.profession = [{ id: 1, Type: "Znanstveno" }, { id: 2, Type: "Stručno" }];
            vm.lookups = [];
            vm.foreignInstitutions = [];
            vm.check = {
                type: false,
                institution: false
            };

            vm.addForeignInstitution = function () {
                ngDialog.open({
                    template: 'app/foreign-institution/add-foreign-institution.html',
                    controller: 'AddForeignInstitutionController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchForeignInstitutions = function () {
                return foreignInstitutionService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.foreignInstitutions = data;
                    });
            };

            vm.fetchLookups = function () {
                lookupTablesService.fetch('ConferenceTypes')
                    .then(function (data) {
                        vm.lookups = data;
                    },
                function (error) {
                    console.log("Unable to get lookups: " + error.message);
                });
            };

            vm.fetch = function () {
                vm.fetchLookups();
                vm.fetchForeignInstitutions();
            }
            vm.fetch();

            vm.put = function (item) {
                if (item.ConferenceType) {
                    lookupTablesService.clearCache();
                };
                vm.nonPublicationLecture.ForeignInstitution = null;

                nonPublicationLectureService.put(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                        $state.go('nonPublicationLecture', { 'notification': notifications.entity.editError });
                    });
            };
        }]);
})(angular);