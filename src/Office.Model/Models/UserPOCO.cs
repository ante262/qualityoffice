﻿using Office.Model.Common;
using System;
using System.Collections.Generic;

namespace Office.Model
{
    public partial class UserPOCO : IUser
    {
        #region Constructors

        public UserPOCO()
        {
            //this.UserClaims = new List<IUserClaim>();
            //this.UserLogins = new List<IUserLogin>();
            this.Roles = new List<IRole>();
        }

        #endregion Constructors

        #region Properties

        public int AccessFailedCount { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string ID { get; set; }
        public bool LockoutEnabled { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public virtual ICollection<IRole> Roles { get; set; }
        public string SecurityStamp { get; set; }
        public bool TwoFactorEnabled { get; set; }

        //public virtual ICollection<IUserClaim> UserClaims { get; set; }
        //public virtual ICollection<IUserLogin> UserLogins { get; set; }
        public string UserName { get; set; }

        #endregion Properties
    }
}