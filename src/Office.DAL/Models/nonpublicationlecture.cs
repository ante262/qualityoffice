using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class nonpublicationlecture
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Location { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ConferenceTypeId { get; set; }
        public string Period { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Title { get; set; }
        public string EnglishTitle { get; set; }
        public string Profession { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public string Comment { get; set; }
        public string ForeignInstitutionId { get; set; }
        public virtual conferencetype conferencetype { get; set; }
        public virtual foreigninstitution foreigninstitution { get; set; }
    }
}
