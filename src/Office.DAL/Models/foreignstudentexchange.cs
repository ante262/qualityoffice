using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class foreignstudentexchange
    {
        public string ID { get; set; }
        public string DepartmentId { get; set; }
        public string UserProfileId { get; set; }
        public string CourseId { get; set; }
        public string VisitPurposeId { get; set; }
        public string ForeignStudentId { get; set; }
        public string SemesterId { get; set; }
        public string ForeignInstitutionId { get; set; }
        public Nullable<short> AcademicYear { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public string DurationId { get; set; }
        public string ExchangeOrganizationId { get; set; }
        public string TravelFundingId { get; set; }
        public string FundingOfStayId { get; set; }
        public string ExchangeResult { get; set; }
        public string Comment { get; set; }
        public virtual course course { get; set; }
        public virtual department department { get; set; }
        public virtual duration duration { get; set; }
        public virtual exchangeorganization exchangeorganization { get; set; }
        public virtual foreigninstitution foreigninstitution { get; set; }
        public virtual foreignstudent foreignstudent { get; set; }
        public virtual funding funding { get; set; }
        public virtual semester semester { get; set; }
        public virtual funding funding1 { get; set; }
        public virtual userprofile userprofile { get; set; }
        public virtual visitpurpose visitpurpose { get; set; }
    }
}
