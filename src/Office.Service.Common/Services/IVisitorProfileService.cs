﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IVisitorProfileService
    {
        #region Methods

        Task<int> DeleteAsync(IVisitorProfile entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IVisitorProfile>> GetAsync();

        Task<IVisitorProfile> GetAsync(string ID);

        Task<int> InsertAsync(IVisitorProfile entity);

        Task<int> UpdateAsync(IVisitorProfile entity);

        #endregion Methods
    }
}