﻿using AutoMapper;
using Office.Model;
using Office.Model.Common;
using Office.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Office.WebAPI.Controllers
{
    [RoutePrefix("api/UserProfile")]
    public class UserProfileController : ApiController
    {
        #region Constructors

        public UserProfileController(IUserProfileService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private IUserProfileService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/UserProfile/
        [HttpDelete]
        [Route("{ID}")]
        public async Task<IHttpActionResult> Delete(string ID)
        {
            try
            {
                var result = await Service.DeleteAsync(ID);
                if (result == 1) return Ok("Deleted");
                else return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        // GET: api/UserProfile
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<UserProfileModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/UserProfile/5
        [HttpGet]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Get(string ID)
        {
            var result = await Service.GetAsync(ID);

            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<UserProfileModel>(result));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        // POST: api/UserProfile
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post(UserProfileModel entity)
        {
            try
            {
                var result = await Service.InsertAsync(Mapper.Map<IUserProfile>(entity));

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Add operation error.");

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/UserProfile/5
        [HttpPut]
        [Route("{ID}")]
        public async Task<HttpResponseMessage> Put(string ID, UserProfileModel entity)
        {
            try
            {
                if (ID != entity.ID)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "IDs do not match.");
                }

                var result = await Service.UpdateAsync(Mapper.Map<IUserProfile>(entity));
                if (result > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "Failed.");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public partial class UserProfileModel
        {
            #region Properties

            public string AcademicRankId { get; set; }

            // Added to return Chair table with UserProfile table
            public ChairPOCO Chair { get; set; }

            public string ChairId { get; set; }
            public string Comment { get; set; }
            public string Email { get; set; }
            public Nullable<sbyte> Floor { get; set; }
            public string ID { get; set; }
            public string Name { get; set; }
            public string PicturePath { get; set; }

            // Added to return ResearchGroup table with UserProfile table
            public ResearchGroupPOCO ResearchGroup { get; set; }

            public string ResearchGroupId { get; set; }
            public string Room { get; set; }
            public string Surname { get; set; }
            public string TelephoneNumber { get; set; }
            public string TitleId { get; set; }
            public string URL { get; set; }

            // Added to return UserAcademicRank table with UserProfile table
            public UserAcademicRankPOCO UserAcademicRank { get; set; }

            // Added to return UserTitle table with UserProfile table
            public UserTitlePOCO UserTitle { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}