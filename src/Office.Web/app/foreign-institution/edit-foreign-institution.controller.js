(function (angular) {
    'use strict';
    angular.module('officeApp').controller('EditForeignInstitutionController', ['$scope', 'foreignInstitutionService', 'contactService', 'userProfileService', 'contractService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, foreignInstitutionService, contactService, userProfileService, contractService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.foreignInstitution = $scope.ngDialogData;

            vm.contacts = [];
            vm.userProfiles = [];
            vm.contracts = [];
            vm.check = {
                contact: false,
                user: false,
                contract: false
            };

            vm.addUserProfile = function () {
                ngDialog.open({
                    template: 'app/user-profile/add-user-profile.html',
                    controller: 'AddUserProfileController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addContact = function () {
                ngDialog.open({
                    template: 'app/contact/add-contact.html',
                    controller: 'AddContactController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.addContract = function () {
                ngDialog.open({
                    template: 'app/contract/add-contract.html',
                    controller: 'AddContractController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.fetchUsers = function () {
                return userProfileService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.userProfiles = data;
                    });
            };

            vm.fetchContracts = function () {
                return contractService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.contracts = data;
                    });
            };

            vm.fetchContacts = function () {
                return contactService.fetch()
                    .error(function (data) {
                    })
                    .success(function (data) {
                        vm.contacts = data;
                    });
            };

            vm.fetch = function () {
                vm.fetchUsers();
                vm.fetchContracts();
                vm.fetchContacts();
            }
            vm.fetch();

            vm.put = function (item) {
                if (!item.Foreign) {
                    item.State = "Hrvatska";
                }
                vm.foreignInstitution.Contact = null;
                vm.foreignInstitution.Contract = null;
                vm.foreignInstitution.UserProfile = null;

                foreignInstitutionService.put(item).success(
                    function (data) {
                        $scope.closeThisDialog();
                    }).error(function (data) {
                        console.log(item);
                    });
            };
        }]);
})(angular);