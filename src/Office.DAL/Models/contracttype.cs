using System;
using System.Collections.Generic;

namespace Office.DAL.Models
{
    public partial class contracttype
    {
        public contracttype()
        {
            this.contracts = new List<contract>();
        }

        public string ID { get; set; }
        public string Type { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<contract> contracts { get; set; }
    }
}
