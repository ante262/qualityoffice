﻿using Office.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Office.Service.Common
{
    public interface IForeignStudentService
    {
        #region Methods

        Task<int> DeleteAsync(IForeignStudent entity);

        Task<int> DeleteAsync(string ID);

        Task<List<IForeignStudent>> GetAsync();

        Task<IForeignStudent> GetAsync(string ID);

        Task<int> InsertAsync(IForeignStudent entity);

        Task<int> UpdateAsync(IForeignStudent entity);

        #endregion Methods
    }
}