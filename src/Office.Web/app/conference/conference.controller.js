(function (angular) {
    'use strict';
    angular.module('officeApp')
        .controller('ConferenceController', ['$scope', 'conferenceService', 'notifications', '$window', '$state', '$stateParams', 'ngDialog',
        function ($scope, conferenceService, notifications, $window, $state, $stateParams, ngDialog) {
            var vm = $scope.vm = {};
            vm.conferences = [];
            vm.conference = {};
            vm.selectedConference = null;
            vm.loading = true;

            vm.addConference = function () {
                ngDialog.open({
                    template: 'app/conference/add-conference.html',
                    controller: 'AddConferenceController',
                    className: 'ngdialog-theme-default',
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            };

            vm.editConference = function (data) {
                ngDialog.open({
                    template: 'app/Conference/edit-conference.html',
                    controller: 'EditConferenceController',
                    className: 'ngdialog-theme-default',
                    data: data,
                    closeByDocument: false
                })
                .closePromise.then(function (data) {
                    vm.fetch();
                });
            }

            vm.showConference = function (data) {
                ngDialog.open({
                    template: 'app/Conference/show-conference.html',
                    controller: 'ShowConferenceController',
                    className: 'ngdialog-theme-default',
                    data: data
                })
            }

            vm.message = $stateParams.notification;
            if ($stateParams.notification == null) {
                vm.message = '';
            }

            vm.class = 'list-group-item-success';
            vm.iconClass = 'glyphicon-ok';
            if (vm.message.substring(0, 6) == "Greška") {
                vm.class = 'list-group-item-danger';
                vm.iconClass = 'glyphicon-alert';
            }

            if (vm.message == '') {
                vm.fadeClass = 'is-hidden';
            } else {
                vm.fadeClass = 'fade-out';
            }

            vm.fetch = function () {
                return conferenceService.fetch().error(function (data) {
                    if (data === null) {
                        $state.go('conference', { 'notification': notifications.entity.serverError });
                    };
                    console.log('Unable to get conference:' + data.message);
                    $state.go('conference', { 'notification': notifications.entity.loadError });
                }).success(function (data) {
                    vm.conferences = data;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.fetch();

            vm.get = function (item) {
                vm.selectedConference = item;
                conferenceService.get(vm.selectedConference)
                    .success(function (data) {
                        vm.conference = data;
                    }).error(function (data) {
                        console.log("Unable to get this conference:" + data.message);
                    });
            };

            vm.delete = function (data) {
                vm.conference = data;
                vm.fadeClass = 'is-hidden';
                vm.class = 'list-group-item-warning';
                vm.iconClass = 'glyphicon-trash';
                if (confirm("Konferencija '" + vm.conference.Name + "' će biti obrisana!")) {
                    conferenceService.delete(vm.conference.ID).success(
                        function (data) {
                            vm.message = notifications.entity.deleteSuccess;
                            vm.fetch();
                        }).error(function (data) {
                            console.log(data);
                            vm.message = notifications.entity.deleteError;
                        }).finally(function () {
                            vm.fadeClass = 'fade-out';
                        });
                };
            };
        }]);
})(angular);