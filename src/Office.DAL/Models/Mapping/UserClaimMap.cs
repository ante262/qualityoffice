using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Office.DAL.Models.Mapping
{
    public class userclaimMap : EntityTypeConfiguration<userclaim>
    {
        public userclaimMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.UserId)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.ClaimType)
                .HasMaxLength(1073741823);

            this.Property(t => t.ClaimValue)
                .HasMaxLength(1073741823);

            // Table & Column Mappings
            this.ToTable("userclaim", "officeapp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.ClaimType).HasColumnName("ClaimType");
            this.Property(t => t.ClaimValue).HasColumnName("ClaimValue");

            // Relationships
            this.HasRequired(t => t.user)
                .WithMany(t => t.userclaims)
                .HasForeignKey(d => d.UserId);

        }
    }
}
